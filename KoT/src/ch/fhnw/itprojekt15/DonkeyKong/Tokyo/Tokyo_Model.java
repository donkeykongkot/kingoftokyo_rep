package ch.fhnw.itprojekt15.DonkeyKong.Tokyo;
import javax.swing.JOptionPane;

import ch.fhnw.itprojekt15.*;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Monsters;

/**
 * Aktualisiert den Tokyo Status auf den Monstern.
 * 
 * leavetokyoQuestion:
 * Hier wird dem Spieler die Frage gestellt, ob er Tokyo verlassen m�chte.
 * 
 * sendTokyoStatus:
 * Hier wird dem Server eine Meldung geschickt, sollte sich in Tokyo etwas tun.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:17:00
 */
public class Tokyo_Model implements Constants_Tokyo {

	private Monsters monsterList;
	private JOptionPane questionLeaveTokyo;

	public Tokyo_Model(){

	}

	
	protected void actualiseTokyoOccupied(){

	}

	protected void tokyoLeaveAnswer(){

	}
}//end Tokyo_Model