package ch.fhnw.itprojekt15.DonkeyKong.Tokyo;
import ch.fhnw.itprojekt15.*;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Monsters;

/**
 * in-Message:
 * Diese Methode wird vom Service_Enum aufgerufen. Hier wird nun entschieden wie
 * weiter zu verfahren ist.
 * Ebenfalls wird das GUI mit dem korrekten Monster besetzt.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:17:00
 */
public class Tokyo_Controller implements Constants_Tokyo {

	private Tokyo_Model model;
	private Monsters monsterList;
	private static Tokyo_Controller singelton;
	private Tokyo_View view;
	public Monsters m_Monsters;
	public Tokyo_View m_Tokyo_View;
	public Tokyo_Model m_Tokyo_Model;

	public Tokyo_Controller(){

	}

	
	/**
	 * 
	 * @param message
	 */
	public void connectionLost(String message){

	}

	public static Tokyo_Controller getTokyo(){
		return null;
	}

	/**
	 * 
	 * @param message
	 */
	public void playerLeave(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void playerSuspendet(String message){

	}

	private void Tokyo(){

	}

	/**
	 * 
	 * @param message
	 */
	public void tokyoGo(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void tokyoLeaveAnswer(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void tokyoLeaveQuestion(String message){

	}
}//end Tokyo_Controller