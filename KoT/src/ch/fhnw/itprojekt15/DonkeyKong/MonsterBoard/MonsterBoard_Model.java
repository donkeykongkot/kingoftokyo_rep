package ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard;


import ch.fhnw.itprojekt15.*;
/**
 * Hier werden UseCase 302 bis UseCase 305 abgebildet und die Werte auf den
 * Monsern gesetzt.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:14:50
 */
public class MonsterBoard_Model implements Constants_Monster {

	private Monsters monsterList;

	public MonsterBoard_Model(){

	}

	
	/**
	 * 
	 * @param message
	 */
	protected void collectEnergy(String message){

	}

	/**
	 * 
	 * @param message
	 */
	protected void collectHealth(String message){

	}

	/**
	 * 
	 * @param message
	 */
	protected void collectVictoryPoints(String message){

	}

	/**
	 * 
	 * @param message
	 */
	protected void looseLife(String message){

	}

	
}//end MonsterBoard_Model