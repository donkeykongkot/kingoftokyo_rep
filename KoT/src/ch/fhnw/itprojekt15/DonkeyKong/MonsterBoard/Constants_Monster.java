package ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard;

import ch.fhnw.itprojekt15.*;
/**
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:14:50
 */
public interface Constants_Monster {

	
	public static enum Monster {Platzhalter("platzhalter");
	
	
	public static String pathPictureHeart;
	public static String pathPictureVictoryPoints;
	public static String picturePath;
	public static  boolean tokyoOccupied;
	public static int victoryPoints;
	public static boolean belongsToThisPlayer;
	public static int energy;
	public static int life;

	/**
	 * 
	 * @param picturePath
	 */
	Monster(String picturePath) {
	}
	
	
	public boolean getBelongsToThisPlayer() {
		return false;
	}

	public int getEnergy() {
		return 0;
	}

	public int getLife() {
		return 0;
	}

	public String getPicturePath() {
		return null;
	}

	public boolean getTokyoOccupied() {
		return false;
	}

	public int getVicotryPoints() {
		return 0;
	}



	/**
	 * 
	 * @param belongsToThisPlayer
	 */
	public void setBelongsToThisPlayer(boolean belongsToThisPlayer) {
	}

	/**
	 * 
	 * @param energy
	 */
	public void setEnergy(int energy) {
	}

	/**
	 * 
	 * @param life
	 */
	public void setLife(int life) {
	}

	/**
	 * 
	 * @param tokyo
	 */
	public void setTokyoOccupied(boolean tokyo) {
	}

	/**
	 * 
	 * @param victoryPoints
	 */
	public void setVictoryPoints(int victoryPoints) {
	}
	};


}