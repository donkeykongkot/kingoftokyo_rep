package ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard;


import ch.fhnw.itprojekt15.*;
/**
 * in-Methoden
 * Diese Methoden werden vom Service_Enum aufgerufen. Hier wird nun entschieden
 * wie weiter zu verfahren ist. Je nach dem welche Meldung vom Server zur�ckkommt
 * muss der Controller die Methoden auf dem Model aufrufen. (bei diceRollFinal)
 * Diese setzen auf dem Monster die entsprechenden Werte. Anschliessend �ndert der
 * Controller das GUI entsprechend. M�glicherweise kann man hier noch Effekte
 * einbauen (z.B. Rot blickend, wenn ein Monster soeben Leben verloren hat oder
 * gr�n blickend wenn ein Monster Ruhmpunkte erhalten hat.)
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:14:50
 */
public class MonsterBoard_Controller implements Constants_Monster {

	private MonsterBoard_Model model;
	private Monsters monsterList;
	private static MonsterBoard_Controller singelton;
	private MonsterBoard_View view;

	public MonsterBoard_Controller(){

	}

	
	/**
	 * 
	 * @param model
	 * @param view
	 */
	public MonsterBoard_Controller(MonsterBoard_Model model, MonsterBoard_View view){

	}

	/**
	 * 
	 * @param message
	 */
	public void activePlayer(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void connectionLost(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void diceRollFinal(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void diceTokyoBonus(String message){

	}

	public static MonsterBoard_Controller getController(){
		return null;
	}

	/**
	 * 
	 * @param view
	 * @param model
	 */
	public static MonsterBoard_Controller getController(MonsterBoard_View view, MonsterBoard_Model model){
		return null;
	}

	/**
	 * 
	 * @param message
	 */
	public void playerLeave(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void playerSuspended(String message){

	}

	
}//end MonsterBoard_Controller