package ch.fhnw.itprojekt15.DonkeyKong.ServerConnection;


import ch.fhnw.itprojekt15.*;
/**
 * <b>Constants</b><b>_Services</b>
 * Hier wird ein enum mit den vorkommenden Services deklariert.
 * 
 * Folgende Services sind zu definiere:
 * <ul>
 * 	<li>LoginValidation</li>
 * 	<li>LoginUserNotFound</li>
 * 	<li>LoginPasswordWrong</li>
 * 	<li>LoginOK</li>
 * 	<li>RegistrationValidation</li>
 * 	<li>RegistrationUserAlreadyExists</li>
 * 	<li>RegistrationOK</li>
 * 	<li>HighscoreRequestData</li>
 * 	<li>HighscoreSendData</li>
 * 	<li>PlayerListRequestData</li>
 * 	<li>PlayerListSendData</li>
 * 	<li>GameRequest</li>
 * 	<li>GameRequestJustOnePlayer</li>
 * 	<li>CountdownGameRequest</li>
 * 	<li>GameStartGame</li>
 * 	<li>GameBoardPlayerLeave</li>
 * 	<li>GamePlayerLeave</li>
 * 	<li>CountdownGame</li>
 * 	<li>GamePlayerSuspended</li>
 * 	<li>GameConnectionLost</li>
 * 	<li>GameActivePlayer</li>
 * 	<li>GameDiceTokyoBonus</li>
 * 	<li>DiceRoll</li>
 * 	<li>DiceRollFinal</li>
 * 	<li>GameTokyoLeaveQuestion</li>
 * 	<li>TokyoLeaveAnswer</li>
 * 	<li>TokyoGo</li>
 * 	<li>CardBuyQuestion</li>
 * 	<li>CardBuyAnswer</li>
 * 	<li>CardNewCards</li>
 * 	<li>CardOneNewCard</li>
 * 	<li>CardBought</li>
 * 	<li>GameProoverMonsterLoose</li>
 * 	<li>GameProoverMonsterWin</li>
 * </ul>
 * <b>searchController:</b>
 * Es handelt es sich hier um eine Methode, die innerhalb des enums definiert
 * wird! Im ersten String wird nun �ber ein Switch-Case-Konstrukt die richtige
 * Methode auf dem richtigen Controller ausgef�hrt und hierbei der zweite Teil
 * (String) der urspr�nglichen Message �bergeben. Je nach Service k�nnen mehrere
 * Controllers bzw. Methoden der Controllers aufgerufen werden.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:16:30
 */
public interface Constants_Services {

	public enum Service {platzhalter;
	

	/**
	 * 
	 * @param message
	 * @param service
	 */
	public void searchController(String message, String service) {
	}
	};

	

}