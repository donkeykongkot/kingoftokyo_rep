package ch.fhnw.itprojekt15.DonkeyKong.ServerConnection;


import ch.fhnw.itprojekt15.*;
/**
 * <b>ServerConnection_Model: </b>
 * <b>requestSocket (throws IOException):</b>
 * Diese Methode nimmt die IP-Nummer vom Controller entgegen. Zusammen mit der
 * Port-Nummer aus dem Interface wird das Server-Objekt instanziert in dem die
 * getServer-Methode der Klasse Server aufgerufen wird. Die Methode erstellt noch
 * ein MessageHandler_IN Objekt. Den Konstruktoren wird hierbei das Objekt Server
 * �bergeben.
 * <b>buildLoginMVC:</b>
 * Loing_MVC sol instanziert werden (Login_Model und Login_View instanzieren und
 * diese dem Konstruktor Login_Controller �bergeben).
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:16:30
 */
public class ServerConnection_Model implements Constants_ServerConnection {

	private String ip;
	public Server m_Server;
	public MessageHandler_IN m_MessageHandler_IN;



	
	protected ServerConnection_Model(){

	}

	protected void buildLoginMVC(){

	}

	/**
	 * 
	 * @param ip
	 */
	protected void requestSocket(String ip){

	}
}//end ServerConnection_Model