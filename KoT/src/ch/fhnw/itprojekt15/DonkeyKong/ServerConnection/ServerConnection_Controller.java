package ch.fhnw.itprojekt15.DonkeyKong.ServerConnection;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ch.fhnw.itprojekt15.*;
/**
 * <b>ServerConnection_Controller:</b>
 * Der Konstruktor nimmt das Model- und View-Objekt entgegen und speichert die
 * Referenzen in den entsprechenden Instanzvariablen.
 * <b>actionPerformed</b>
 * Beim bet�tigen des Buttons, wird die requestSocket-Methode auf dem Model
 * ausgef�hrt. Hierbei wird die eingegebene IP-Nr. als String �bergeben. Wird
 * hierbei eine IOException ausgel�st (Server ist somit nicht erreichbar), soll
 * die im interface definierte Message (Msg_Error) angezeigt werden.
 * Nun wird noch die Methode buildLoginMVC vom Model aufgerufen.
 * ServerConnection_View soll danach noch disabled werden.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:16:30
 */
public class ServerConnection_Controller implements ActionListener, Constants_ServerConnection {

	private ServerConnection_Model model;
	private ServerConnection_View view;
	public ServerConnection_View m_ServerConnection_View;
	public ServerConnection_Model m_ServerConnection_Model;

	public ServerConnection_Controller(){

	}

	
	/**
	 * 
	 * @param model
	 * @param view
	 */
	protected ServerConnection_Controller(ServerConnection_Model model, ServerConnection_View view){

	}

	/**
	 * 
	 * @param next
	 */
	public void actionPerformed(ActionEvent next){

	}
}//end ServerConnection_Controller