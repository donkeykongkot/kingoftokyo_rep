package ch.fhnw.itprojekt15.DonkeyKong.ServerConnection;


import java.io.BufferedReader;
import java.net.Socket;

import ch.fhnw.itprojekt15.*;
/**
 * <b>MessageHandler_IN</b>
 * Die Klasse erbt von der Klasse Thread
 * 
 * <b>Konstruktor</b><b>:</b>
 * Der Konstruktor instanziert einen BufferedReader und weisst das Socket vom
 * Server der Instanzvariablen socket zu. Anschliessend wird Thread gestartet.
 * 
 * <b>run:</b>
 * Ruft die eigene listen-Methode auf.
 * 
 * <b>listen:</b>
 * in einer while-Schlaufe wartet der BufferdReader (welcher wiederum ein
 * InputStreamReader instanziert) auf Messages vom Server und weisst diese der
 * String-Variablen zu.
 * Danach wird die Hilfsmethode searchService aufgerufen.
 * 
 * <b>searchService:</b>
 * Die �bergebene Message wird aufgesplittet. Im ersten Teil der Messages ist
 * jeweils der Service enthalten. Im zweiten Teil ist die Message f�r den Service
 * enthalten.
 * Mit dem ersten Teil der Message wird der Service aufgerufen, welche im
 * Interface Constants_Services im <u>enum</u> definiert sind. Mit der Methode
 * valueOf (von der Klasse Enum) kann man den richtigen Service vom interface
 * holen. Danach wird mit der Methode searchController des Service die
 * auszuf�hrende Methode der Controller gesucht. Hierbei m�ssen beide Teile der
 * Message der Methode �bergeben werden.
 * Hinweis: die Controller m�ssen zwingend singelton sein.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:16:30
 */
public class MessageHandler_IN extends Thread implements Constants_Services {

	private BufferedReader inputMessage;
	private String message;
	private Server server;
	private Socket socket;
	public Server m_Server;

	public MessageHandler_IN(){

	}

	
	/**
	 * 
	 * @param server
	 */
	protected void MessageHandler_IN(Server server){

	}

	private void listen(){

	}

	public void run(){

	}

	/**
	 * 
	 * @param message
	 */
	private void searchService(String message){

	}

	/**
	 * 
	 * @param message
	 * @param service
	 */
	public void searchController(String message, String service){

	}
}//end MessageHandler_IN