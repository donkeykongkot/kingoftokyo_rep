package ch.fhnw.itprojekt15.DonkeyKong.ServerConnection;


import javax.swing.*;

import ch.fhnw.itprojekt15.*;
/**
 * <b>ServerConnection_View:</b>
 * ServerConnection_View erbt von der Klasse JFrame. Hier wird das in der
 * Anforderungsspezifikation aufgezeigte GUI erstellt. Die Texte der Elemente
 * werden vom Interface bezogen.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:16:30
 */
public class ServerConnection_View extends JFrame implements Constants_ServerConnection {

	protected JLabel errorMessage;
	protected JTextField ip;
	protected JButton next;

	public ServerConnection_View(){

	}


	/**
	 * 
	 * @param guiName
	 */
	protected ServerConnection_View(String guiName){

	}
}//end ServerConnection_View