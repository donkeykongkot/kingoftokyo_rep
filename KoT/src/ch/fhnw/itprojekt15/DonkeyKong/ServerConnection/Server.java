package ch.fhnw.itprojekt15.DonkeyKong.ServerConnection;


import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JOptionPane;

import ch.fhnw.itprojekt15.*;
/**
 * <b>Server</b>
 * Bei dieser Klasse handelt es sich um eine Singelton-Klasse. Es darf n�mlich nur
 * ein Server-Objekt instanziert werden, da zum Server nicht mehrere Verbindungen
 * aufgebaut werden m�ssen
 * <b>Konstruktor (throws IOException):</b>
 * Der Konstruktor versucht ein Socket zu erhalten. Hierf�r wird die IP und Port
 * ben�tigt. Das Socket wird in der Variablen socket abgespeichert. Das Server-
 * Objekt wird der singelton-Variablen zugewiesen. Ebenfalls soll ein PrintWriter-
 * Objekt mit dem getOutputStream des socket instanziert werden.
 * <b>getServer(int, String) (throws IOException)</b>
 * Ruft den Konstruktor auf. Existiert bereits ein Server-Objekt, wird dieses
 * zur�ckgegeben (singelton).
 * <b>getServer</b>
 * Diese Methode gibt das einzige Server-Objekt zur�ck (singelton).
 * <b>send:</b>
 * Mit dieser Methode kann man Messages an den Server schicken. Befehl lautet also:
 * getServer().send(�blablabla�). Sobald eine IOException geworfen wird, m�ssen
 * entsprechende aufr�umarbeiten stattfinden (z.B. Game schliessen) und dem
 * Spieler eine Hinweismeldung erscheinen (mit JOptionPane).
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:16:30
 */
public class Server {

	private JOptionPane connectionLost;
	private String ip;
	private String message;
	private PrintWriter outputMessage;
	private int port;
	private static Server singelton;
	protected Socket socket;

	public Server(){

	}

	
	/**
	 * 
	 * @param port
	 * @param ip
	 */
	private void Server(int port, String ip){

	}

	/**
	 * 
	 * @param port
	 * @param ip
	 */
	protected static Server getServer(int port, String ip){
		return null;
	}

	public static Server getServer(){
		return null;
	}

	/**
	 * 
	 * @param message
	 */
	public void send(String message){

	}
}//end Server