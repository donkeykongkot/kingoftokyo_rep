package ch.fhnw.itprojekt15.DonkeyKong.CardBoard;

import ch.fhnw.itprojekt15.*;

/**
 * <b>CardBoard_Model</b>
 * <b>buyAnswer</b>
 * Hier wird dem Server mitgeteilt, ob der Spieler Karten kaufen m�chte oder nicht.
 * 
 * Meldung:
 * CardBuyAnswer|true oder false
 * <b>cardBought</b>
 * Hier wird dem Server mitgeteilt, welche Karte der Spieler gekauft hat.
 * Meldung:
 * CardBought|Karte
 * <b>newCards</b>
 * Es m�ssen 3 neue Karten eruiert werden. Es wird eine Meldung an den Server
 * ausgel�st werden, welche drei Karte das sind.
 * Meldung:
 * CardNewCards|Karten (getrennt durch Kommas)
 * <b>oneNewCard</b>
 * Diese Methode w�hlt eine Karte aus und meldet diese dem Server.
 * Meldung:
 * CardOneNewCard|Karte
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:12:02
 */
public class CardBoard_Model implements Constants_Cards {



	public CardBoard_Model(){

	}

	/**
	 * 
	 * @param wannaBuyCards
	 */
	protected void buyAnswer(boolean wannaBuyCards){

	}

	/**
	 * 
	 * @param card
	 */
	protected void cardBought(Card card){

	}

	protected void newCards(){

	}

	protected void oneNewCard(){

	}

	public String getPathPicture(){
		return "";
	}
}//end CardBoard_Model