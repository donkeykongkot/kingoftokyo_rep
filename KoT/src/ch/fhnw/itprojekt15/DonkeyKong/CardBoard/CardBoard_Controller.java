package ch.fhnw.itprojekt15.DonkeyKong.CardBoard;

import ch.fhnw.itprojekt15.*;
import java.awt.event.ActionEvent;


/**
 * <b>CardBoard_Controller</b>
 * <b>actionPerformed</b>
 * newCards � sobald dieser Button geklickt wird, m�ssen die drei Karten
 * ausgetauscht werden. Hierf�r kann die Methode newCards vom Model aufgerufen
 * werden.
 * Exit � Das Monster hat zuerst gesagt, er m�chte Karten kaufen, nun will er doch
 * nicht. Daf�r muss die Methode buyAnswer mit dem boolean false aufgerufen werden.
 * 
 * Sobald eine Karte angeklickt wird, muss entsprechend die Methode cardBought()
 * vom Model aufgerufen werden und die Karte �bergeben werden, die der Spieler
 * gekauft hat.
 * <b>buyQuestion</b>
 * Hierbei wird eine Hinweisbox aufgerufen, mit der der Spieler anklicken kann, ob
 * er Kartekaufen m�chte oder nicht.
 * Wenn nicht � buyAnswer(false) vom Model
 * Wenn ja � buyAnswer(true) vom Model aufgerufen und die Buttons newCards, exit
 * werden angezeigt.
 * Meldung:
 * CardBuyQuestion|CardBuyQuestion
 * <b>newCards</b>
 * Diese Meldung zeigt an, welche Karten aufgedeckt wurden. Entsprechen m�ssen auf
 * dem GUI diese angezeigt werden.
 * Meldung:
 * CardNewCards|Karten (getrennt durch Kommas)
 * <b>cardBought</b>
 * Hier muss auf dem GUI der Kartenslot geleert werden, und zwar bei dieser Karte
 * die ein Spieler gekauft hat.
 * CardBought|Karte
 * <b>oneNewCard</b>
 * Hier wird gesagt, welche Karte den leeren Kartenslot f�llt.
 * Meldung:
 * CardOneNewCard|Karte
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:12:02
 */
public class CardBoard_Controller implements Constants_Cards {

	private CardBoard_Model model;
	private static CardBoard_Controller singelton;
	private CardBoard_View view;
	public CardBoard_View m_CardBoard_View;
	public CardBoard_Model m_CardBoard_Model;

	public CardBoard_Controller(){

	}

	
	/**
	 * 
	 * @param model
	 * @param view
	 */
	public CardBoard_Controller(CardBoard_Model model, CardBoard_View view){

	}

	/**
	 * 
	 * @param e
	 */
	public void actionPerformed(ActionEvent e){

	}

	/**
	 * 
	 * @param message
	 */
	public void buyQuestion(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void cardBought(String message){

	}

	public static CardBoard_Controller getController(){
		return null;
	}

	/**
	 * 
	 * @param model
	 * @param view
	 */
	public static CardBoard_Controller getController(CardBoard_Model model, CardBoard_View view){
		return null;
	}

	/**
	 * 
	 * @param message
	 */
	public void newCards(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void oneNewCard(String message){

	}

	public String getPathPicture(){
		return "";
	}
}//end CardBoard_Controller