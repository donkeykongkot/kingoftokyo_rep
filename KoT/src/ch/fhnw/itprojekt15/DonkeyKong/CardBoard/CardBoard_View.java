package ch.fhnw.itprojekt15.DonkeyKong.CardBoard;

import javax.swing.*;

import ch.fhnw.itprojekt15.*;

/**
 * <b>CardBoard_View</b>
 * CardBoard_View erbt von der Klasse JPanel. Hier wird das in der
 * Anforderungsspezifikation aufgezeigte Teil-GUI erstellt. Die Texte der Elemente
 * werden vom Interface bezogen. Dieses Panel wird dann in der GameBoard (JFrame)
 * angezeigt.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:12:02
 */
public class CardBoard_View extends JPanel implements Constants_Cards {

	protected JOptionPane buyQuestion;
	protected JButton[] cards;
	protected JButton exit;
	protected JButton newCards;



	
	public CardBoard_View(){

	}

	public String getPathPicture(){
		return "";
	}
}//end CardBoard_View