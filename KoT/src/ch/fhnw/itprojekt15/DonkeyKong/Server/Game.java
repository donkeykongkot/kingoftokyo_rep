package ch.fhnw.itprojekt15.DonkeyKong.Server;

import java.net.Socket;

/**
 * Konstruktor:
 * Anhand des Socket-Array vom GameRequest-Server und dem monsters Array vom
 * interface werden in einer for-Schlaufe so viele ClientMonster generiert, wie im
 * Socket-Array Anzahl Sockets sind. Diese ClientMonster werden wiederum in einem
 * eigenen Array gespeichert. Danach wird die Hilfsmethode startGame ausgef�hrt.
 * Danach wird noch der GameProover und der GameProcess Thread instanziert, sowie
 * f�r jedes socket ein GameListener.
 * 
 * run:
 * Dieser
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class Game implements Constants_Server {

	protected ClientMonster[] clientMonsters;
	private static int gameID;
	public ClientMonster m_ClientMonster;
	public GameProover m_GameProover;
	public Game_Process m_Game_Process;

	public Game(){

	}

	
	/**
	 * 
	 * @param sockets
	 */
	protected Game(Socket[] sockets){

	}

	protected void activePlayer(){

	}

	/**
	 * 
	 * @param socket
	 * @param message
	 */
	protected void cardBought(Socket socket, String message){

	}

	/**
	 * 
	 * @param socket
	 * @param message
	 */
	protected void cardBuyAnswer(Socket socket, String message){

	}

	protected void cardBuyQuestion(){

	}

	protected void connectionLost(){

	}

	/**
	 * 
	 * @param socket
	 * @param message
	 */
	protected void diceRoll(Socket socket, String message){

	}

	/**
	 * 
	 * @param socket
	 * @param message
	 */
	protected void diceRollFinal(Socket socket, String message){

	}

	protected void diceTokyoBonus(){

	}

	/**
	 * 
	 * @param socket
	 * @param message
	 */
	protected void newCards(Socket socket, String message){

	}

	/**
	 * 
	 * @param socket
	 * @param message
	 */
	protected void oneNewCard(Socket socket, String message){

	}

	/**
	 * 
	 * @param socket
	 * @param message
	 */
	protected void playerLeave(Socket socket, String message){

	}

	/**
	 * 
	 * @param clientMonster
	 */
	protected void playerSuspended(ClientMonster clientMonster){

	}

	protected void startGame(){

	}

	protected void tokyoGo(){

	}

	/**
	 * 
	 * @param socket
	 * @param message
	 */
	protected void tokyoLeaveAnswer(Socket socket, String message){

	}

	protected void tokyoLeaveQuestion(){

	}
}//end Game