package ch.fhnw.itprojekt15.DonkeyKong.Server;

import java.io.BufferedReader;

/**
 * run() Game_Process ist ein Thread. Beim Instanzieren eines GameProcess muss ein
 * Game �bergeben werden. Beim Starten des Threads, startet eine while-Schlaufe.
 * Zuerst wird mitgeteilt, wer an der Reihe ist, danach wird auf Input vom Spieler
 * gewartet, der an der Reihe ist - je nachdem muss der Spieler noch im
 * ClientMonster-Array gesucht werden, der in Tokyo steht und evtl. angegriffen
 * wird. Er erh�lt hierbei die M�glichkeit, Tokyo zu verlassen. Danach wird wieder
 * auf Input vom aktiven Spieler gewartet, ob er Karten kaufen m�chte. Hat ein
 * Spieler seinen Spielzug ausgef�hrt, startet die while-Schlaufe wieder von vorne.
 * Immer wenn der Thread auf einen Input des aktiven oder des Tokyo-Spielers
 * erwartet wird, wird entsprechend eine Countdown-Game Thread instanziert.
 * searchGameMethodes:
 * W�hrend eines Spielzuges k�nnen viele Meldungen reinkommen. Diese Methode hilft,
 * diese Meldungen zu analysieren und die korrekte Methode auf dem mitgelieferten
 * Game-Objekt wird aus zu f�hren.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class Game_Process extends Thread {

	private ClientMonster[] clientMonsters;
	private boolean endThread;
	private Game game;
	private BufferedReader inputMessage;
	private String message;

	public Game_Process(){

	}

	/**
	 * 
	 * @param game
	 */
	protected Game_Process(Game game){

	}

	protected void endThread(){

	}

	public void run(){

	}

	/**
	 * 
	 * @param message
	 */
	private void searchGameMethodes(String message){

	}
}//end Game_Process