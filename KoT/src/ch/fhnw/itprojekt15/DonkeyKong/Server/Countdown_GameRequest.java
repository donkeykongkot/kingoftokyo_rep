package ch.fhnw.itprojekt15.DonkeyKong.Server;

/**
 * countdown:
 * Der Countdown pr�ft in einer while-Schleife st�ndig das Socket-Array vom Objekt
 * GameRequest_Server. Sobald mehr als zwei Sockets im Array sind beginnt der
 * Countdown. Nun gibt es zwei M�glichkeiten: Entweder der Countdown ist
 * abgelaufen und es wird die Methode startGame ausgel�st oder das Array ist
 * pl�tzlich leer und der Thread stirbt.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class Countdown_GameRequest extends Thread {

	public Countdown_GameRequest(){

	}

	
	/**
	 * 
	 * @param gameRequest
	 */
	protected Countdown_GameRequest(GameRequest_Server gameRequest){

	}

	protected void countdown(){

	}
}//end Countdown_GameRequest