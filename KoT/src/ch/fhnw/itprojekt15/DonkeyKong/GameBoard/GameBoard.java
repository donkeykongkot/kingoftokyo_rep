package ch.fhnw.itprojekt15.DonkeyKong.GameBoard;

import javax.swing.JFrame;
import javax.swing.JPanel;

import ch.fhnw.itprojekt15.*;

/**
 * <b>GameBoard</b>
 * <b>Konstruktor:</b>
 * Der Konstruktor kann nur durch die getGameBoard-Methode aufgerufen werden.
 * Der Konstruktor erstellt die einzelnen MVC�s CardBoard, DiceBoard, MonsterBoard,
 * Toyko, TickerBoard. Die f�nf Views dieser MVC werden als JPanel-Objekte
 * zur�ckgegeben und auf dem GameBoard (ist ein JFrame) angezeigt. Siehe hierf�r
 * die entsprechenden GUI-Anforderungen in der Anforderungsspezifikation.
 * <b>closeGame: </b>
 * Diese Methode soll immer aufgerufen werden, wenn der Spieler das aktuelle Spiel
 * verl�sst (egal auf welche Art). Diese Methode dient dazu, hier allf�llige
 * Aufr�um-Arbeiten implementieren zu k�nnen.
 * <b>countdown</b>
 * Vom Server wird jede Sekunde eine Meldung geschickt. In dieser Meldung ist die
 * f�r den aktuellen Spielzug verbleibende Zeit enthalten (Sekunden). Die Sekunde
 * muss jeweils auf dem GameBoard angezeigt werden.
 * Meldung:
 * CountdownGame|Sekunden
 * <b>playerSuspended</b>
 * Wenn das betroffene Monster vom Server suspendiert wird, m�ssen mit der close-
 * Methode die Aufr�um-Arbeiten vollzogen und das Game-GUI disabled werden.
 * Welches Monster dem Spieler geh�rt kann man anhand des Objektes Monsters
 * eruiert werden.
 * Meldung:
 * GamePlayerSuspended|Monster
 * <b>playerLeave</b>
 * Sobald der Spieler das GameBoard-GUI schliesst � also das Spiel von sich aus
 * verl�sst � muss zuerst noch diese Meldung abgesetzt werden.
 * Meldung:
 * GameBoardPlayerLeave|GameBoardPlayerLeave
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:13:13
 */
public class GameBoard extends JFrame {

	private JPanel cardBoard;
	private JPanel diceBoard;
	private JPanel gameCountdown;
	private JPanel monsterBoard;
	
	private JPanel tickerBoard;
	private JPanel tokyo;
	



	
	private GameBoard(){

	}

	private void closeGame(){

	}

	/**
	 * 
	 * @param message
	 */
	public void countdown(String message){

	}

	public static GameBoard getGameBoard(){
		return null;
	}

	private void playerLeave(){

	}

	/**
	 * 
	 * @param message
	 */
	public void playerSuspended(String message){

	}
}//end GameBoard