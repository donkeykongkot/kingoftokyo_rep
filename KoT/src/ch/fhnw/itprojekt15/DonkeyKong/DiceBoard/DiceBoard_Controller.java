package ch.fhnw.itprojekt15.DonkeyKong.DiceBoard;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ch.fhnw.itprojekt15.*;

/**
 * Beim aktiven Player wird der Button "roll" aktiviert. Dieser kann nun W�rfeln.
 * Hierbei wird diceRoll vom Model ausgef�ht. Das Ergebnis geht an den Server,
 * welcher die Message an alle Spieler weiterleitet. Dies geschieht drei Mal. Beim
 * dritten Mal wird aber diceRollFinal vom Model ausgel�st und das letztmalige
 * W�rfeln signalisiert.
 * <b>
 * </b><b>
 * </b><b>DiceBoard_Controller</b>
 * <b>actionPerformed</b>
 * Zweimal w�rfeln, W�rfel behalten danach diceRoll auf dem Model wird zweimal
 * aufgerufen um Ergebnis an Server zu schicken.
 * Letztes mal w�rfel, W�rfel behalten danach diceRollFinal auf dem Model wird
 * aufgerufen um Ergebnis an Server zu schicken.
 * <b>activePlayer</b>
 * Hier muss der Button roll aktiviert werden, insofern es sich um das Monster
 * handelt, das sich in der Meldung befindet. Diese kann man mit dem Monsters-
 * Objekt feststellen
 * Meldung:
 * GameActivePlayer|Monster
 * <b>diceRoll</b>
 * Die W�rfel auf dem GUI m�ssen entsprechend der Meldung angezeigt werden.
 * Meldung:
 * DiceRoll| W�rfelergebnis getrennt durch ","
 * <b>diceRollFinal</b>
 * Die W�rfel auf dem GUI m�ssen entsprechend angezeigt werden.
 * Meldung:
 * DiceRollFinal| W�rfelergebnis getrennt durch ","
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:12:30
 */
public class DiceBoard_Controller implements ActionListener, Constants_Dices {
 /// TEEEEEST!
	private DiceBoard_Model model;
	private static DiceBoard_Controller singelton;
	private DiceBoard_View view;
	public DiceBoard_Model m_DiceBoard_Model;

	public DiceBoard_Controller(){

	}

	
	/**
	 * 
	 * @param view
	 * @param model
	 */
	public DiceBoard_Controller(DiceBoard_View view, DiceBoard_Model model){

	}

	/**
	 * 
	 * @param e
	 */
	public void actionPerformed(ActionEvent e){

	}

	/**
	 * 
	 * @param message
	 */
	public void activePlayer(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void diceRoll(String message){

	}

	/**
	 * Beim aktiven Player wird der Button "roll" aktiviert. Dieser kann nun W�rfeln.
	 * Hierbei wird diceRoll vom Model ausgef�ht. Das Ergebnis geht an den Server,
	 * dieser verteilt es an alle Spieler. Dies geschieht dreimal. Beim dritten mal
	 * wird aber diceRollFinal vom Model ausgel�st.
	 * 
	 * @param message
	 */
	public void diceRollFinal(String message){

	}

	public static DiceBoard_Controller getController(){
		return null;
	}

	/**
	 * 
	 * @param model
	 * @param view
	 */
	public static DiceBoard_Controller getController(DiceBoard_Model model, DiceBoard_View view){
		return null;
	}

	public boolean getKeepThisDice(){
		return false;
	}

	public String getPathPicture(){
		return "";
	}

	/**
	 * 
	 * @param keepThisDice
	 */
	public void setKeepThisDice(boolean keepThisDice){

	}



}//end DiceBoard_Controller