package ch.fhnw.itprojekt15.DonkeyKong.DiceBoard;

import javax.swing.JButton;
import javax.swing.JPanel;

import ch.fhnw.itprojekt15.*;

/**
 * <b>DiceBoard_View</b>
 * DiceBoard_View erbt von der Klasse JPanel. Hier wird das in der
 * Anforderungsspezifikation aufgezeigte Teil-GUI erstellt. Die Texte der Elemente
 * werden vom Interface bezogen. Dieses Panel wird dann in der GameBoard (JFrame)
 * angezeigt.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:12:30
 */
public class DiceBoard_View extends JPanel implements Constants_Dices {

	private JButton[] dices;
	private JButton roll;
	public DiceBoard_Controller m_DiceBoard_Controller;



	
	public DiceBoard_View(){

	}

	public boolean getKeepThisDice(){
		return false;
	}

	public String getPathPicture(){
		return "";
	}

	/**
	 * 
	 * @param keepThisDice
	 */
	public void setKeepThisDice(boolean keepThisDice){

	}
}//end DiceBoard_View