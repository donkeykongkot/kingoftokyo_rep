package ch.fhnw.itprojekt15.DonkeyKong.DiceBoard;

import ch.fhnw.itprojekt15.*;

/**
 * <b>DiceBoard_Model</b>
 * <b>diceRollFinal</b>
 * Meldung:
 * DiceRollFinal| Würfelergebnis getrennt durch ","
 * <b>diceRoll</b>
 * Hier wird gewürfelt und das Ergebnis an den Server verschickt.
 * Meldung:
 * DiceRoll| Würfelergebnis getrennt durch ","
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:12:30
 */
public class DiceBoard_Model implements Constants_Dices {



	
	public DiceBoard_Model(){

	}

	public void diceRoll(){

	}

	public void diceRollFinal(){

	}

	public boolean getKeepThisDice(){
		return false;
	}

	public String getPathPicture(){
		return "";
	}

	/**
	 * 
	 * @param keepThisDice
	 */
	public void setKeepThisDice(boolean keepThisDice){

	}
}//end DiceBoard_Model