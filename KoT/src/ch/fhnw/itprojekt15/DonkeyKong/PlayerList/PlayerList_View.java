package ch.fhnw.itprojekt15.DonkeyKong.PlayerList;
import javax.swing.*;

import ch.fhnw.itprojekt15.*;

/**
 * <b>PlayerList_View:</b>
 * PlayerList_View erbt von der Klasse JPanel. Hier wird das in der
 * Anforderungsspezifikation aufgezeigte Teil-GUI erstellt. Die Texte der Elemente
 * werden vom Interface bezogen. Dieses Panel wird dann in der Lobby_View (JFrame)
 * angezeigt.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 14:10:37
 */
public class PlayerList_View extends JPanel implements Constants_PlayerList {

	protected String[] column;
	protected JButton refresh;
	protected String[][] row;
	protected JTable table;




	
	public PlayerList_View(){

	}
}//end PlayerList_View