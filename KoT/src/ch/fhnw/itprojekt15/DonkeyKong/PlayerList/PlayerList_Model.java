package ch.fhnw.itprojekt15.DonkeyKong.PlayerList;

/**
 * <b>PlayerList_Model</b>
 * <b>requestData</b>
 * Hier sollen die PlayerList-Daten vom Server verlangt werden. Hierf�r kann die
 * Methode send von der Klasse Server zur Hilfe beigezogen werden.
 * Meldung:
 * PlayerListRequestData|PlayerListRequestData
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 14:10:37
 */
public class PlayerList_Model implements Constants_PlayerList {

	public PlayerList_Model(){

	}

	public void finalize() throws Throwable {

	}
	protected void requestData(){

	}
}//end PlayerList_Model