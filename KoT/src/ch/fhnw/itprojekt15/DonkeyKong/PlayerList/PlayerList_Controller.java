package ch.fhnw.itprojekt15.DonkeyKong.PlayerList;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * <b>PlayerList_Controller</b>
 * <b>Konstruktor:</b>
 * Beim Erstellen des PlayerList_Controllers wird �ber die Methode requestData()
 * des Models ein Request an den Server abgesetzt. Dieser liefert die PlayerList-
 * Daten zur�ck. Um den Datenverkehr zu minimieren, werden diese also nur einmalig
 * geliefert. K�nnen aber mit einem Aktualisieren-Button interaktiv neu
 * angefordert werden.
 * <b>actionPerformed</b>
 * Ruft die Methode requestData auf dem Model auf.
 * <b>sendData</b>
 * Sobald diese Methode aufgerufen wird, m�ssen die Daten aus der �bergebenen
 * Message auf dem GUI in einer Tabelle angezeigt werden.
 * Meldung:
 * PlayerListSendData|xxxx,xxxx,x,xxxxx;xxxx,xxxx,x,xxxxx;�..
 * Attribute werden durch "," getrennt, Datens�tze werden mit ";" getrennt
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 14:10:37
 */
public class PlayerList_Controller implements ActionListener, Constants_PlayerList {

	private PlayerList_Model model;
	private static PlayerList_Controller singelton;
	private PlayerList_View view;


	public PlayerList_Controller(){

	}

	public void finalize() throws Throwable {

	}
	/**
	 * 
	 * @param e
	 */
	public void actionPerformed(ActionEvent e){

	}

	public static PlayerList_Controller getController(){
		return null;
	}

	/**
	 * 
	 * @param model
	 * @param view
	 */
	public static PlayerList_Controller getController(PlayerList_Model model, PlayerList_View view){
		return null;
	}

	/**
	 * 
	 * @param model
	 * @param view
	 */
	public PlayerList_Controller(PlayerList_Model model, PlayerList_View view){

	}

	/**
	 * 
	 * @param message
	 */
	public void sendData(String message){

	}
}//end PlayerList_Controller