package ch.fhnw.itprojekt15.DonkeyKong.Login;

import java.awt.event.ActionListener;
import javax.swing.*;

import ch.fhnw.itprojekt15.*;
/**
 * <b>Login_View:</b>
 * Login_View erbt von der Klasse JFrame. Hier wird das in der
 * Anforderungsspezifikation aufgezeigte GUI erstellt. Die Texte der Elemente
 * werden vom Interface bezogen.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:14:29
 */
public class Login_View extends JFrame implements Constants_Login {

	protected JButton login = new JButton("Login");
	protected JLabel message = new JLabel();
	protected JPasswordField password = new JPasswordField(10);
	protected JButton registration = new JButton("Registrieren");
	protected JTextField username = new JTextField(10);

	
	public Login_View(){
		JPanel logView = new JPanel();
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(600, 200);
		
		logView.add(login);
		logView.add(message);
		logView.add(password);
		logView.add(registration);
		logView.add(username);
		
		this.add(logView);
		
		}
	
		public
	
	
	
}//end Login_View