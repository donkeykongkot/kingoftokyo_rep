package ch.fhnw.itprojekt15.DonkeyKong.Login;


import ch.fhnw.itprojekt15.*;
import ch.fhnw.itprojekt15.DonkeyKong.Registration.Registration_Model;
import ch.fhnw.itprojekt15.DonkeyKong.Registration.Registration_View;
/**
 * <b>Login_Model:</b>
 * <b>buildLobbyMVC:</b>
 * Lobby soll instanziert werden.
 * 
 * <b>buildRegistrationMVC:</b>
 * Registration_MVC soll instanziert werden (Registration_Model und
 * Registration_View instanzieren und diese dem Konstruktor
 * Registration_Controller �bergeben).
 * 
 * <b>validation</b>
 * Hier sollen die Logindaten an den Server geschickt werden. Hierf�r kann die
 * Methode send von der Klasse Server zur Hilfe beigezogen werden.
 * Meldung:
 * LoginValidation| Benutzername,Passwort
 * 
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:14:29
 */
public class Login_Model implements Constants_Login {

	private char[] password;
	private String username;



	
	public Login_Model(){

	}

	protected void buildLobbyMVC(){
		
	}

	protected void buildRegistrationMVC(){
		Registration_Model rm = new Registration_Model();
		Registration_View rv = new Registration_View();
	}

	/**
	 * 
	 * @param password
	 * @param username
	 */
	protected void validation(char[] password, String username){
		this.password = password;
		this.username = username;
	}
}//end Login_Model