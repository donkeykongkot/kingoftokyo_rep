package ch.fhnw.itprojekt15.DonkeyKong.Login;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ch.fhnw.itprojekt15.*;
/**
 * <b>Login_Controller</b>
 * <b>actionPerformed:</b>
 * Hier wird zuerst mit einer if-else-Abfrage eruiert, wie weiter vorzugehen ist.
 * Wird auf den Login-Button geklickt, so muss zuerst gepr�ft werden, ob beide
 * Felder ausgef�llt wurden. Wenn ja, kann <i>validation </i>vom Model ausgef�hrt
 * werden. Wird der Registrierungs-Button angeklickt, so muss die Methode
 * buildRegistrationMVC aufgerufen und Login-Fenster auf <i>disabled </i>gesetzt
 * werden.
 * <b>ok</b>
 * Sobald diese Methode aufgerufen wird, soll auf Model die Methode buildLobbyMVC
 * aufgerufen werden.
 * Meldung:
 * LoginOK|LoginOK
 * <b>passwordWrong</b>
 * Hier muss eine entsprechende Meldung auf dem GUI angezeigt werden.
 * Meldung:
 * LoginPasswordWrong|LoginPasswordWrong
 * <b>userNotFound</b>
 * Hier muss eine entsprechende Meldung auf dem GUI angezeigt werden.
 * Meldung:
 * LoginUserNotFound|LoginUserNotFound
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:14:29
 */
public class Login_Controller implements ActionListener, Constants_Login {

	private Login_Model model;
	private static Login_Controller singelton;
	private Login_View view;


	public Login_Controller(Login_Model model, Login_Controller singelton, Login_View view){
		this.model = model;
		this.singelton = singelton;
		this.view = view;
		
		this.model.add....
		
	}

	
	/**
	 * 
	 * @param view
	 * @param model
	 */
	private Login_Controller(Login_View view, Login_Model model){

	}

	/**
	 * 
	 * @param e
	 */
	public void actionPerformed(ActionEvent e){

	}

	public static Login_Controller getController(){
		return null;
	}

	/**
	 * 
	 * @param model
	 * @param view
	 */
	public static Login_Controller getController(Login_Model model, Login_View view){
		return null;
	}

	/**
	 * 
	 * @param message
	 */
	public void ok(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void passwordWrong(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void userNotFound(String message){

	}
}//end Login_Controller