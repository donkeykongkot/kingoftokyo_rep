package ch.fhnw.itprojekt15.DonkeyKong.GameRequest;

import ch.fhnw.itprojekt15.*;

/**
 * <b>GameRequest_Model:</b>
 * <b>sendGameRequest</b>
 * Hier sollen eine Spielanfrage an der Server verschickt werden. Hierf�r kann die
 * Methode send von der Klasse Server zur Hilfe beigezogen werden.
 * Meldung:
 * GameRequest|GameRequest
 * <b>buildGameBoardMVC</b>
 * Instanziiert das GameBoard-Objekt.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:13:28
 */
public class GameRequest_Model implements Constants_GameRequest {



	
	protected GameRequest_Model(){

	}

	protected void buildGameBoardMVC(){

	}

	protected void sendGameRequest(){

	}
}//end GameRequest_Model