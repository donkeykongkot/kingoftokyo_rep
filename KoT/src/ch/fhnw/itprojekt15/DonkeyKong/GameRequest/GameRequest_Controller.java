package ch.fhnw.itprojekt15.DonkeyKong.GameRequest;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ch.fhnw.itprojekt15.*;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Monsters;

/**
 * <b>GameRequest_Controller</b>
 * <b>actionPerformed:</b>
 * Wird auf den Play-Button geklickt, so muss die sendGameRequest-Methode vom
 * Model ausgef�hrt werden. Hierbei wird die Anfrage an den Server geschickt.
 * (Ablauf: <a href="{0gk37} \h')"><font color="#1155cc"><u>Use Case
 * 105</u></font></a>)
 * <b>countdown</b>
 * Sobald der Server den Countdown gestartet hat, wird diese Methode im
 * Sekundentakt aufgerufen, da der Server im Sekundentakt eine Meldung schickt,
 * die den Countdown und die Anzahl Spielanfrager miteilt. Das muss auf dem GUI
 * angezeigt werden (Siehe GUI-Spezifikation).
 * Meldung:
 * CountdownGameRequest|SekundeCountdown, AnzahlSpieler
 * <b>justOnePlayer</b>
 * Erh�lt der Client vom Server diese Meldung, so wird diese Methode hier
 * aufgerufen. Sie zeigt auf dem GUI an, das noch auf weitere Spielinteressenten
 * gewartet werden muss.
 * 
 * Meldung:
 * GameRequestJustOnePlayer|GameRequestJustOnePlayer
 * 
 * <b>startGame:</b>
 * Game kann gestart werden. Es wird mitgeliefert, welches Monster dem jeweiligen
 * Message-Empf�nger geh�rt und welche Monster mitspielen.
 * 
 * Die Monster werden vom Monster enum (interface) bezogen und der
 * MonsterArrayList eingef�gt. Gleichzeitig werden die Ruhmpunkte auf 0 und die
 * Leben auf 10 initialisiert. (setter Methoden des Enums). Energy wird auf 0
 * gesetzt. toykoOccupied wird bei allen auf false gesetzt. Ebenfalls wird beim
 * ersten Monster das in der Message mitgeliefert wird, der Wert der Monster-
 * Variable belongsToThisPlayer auf true gesetzt. Danach startet das Game, indem
 * die buildGameBoad-Methode ausgef�hrt wird.
 * 
 * Meldung:
 * GameStartGame| Monster des Spielers; Alle Monster die am spiel Teilnehmen
 * getrennt durch �,�
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:13:28
 */
public class GameRequest_Controller implements ActionListener, Constants_GameRequest {

	private GameRequest_Model model;
	private Monsters monsterList;
	private static GameRequest_Controller singelton;
	private GameRequest_View view;
	public GameRequest_View m_GameRequest_View;
	public GameRequest_Model m_GameRequest_Model;
	public Monsters m_Monsters;

	public GameRequest_Controller(){

	}

	
	/**
	 * 
	 * @param model
	 * @param view
	 */
	private GameRequest_Controller(GameRequest_Model model, GameRequest_View view){

	}

	/**
	 * 
	 * @param e
	 */
	public void actionPerformed(ActionEvent e){

	}

	/**
	 * 
	 * @param message
	 */
	public void countdown(String message){

	}

	public static GameRequest_Controller getController(){
		return null;
	}

	/**
	 * 
	 * @param model
	 * @param view
	 */
	public static GameRequest_Controller getController(GameRequest_Model model, GameRequest_View view){
		return null;
	}

	/**
	 * 
	 * @param message
	 */
	public void justOnePlayer(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void startGame(String message){

	}
}//end GameRequest_Controller