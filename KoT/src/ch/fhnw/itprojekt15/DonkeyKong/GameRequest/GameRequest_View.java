package ch.fhnw.itprojekt15.DonkeyKong.GameRequest;
import javax.swing.*;

import ch.fhnw.itprojekt15.*;

/**
 * <b>GameRequest_View:</b>
 * GameRequest_View erbt von der Klasse JPanel. Hier wird das in der
 * Anforderungsspezifikation aufgezeigte Teil-GUI erstellt. Die Texte der Elemente
 * werden vom Interface bezogen. Dieses Panel wird dann in der Lobby_View (JFrame)
 * angezeigt.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:13:28
 */
public class GameRequest_View extends JPanel implements Constants_GameRequest {

	protected JLabel countdown;
	protected JLabel numberOfPlayer;
	protected JButton play;
	protected JLabel textCountdown;
	protected JLabel textNummberOfPlayer;
	protected JLabel textWait;



	
	public GameRequest_View(){

	}
}//end GameRequest_View