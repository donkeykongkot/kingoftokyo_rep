package ch.fhnw.itprojekt15.DonkeyKong.Registration;


import javax.swing.*;

import ch.fhnw.itprojekt15.*;
/**
 * <b>Registration_View:</b>
 * Registration_View erbt von der Klasse JFrame. Hier wird das in der
 * Anforderungsspezifikations aufgezeigte GUI erstellt. Die Texte der Elemente
 * werden vom inteface bezogen.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:15:56
 */
public class Registration_View extends JFrame implements Constants_Registration {

	protected JLabel message;
	protected JButton next;
	protected JPasswordField password;
	protected JPasswordField passwordRepeat;
	protected JTextField username;



	
	public Registration_View(){

	}
}//end Registration_View