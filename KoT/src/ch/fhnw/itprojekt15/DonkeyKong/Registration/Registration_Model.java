package ch.fhnw.itprojekt15.DonkeyKong.Registration;


import ch.fhnw.itprojekt15.*;
/**
 * <b>Registration_Model:</b>
 * <b>buildLobbyMVC</b>
 * Lobby soll instanziert werden.
 * <b>validation</b>
 * Hier sollen die Registration-Daten an den Server geschickt werden. Hierf�r kann
 * die Methode send von der Klasse Server zur Hilfe beigezogen werden.
 * Meldung:
 * RegistrationValidation|Benutzername,Passwort
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:15:56
 */
public class Registration_Model implements Constants_Registration {

	private char[] passwort;
	private String username;



	
	public Registration_Model(){

	}

	protected void buildLobbyMVC(){

	}

	/**
	 * 
	 * @param password
	 * @param username
	 */
	protected void validation(char[] password, String username){
		this.passwort = password;
		this.username = username;
	}
}//end Registration_Model