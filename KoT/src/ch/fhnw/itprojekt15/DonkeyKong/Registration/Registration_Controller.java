package ch.fhnw.itprojekt15.DonkeyKong.Registration;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ch.fhnw.itprojekt15.*;
/**
 * <b>Registration_Controller:</b>
 * <b>actionPerformed:</b>
 * Wird auf den Weiter-Button geklickt, so muss zuerst gepr�ft werden:
 * <i>WENN</i>: Wiederholung des Passwortes nicht identisch mit Passwort
 * <i>DANN</i>: 	Meldung informiert, dass die Passw�rter nicht �bereinstimmen.
 * <i>SONST</i>: 	Es wird die Methode validation vom Model ausgef�hrt.
 * <b>ok</b>
 * Sobald diese Methode aufgerufen wird, soll auf Model die Methode buildLobbyMVC
 * aufgerufen werden. Das Registrationsfenster muss disabled werden.
 * Meldung:
 * RegistrationOK|RegistrationOK
 * <b>userAlreadyExists</b>
 * Es muss auf dem GUI eine Meldung erscheinen, dass der Username bereits vergeben
 * ist. Eingegebene Daten in den JTextField m�ssen geleert werden.
 * Meldung:
 * RegistrationUserAlreadyExists|RegistrationUserAlreadyExists
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:15:56
 */
public class Registration_Controller implements ActionListener, Constants_Registration {

	private Registration_Model model;
	private static Registration_Controller singelton;
	private Registration_View view;
	public Registration_View m_Registration_View;
	public Registration_Model m_Registration_Model;

	public Registration_Controller(){

	}

	
	/**
	 * 
	 * @param view
	 * @param model
	 */
	private Registration_Controller(Registration_View view, Registration_Model model){

	}

	/**
	 * 
	 * @param e
	 */
	public void actionPerformed(ActionEvent e){

	}

	public static Registration_Controller getController(){
		return null;
	}

	/**
	 * 
	 * @param view
	 * @param model
	 */
	public static Registration_Controller getController(Registration_View view, Registration_Model model){
		return null;
	}

	/**
	 * 
	 * @param message
	 */
	public void ok(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void userAlreadyExists(String message){

	}
}//end Registration_Controller