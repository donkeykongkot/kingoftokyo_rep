package ch.fhnw.itprojekt15.DonkeyKong.TickerBoard;


import ch.fhnw.itprojekt15.*;
/**
 * in-Methoden
 * Je nach dem welche Message vom MessagHandelr-Int �bergeben wurde, wird eine
 * entsprechende Meldung auf dem GUI angezeigt. Die Zusammensetzung findet aus den
 * einzelnen Textbausteinen statt, die sich auf dem Interface befinden. Sie werden
 * durch die Methode auf dem Model zusammengesetzt. Diese Bausteine sind noch
 * genauer zu spezifizieren.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:16:46
 */
public class TickerBoard_Controller implements Constants_Tickerboad {

	private TickerBoard_Model model;
	private static TickerBoard_Controller singelton;
	private TickerBoard_View view;
	public TickerBoard_View m_TickerBoard_View;
	public TickerBoard_Model m_TickerBoard_Model;

	public TickerBoard_Controller(){

	}

	
	/**
	 * 
	 * @param model
	 * @param view
	 */
	private TickerBoard_Controller(TickerBoard_Model model, TickerBoard_View view){

	}

	/**
	 * 
	 * @param message
	 */
	public void activePlayer(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void cardBought(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void cardBuyAnswer(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void cardNewCards(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void cardOneNewCards(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void connectionLost(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void diceRoll(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void diceRollFinal(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void diceTokyoBonus(String message){

	}

	public static TickerBoard_Controller getController(){
		return null;
	}

	/**
	 * 
	 * @param model
	 * @param view
	 */
	public static TickerBoard_Controller getController(TickerBoard_Model model, TickerBoard_View view){
		return null;
	}

	/**
	 * 
	 * @param message
	 */
	public void playerLeave(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void playerSuspended(String message){

	}

	/**
	 * 
	 * @param message
	 */
	public void tokyoLeaveAnswer(String message){

	}
}//end TickerBoard_Controller