package ch.fhnw.itprojekt15.DonkeyKong.Highscore;


import javax.swing.*;

import ch.fhnw.itprojekt15.*;

/**
 * <b>Highscore_View:</b>
 * Highscore_View erbt von der Klasse JPanel. Hier wird das in der
 * Anforderungsspezifikation aufgezeigte Teil-GUI erstellt. Die Texte der Elemente
 * werden vom Interface bezogen. Dieses Panel wird dann in der Lobby_View (JFrame)
 * angezeigt.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:13:48
 */
public class Highscore_View extends JPanel implements Constants_Highscore {

	protected String[] column;
	protected JButton refresh;
	protected String[][] row;
	protected JTable table;



	
	public Highscore_View(){

	}
}//end Highscore_View