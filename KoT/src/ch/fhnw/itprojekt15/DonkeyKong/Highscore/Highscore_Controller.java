package ch.fhnw.itprojekt15.DonkeyKong.Highscore;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ch.fhnw.itprojekt15.*;

/**
 * <b>Highscore_Controller</b>
 * <b>Konstruktor:</b>
 * Beim Erstellen des Highscore Controllers wird �ber die Methode requestData()
 * des Models ein Request an den Server abgesetzt. Dieser liefert die Highscore-
 * Daten zur�ck. Um den Datenverkehr zu minimieren, werden diese also nur einmalig
 * geliefert. K�nnen aber mit einem Aktualisieren-Button interaktiv neu
 * angefordert werden.
 * <b>actionPerformed</b>
 * Ruft die Methode requestData auf dem Model auf.
 * <b>sendData</b>
 * Sobald diese Methode aufgerufen wird, m�ssen die Daten aus der �bergebenen
 * Message auf dem GUI in einer Tabelle angezeigt werden.
 * Meldung:
 * HighscoreSendData|xxxx,xxxx,x,xxxxx;xxxx,xxxx,x,xxxxx;�..
 * Attribute werden durch "," getrennt, Datens�tze werden mit ";" getrennt
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:13:48
 */
public class Highscore_Controller implements ActionListener, Constants_Highscore {

	private Highscore_Model model;
	private static Highscore_Controller singelton;
	private Highscore_View view;
	public Highscore_View m_Highscore_View;
	public Highscore_Model m_Highscore_Model;

	public Highscore_Controller(){

	}

	
	/**
	 * 
	 * @param model
	 * @param view
	 */
	public Highscore_Controller(Highscore_Model model, Highscore_View view){

	}

	/**
	 * 
	 * @param e
	 */
	public void actionPerformed(ActionEvent e){

	}

	/**
	 * Konstruktor:
	 * Beim erstell
	 */
	public static Highscore_Controller getController(){
		return null;
	}

	/**
	 * 
	 * @param model
	 * @param view
	 */
	public static Highscore_Controller getController(Highscore_Model model, Highscore_View view){
		return null;
	}

	/**
	 * 
	 * @param message
	 */
	public void sendData(String message){

	}
}//end Highscore_Controller