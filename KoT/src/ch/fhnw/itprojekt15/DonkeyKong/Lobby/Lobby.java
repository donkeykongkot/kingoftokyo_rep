package ch.fhnw.itprojekt15.DonkeyKong.Lobby;
import javax.swing.*;

import ch.fhnw.itprojekt15.*;
import ch.fhnw.itprojekt15.DonkeyKong.GameRequest.GameRequest_View;
import ch.fhnw.itprojekt15.DonkeyKong.Highscore.Highscore_View;
import ch.fhnw.itprojekt15.DonkeyKong.PlayerList.PlayerList_View;

/**
 * <b>getLobby</b>
 * Gibt das Lobby-Objekt zur�ck, wenn es bereits instanziiert wurde oder ruft
 * sonst den Konstruktor auf. (singelton-Konzept)
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:14:03
 */
public class Lobby extends JFrame {

	private JPanel gameRequestMVC;
	private JPanel highscore;
	private JPanel playerList;
	private static Lobby singelton;
	public GameRequest_View m_GameRequest_View;
	public PlayerList_View m_PlayerList_View;
	public Highscore_View m_Highscore_View;




	public Lobby(){

	}

	public static Lobby getLobby(){
		return null;
	}
}//end Lobby