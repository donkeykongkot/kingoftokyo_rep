package ch.fhnw.itprojekt15.DonkeyKong.PlayerList;

/**
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 14:10:37
 */
public interface Constants_PlayerList {

	public static String delimiter1 = "|";
	public static String delimiter2 = ",";
	public static String delimiter3 = ";";
	public static final String textButtonRefresh = "Tabelle aktualisieren";
	public static final String textGuiName = "Angemeldete Spieler";
	public static final String[] columnName = {"Benutzername","Online seit"};
	public static final String serviceRequestData = "PlayerListRequestData"; //EA
	public static String messageRequestData = "PlayerListRequestData"; //EA

}