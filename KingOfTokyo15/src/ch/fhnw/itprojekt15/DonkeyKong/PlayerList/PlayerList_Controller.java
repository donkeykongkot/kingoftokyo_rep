package ch.fhnw.itprojekt15.DonkeyKong.PlayerList;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.table.DefaultTableModel;

/**
 * Beim Erstellen des PlayerList_Controllers wird �ber die Methode requestData()
 * des Models ein Request an den Server abgesetzt. Dieser liefert die PlayerList-
 * Daten zur�ck. Um den Datenverkehr zu minimieren, werden diese also nur einmalig
 * geliefert. K�nnen aber mit einem Aktualisieren-Button interaktiv neu
 * angefordert werden. Ebenfalls werden auf dem WindowsListener der Lobby die Daten neu angefordert.
 *
 * Sobald die sendData() Methode aufgerufen wird, m�ssen die Daten aus der �bergebenen
 * Message auf dem GUI in einer Tabelle angezeigt werden.
 * Meldung:
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 14:10:37
 */
public class PlayerList_Controller implements ActionListener, Constants_PlayerList {

	private PlayerList_Model model;
	private static PlayerList_Controller singelton;
	private PlayerList_View view;

	public PlayerList_Controller(PlayerList_Model model, PlayerList_View view){
		this.model = model;
		this.view = view;
		
		this.view.refresh.addActionListener(this);
		this.model.requestData();
	}
	
	//Gibt das PlayerList_Controller-Objekt zur�ck oder instanziiert sonst eines.
	public static PlayerList_Controller getController(PlayerList_Model model, PlayerList_View view){
		if(singelton == null){
			singelton = new PlayerList_Controller(model,view);
			return singelton;
		}
		
		return singelton;
	}
	
	//Gibt PlayerList_Controller-Objekt zur�ck
	public static PlayerList_Controller getController(){
		
		return singelton;
	}
	
	//Aktualisieren-Button
	public void actionPerformed(ActionEvent e){
		this.model.requestData();
	}

	//Methode wird vom WindowListener in der Lobby aufgerufen.
	public void requestData(){
		this.model.requestData();
	}
	
	//MessageIN: Die Daten vom Server werden in die Tabelle abgef�llt.
	//Beispiel-Message: PlayerListSendData|xxxx,xxxx;xxxx,xxxx;�..
	public void sendData(String message){
		this.view.tablePane.setModel(new DefaultTableModel(columnName,0));
		this.view.table = (DefaultTableModel) this.view.tablePane.getModel();
		
		
		Scanner rowScanner = new Scanner(message);
		rowScanner.useDelimiter(delimiter3);
						
		while(rowScanner.hasNext()){
			
			Scanner valueScanner = new Scanner(rowScanner.next());
			valueScanner.useDelimiter(delimiter2);
			
			String[] rowValue = {valueScanner.next(),valueScanner.next()};
			
			this.view.table.addRow(rowValue);
		}
	}
	
}//end PlayerList_Controller