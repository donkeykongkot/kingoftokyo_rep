package ch.fhnw.itprojekt15.DonkeyKong.PlayerList;

import java.io.IOException;

import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Server;

/**
 * Hier sollen die PlayerList-Daten vom Server verlangt werden. Hierf�r kann die
 * Methode send von der Klasse Server zur Hilfe beigezogen werden.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 14:10:37
 */
public class PlayerList_Model implements Constants_PlayerList {

	public PlayerList_Model(){

	}

	protected void requestData(){
		String message;
		message = serviceRequestData + delimiter1 + messageRequestData;
		Server.getServer().send(message);
		
	}
}//end PlayerList_Model