package ch.fhnw.itprojekt15.DonkeyKong.Registration;

import ch.fhnw.itprojekt15.*;
/**
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:15:56
 */
public interface Constants_Registration {

	public static final String delimiter1 = "|";
	public static final String delimiter2 = ",";
	public static final String pathPictureLogo = "logo_small.png";
	public static final String serviceValidation = "RegistrationValidation"; 
	public static final String textButtonNext = "Registrieren!";
	public static final String textGuiName = "Registration";
	public static final String textPasswordFieldPassword = "Passwort";
	public static final String textPasswordFieldPasswordRepeat = "Passwort wiederholen";
	public static final String textPasswordNotIdentical = "Das Passwort stimmt nicht �berein.";
	public static final String textUsernameEmpty = "Benutzername fehlt!"; 
	public static final String textTextFieldUsername = "Benutzername";
	public static final String textUsernameOccupied = "Dieser Benutzername existiert bereits.";
	public static final String textPasswordEmpty = "Passwort fehlt!";
	
}