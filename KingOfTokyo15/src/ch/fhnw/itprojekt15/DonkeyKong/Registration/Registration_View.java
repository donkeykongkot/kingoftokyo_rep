package ch.fhnw.itprojekt15.DonkeyKong.Registration;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseListener;

import javax.swing.*;

/**
 * 
 * Registration_View erbt von der Klasse JFrame. Hier wird das in der
 * Anforderungsspezifikations aufgezeigte GUI erstellt. Die Texte der Elemente
 * werden vom inteface bezogen.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:15:56
 */
public class Registration_View extends JFrame implements Constants_Registration, FocusListener {

	protected JPanel jPNorth; 
	protected JPanel jPSouth; 
	protected JPanel[] areasJPSouth; 
	protected ImageIcon logoSmall;
	protected JLabel logo; 
	protected JLabel usernameJL; 
	protected JTextField username;	
	protected JLabel passwordJL; 
	protected JPasswordField password;	
	protected JLabel passwordRepeatJL; 
	protected JPasswordField passwordRepeat;
	protected JButton next;	
	protected JLabel message;

	
	public Registration_View(){
		//init JFrame
		super(textGuiName);
		this.setLayout(new BorderLayout());
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
						
		this.jPNorth = new JPanel(new FlowLayout());
		this.jPSouth = new JPanel(new GridLayout(0,1));
		this.areasJPSouth = new JPanel[8];		
		
		
		for(int i = 0; i < 8; i++){
			this.areasJPSouth[i] = new JPanel(new FlowLayout());
			JPanel jp = this.areasJPSouth[i];
			jPSouth.add(this.areasJPSouth[i]);
		}
		
		//Logo: Oben
		this.logoSmall = new ImageIcon(pathPictureLogo);
		this.logo = new JLabel(logoSmall);
		this.jPNorth.add(this.logo);
		Dimension size =this.jPNorth.getPreferredSize();
		size.width = 250;
		this.jPNorth.setPreferredSize(size);
		
		//Nun werden die Felder: Username, Passwort, und Passwort repeat erstellt und eingefügt:
		
		this.usernameJL = new JLabel(textTextFieldUsername);
		this.areasJPSouth[0].add(usernameJL);
		this.username = new JTextField();
		this.username.addFocusListener(this);
		this.username.setColumns(12);
		this.areasJPSouth[1].add(username);
	
		this.passwordJL = new JLabel(textPasswordFieldPassword);
		this.areasJPSouth[2].add(passwordJL);
		this.password = new JPasswordField();
		this.password.addFocusListener(this);
		this.password.setColumns(12);
		this.areasJPSouth[3].add(password);
		
		this.passwordRepeatJL = new JLabel(textPasswordFieldPasswordRepeat);
		this.areasJPSouth[4].add(passwordRepeatJL);
		this.passwordRepeat = new JPasswordField();
		this.passwordRepeat.addFocusListener(this);
		this.passwordRepeat.setColumns(12);
		this.areasJPSouth[5].add(passwordRepeat);
				
		//Weiter-Button
		this.next = new JButton(textButtonNext);
		this.areasJPSouth[6].add(next);
			
		//Message, die ausgewiesen werden, falls etwas mit Auth-Daten nicht stimmt.
		this.message = new JLabel(" ");
		this.message.setForeground(Color.RED);
		this.areasJPSouth[7].add(message);
		
		this.add(jPNorth,BorderLayout.NORTH);
		this.add(jPSouth,BorderLayout.SOUTH);
		
		this.pack();
		this.setVisible(true);
		
	}
		
	//Sobald man in die JTextFields's klickt, werden diese geleert.
	public void focusGained(FocusEvent arg0) {
		this.message.setText(" ");
		
	}

	public void focusLost(FocusEvent arg0) {}	
}//end Registration_View
