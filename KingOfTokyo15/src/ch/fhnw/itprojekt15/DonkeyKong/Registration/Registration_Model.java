package ch.fhnw.itprojekt15.DonkeyKong.Registration;


import java.io.IOException;

import ch.fhnw.itprojekt15.DonkeyKong.Lobby.Lobby;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.*;
/**
 * Hier sollen die Registration-Daten an den Server geschickt werden. Hierf�r kann
 * die Methode send von der Klasse Server zur Hilfe beigezogen werden.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:15:56
 */
public class Registration_Model implements Constants_Registration {

	private char[] passwort;
	private String username;

	public Registration_Model(){
	}

	//Lobby soll instanziert werden.
	protected void buildLobbyMVC(){
		Lobby.getLobby();
	}
	
	//Es wird �berpr�ft, ob Passw�rter �bereinstimmen
	protected boolean passwordProover(char[] password, char[] passwordRepeat){ //EA
		if (password.length == passwordRepeat.length){

			for (int i = 0; i < password.length;i++){
				if(password[i] != passwordRepeat[i]){
					return false;
			}}
			
			return true;
		} 
		
		return false;
	}
	
	
	//Hier wird das Passwort und der Username an Server �bergeben
	protected void validation(char[] password, String username){
		String message;
		message = serviceValidation + delimiter1;
		message += username + delimiter2;
		for (char c: password){
			message += c;
		}
		Server.getServer().send(message);
		
	}
}//end Registration_Model