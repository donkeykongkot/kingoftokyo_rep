package ch.fhnw.itprojekt15.DonkeyKong.Registration;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ch.fhnw.itprojekt15.*;
/**
 * Controller Registrations-Komponente
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:15:56
 */
public class Registration_Controller implements ActionListener, Constants_Registration {

	private Registration_Model model;
	private static Registration_Controller singelton;
	private Registration_View view;

	//Gibt das Registration_Controller-Objekt zur�ck oder instanziiert sonst eines.
	private Registration_Controller(Registration_View view, Registration_Model model){
		this.view = view;
		this.model = model;
		this.view.next.addActionListener(this);
	}
		
	//Gibt das Registration_Controller-Objekt zur�ck oder instanziiert sonst eines.
	public static Registration_Controller getController(Registration_View view, Registration_Model model){
		if(singelton == null){
			singelton = new Registration_Controller(view,model);
		}
		return singelton;
	}
	
	//Gibt Registration_Controller-Objekt zur�ck
	public static Registration_Controller getController(){
		return singelton;
	}
	
	//Wird auf den Weiter-Button geklickt, so muss zuerst gepr�ft werden:
	//Ob Username abgef�llt ist, ob beide Passwort-Felder leer sind, oder ob Passw�rter �berhaupt gleich sind.
	//Danach wird ein Validation-Request an den Server verschickt (macht Model)
	public void actionPerformed(ActionEvent e){
		String username = this.view.username.getText();
		char[] password = this.view.password.getPassword();
		char[] passwordRepeat = this.view.passwordRepeat.getPassword();
		
		if (username.equals("")){
			this.view.message.setText(textUsernameEmpty);
			this.view.pack();
		} else if (password.length == 0){
			this.view.message.setText(textPasswordEmpty);
			this.view.pack();
		} else if (this.model.passwordProover(password,passwordRepeat)){
			this.model.validation(passwordRepeat, username);
		} else {
			this.view.message.setText(textPasswordNotIdentical);
			this.view.pack();
		}
	}

	
	//Message-IN: Sobald diese Methode aufgerufen wird, soll auf Model die Methode buildLobbyMVC
	//aufgerufen werden. Das Registrationsfenster muss disabled werden.
		public void ok(String message){
		this.view.setVisible(false);
		this.model.buildLobbyMVC();
	}

	//MessageIN: Es muss auf dem GUI eine Meldung erscheinen, dass der Username bereits vergeben ist. 
	public void userAlreadyExists(String message){
		this.view.message.setText(textUsernameOccupied);
		this.view.pack();
	}
}//end Registration_Controller