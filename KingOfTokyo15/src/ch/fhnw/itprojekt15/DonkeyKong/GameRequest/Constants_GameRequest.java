package ch.fhnw.itprojekt15.DonkeyKong.GameRequest;

import ch.fhnw.itprojekt15.*;

/**
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:13:28
 */
public interface Constants_GameRequest {

	public static final String delimiter1 = "|"; 
	public static final String delimiter2 = ","; 
	public static final String delimiter3 = ";"; 
	public static String messageGameRequest = "GameRequest";
	public static String serviceGameRequest = "GameRequest";
	public static final String textGuiName = "Neues Spiel";
	public static final String textButtonPlay ="Spielen"; 
	public static String textLabelCountdown = "Countdown:";
	public static String textLabelNumberOfPlayers = "Spieler:"; 
	public static String textLabelWait = "Warte auf einen zweiten Mitspieler..."; //EA
	public static final String pathPictureLogo = "logo_small.png"; 
	

}