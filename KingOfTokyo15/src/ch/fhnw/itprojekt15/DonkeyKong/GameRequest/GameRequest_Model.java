package ch.fhnw.itprojekt15.DonkeyKong.GameRequest;

import java.io.IOException;
import java.util.Scanner;

import ch.fhnw.itprojekt15.*;
import ch.fhnw.itprojekt15.DonkeyKong.GameBoard.GameBoard;
import ch.fhnw.itprojekt15.DonkeyKong.Lobby.Lobby;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Constants_Monster;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Monsters;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Constants_Monster.Monster;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Server;

/**
 * Hier sollen eine Spielanfrage an der Server verschickt werden. Hierf�r kann die
 * Methode send von der Klasse Server zur Hilfe beigezogen werden.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:13:28
 */
public class GameRequest_Model implements Constants_GameRequest, Constants_Monster {



	
	public GameRequest_Model(){

	}

	//Instanziiert das GameBoard-Objekt
	protected void buildGameBoardMVC(){
	
		Lobby.getLobby().disableLobby();
		GameBoard.getGameBoard().setVisible(true);
	}
	
	
	//Die �bergebenen Monster, werden in der Monster-ArrayList hinzugef�gt
	protected void fillMonsters(String message){
		Scanner scanner = new Scanner(message);
		scanner.useDelimiter(Constants_GameRequest.delimiter3);
		String myMonster = scanner.next();
		
		Monster.valueOf(myMonster).setBelongsToThisPlayer(true);
		
		while(scanner.hasNext()){
			Scanner monsterSet = new Scanner(scanner.next());
			monsterSet.useDelimiter(Constants_GameRequest.delimiter2);
			
			Monster monster = Monster.valueOf(monsterSet.next());
			Monsters.getMonsters().add(monster);
			
			monster.setUsername(monsterSet.next());
		}
	}

	//Message an Server, das der Spieler spielen m�chte
	protected void sendGameRequest(){
		String message;
		message = serviceGameRequest + Constants_GameRequest.delimiter1 + messageGameRequest;
		Server.getServer().send(message);
		
	}
}//end GameRequest_Model