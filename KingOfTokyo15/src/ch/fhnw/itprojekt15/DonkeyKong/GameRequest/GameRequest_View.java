package ch.fhnw.itprojekt15.DonkeyKong.GameRequest;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.*;
import javax.swing.border.Border;

import ch.fhnw.itprojekt15.*;

/**
 * GameRequest_View erbt von der Klasse JPanel. Hier wird das in der
 * Anforderungsspezifikation aufgezeigte Teil-GUI erstellt. Die Texte der Elemente
 * werden vom Interface bezogen. Dieses Panel wird dann in der Lobby_View (JFrame)
 * angezeigt.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:13:28
 */
public class GameRequest_View extends JPanel implements Constants_GameRequest {

	protected JLabel countdown;
	protected JLabel numberOfPlayers;
	protected JButton play;
	protected JLabel textCountdown;
	protected JLabel textNumberOfPlayers;
	protected JLabel textWait;
	protected JPanel[] panels;
	protected Dimension[] sizes;
	protected ImageIcon logoSmall;
	protected JLabel logo;


	public GameRequest_View(){
		//Init JPanel
		this.setBorder(BorderFactory.createTitledBorder(textGuiName));
		this.setLayout(new BorderLayout());
		
		this.panels = new JPanel[7];
		this.sizes = new Dimension[7];
		
		//Init Anordnung
		for(int i = 0; i<7;i++){
			this.panels[i] = new JPanel();
			this.sizes[i] = this.panels[i].getPreferredSize();
			this.panels[i].setLayout(new GridLayout(0,1));
		}
		
		//Button: Links
		this.sizes[0].width = 150;
		this.sizes[0].height = 100;
		this.panels[0].setPreferredSize(this.sizes[0]);
		this.play = new JButton(textButtonPlay);
		this.play.setBackground(new Color(204, 0, 0));
		this.play.setForeground(new Color(255,237,0));
		this.play.setFont((new Font(null,Font.BOLD, 30)));
		this.play.setBorder(BorderFactory.createLineBorder(new Color(255,237,0)));
		this.panels[0].add(this.play);
		this.add(panels[0], BorderLayout.WEST);
		
		//Container f�r Countdown etc. : Mitte
		this.panels[1].setLayout(new BorderLayout());
		this.panels[1].setBorder(BorderFactory.createEmptyBorder(0, 30, 0, 30));
		this.add(panels[1], BorderLayout.CENTER);
				
		//Logo: Rechts
		this.sizes[2].width = 150;
		this.sizes[2].height =100;
		this.panels[2].setPreferredSize(this.sizes[2]);
		this.logoSmall = new ImageIcon(getClass().getResource(pathPictureLogo));
		this.logo = new JLabel(logoSmall, SwingConstants.RIGHT);
		this.panels[2].add(this.logo);
		this.add(panels[2], BorderLayout.EAST);
				
		//Warte-Text
		this.textWait = new JLabel(" ");
		this.panels[3].add(this.textWait);
		this.panels[1].add(this.panels[3], BorderLayout.NORTH);
		
		//Countdown-Anzeige
		this.sizes[4].width = 150;
		this.panels[4].setPreferredSize(this.sizes[4]);
		this.panels[4].setLayout(new BoxLayout(this.panels[4], BoxLayout.PAGE_AXIS));
		this.textCountdown = new JLabel(" ");
		this.panels[4].add(this.textCountdown);
		this.countdown = new JLabel(" ");
		this.countdown.setFont(new Font(null, Font.PLAIN, 40));
		this.panels[4].add(this.countdown);
		this.panels[1].add(this.panels[4], BorderLayout.WEST);
		
		//Anzeige Anzahl-Spieler
		this.sizes[5].width = 150;
		this.panels[5].setPreferredSize(this.sizes[5]);
		this.panels[5].setLayout(new BoxLayout(this.panels[5], BoxLayout.PAGE_AXIS));
		this.textNumberOfPlayers = new JLabel(" ");
		this.panels[5].add(this.textNumberOfPlayers);
		this.numberOfPlayers = new JLabel(" ");
		this.numberOfPlayers.setFont(new Font(null, Font.PLAIN, 40));
		this.panels[5].add(this.numberOfPlayers);
		this.panels[1].add(this.panels[5], BorderLayout.CENTER);
		
		//Leere Anzeige
		this.sizes[6].width = 150;
		this.panels[6].setPreferredSize(this.sizes[6]);
		this.panels[1].add(this.panels[6], BorderLayout.EAST);
		
	}
}//end GameRequest_View