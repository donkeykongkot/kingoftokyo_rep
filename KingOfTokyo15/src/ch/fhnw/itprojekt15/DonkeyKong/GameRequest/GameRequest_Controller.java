package ch.fhnw.itprojekt15.DonkeyKong.GameRequest;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import ch.fhnw.itprojekt15.*;
import ch.fhnw.itprojekt15.DonkeyKong.Highscore.Highscore_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.Lobby.Lobby;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Constants_Monster;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Monsters;
import ch.fhnw.itprojekt15.DonkeyKong.PlayerList.PlayerList_Controller;

/**
 * 
 * Wird auf den Play-Button geklickt, so muss die sendGameRequest-Methode vom
 * Model ausgef�hrt werden. Hierbei wird die Anfrage an den Server geschickt.
 
 * Sobald der Server den Countdown gestartet hat, wird diese Methode im
 * Sekundentakt aufgerufen, da der Server im Sekundentakt eine Meldung schickt,
 * die den Countdown und die Anzahl Spielanfrager miteilt. Das muss auf dem GUI
 * angezeigt werden (Siehe GUI-Spezifikation).
 * 
 * Erh�lt der Client vom Server die justOnePlayer Meldung, so wird diese Methode hier
 * aufgerufen. Sie zeigt auf dem GUI an, das noch auf weitere Spielinteressenten
 * gewartet werden muss.
 * 
 * Game kann gestart werden. Es wird mitgeliefert, welches Monster dem jeweiligen
 * Message-Empf�nger geh�rt und welche Monster mitspielen (inkl. effektiver Username).
 * 
 * Die Monster werden vom Monster enum (interface) bezogen und der
 * MonsterArrayList eingef�gt.Ebenfalls wird beim ersten Monster das in der Message mitgeliefert wird, der Wert der Monster-
 * Variable belongsToThisPlayer auf true gesetzt. Danach startet das Game, indem
 * die buildGameBoad-Methode ausgef�hrt wird.
 * 
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:13:28
 */
public class GameRequest_Controller implements ActionListener, Constants_GameRequest {

	private GameRequest_Model model;
	private Monsters monsterList;
	private static GameRequest_Controller singelton;
	private GameRequest_View view;
	public Monsters m_Monsters;


	private GameRequest_Controller(GameRequest_Model model, GameRequest_View view){ //EA
		this.model = model;
		this.view = view;
		
		this.view.play.addActionListener(this);
	}
	
	
	//Gibt GameRequest_Controller-Objekt zur�ck, wenn es nicht bereits instanziiert ist
	public static GameRequest_Controller getController(GameRequest_Model model, GameRequest_View view){
		if(singelton == null){
			singelton = new GameRequest_Controller(model,view);
			return singelton;
		}
		return singelton;
	}
	
	
	//Gibt GameRequest_Controller-Objekt zur�ck
	public static GameRequest_Controller getController(){
		return singelton;
	}

	
	//Button wird deaktiviert. Spieler m�chte Game spielen
	public void actionPerformed(ActionEvent e){
		this.view.play.setEnabled(false);
		Lobby.getLobby().setGameActiv(true);
		this.model.sendGameRequest();
		
	}

	//MessageIN: Countdown und Anzahl Spieler f�r GameRequest vom Server wird auf GUI angezeigt
	//Beispiel-Message: CountdownGameRequest|15,3 --> 15,3
	public void countdown(String message){
		this.view.textWait.setText("");
		
		Scanner scannerCountdown = new Scanner(message);
		scannerCountdown.useDelimiter(delimiter2);
		
		this.view.textCountdown.setText(textLabelCountdown);
		this.view.countdown.setText(scannerCountdown.next());
		
		this.view.textNumberOfPlayers.setText(textLabelNumberOfPlayers);
		this.view.numberOfPlayers.setText(scannerCountdown.next());
	}


	//MessageIN: Erst ein Spieler m�chte spielen
	//Beispiel-Message: GameRequestJustOnePlayer|GameRequestJustOnePlayer --> GameRequestJustOnePlayer
	public void justOnePlayer(String message){
		this.view.textCountdown.setText(" ");
		this.view.textNumberOfPlayers.setText(" ");
		this.view.countdown.setText(" ");
		this.view.numberOfPlayers.setText(" ");
		this.view.textWait.setText(textLabelWait);
	}

	//Message: Game kann gestartet werden. Monster werden in ArrayList gespeichert (Model), GameBoard wird ge�ffnet (Model)
	//Beispiel-Message: GameStartGame|ALIENOID;CYBERBUNNY,Username1;ALIENOID,Username2;GIGAZAUR,Username3
	public void startGame(String message){
		this.view.textCountdown.setText(" ");
		this.view.textNumberOfPlayers.setText(" ");
		this.view.countdown.setText(" ");
		this.view.numberOfPlayers.setText(" ");
		this.view.textWait.setText("");
		this.view.play.setEnabled(true);
		
		this.model.fillMonsters(message);
		this.model.buildGameBoardMVC();
	}
}//end GameRequest_Controller