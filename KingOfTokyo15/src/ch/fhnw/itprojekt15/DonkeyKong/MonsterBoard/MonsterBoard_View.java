package ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.*;
import javax.swing.border.Border;

import ch.fhnw.itprojekt15.*;
/**
 * Konstruktor:
 * Mit dem Aufruf des Konstruktors werden alle 6 JPanels (MonsterSlots) generiert und sich selbst
 * eingefügt. Die Monster im Game werden erst im Controller mit der Methode updateMonsterView() eingefügt.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:14:50
 */
public class MonsterBoard_View extends JPanel implements Constants_Monster {

	JPanel[] monsterSlots, monsterWindows, monsterPoints;
	JLabel[] monsterTitel, monsterImages, imageVictoryPoints, monsterVictoryPoints, imageLifes, monsterLifes, imageEnergy, monsterEnergy;
	
	
		
	public MonsterBoard_View(){
		this.setBorder(BorderFactory.createTitledBorder(textGuiName));
		this.setLayout(new GridLayout(2,3));
		this.monsterSlots =  new JPanel[6];
		this.monsterWindows =  new JPanel[6];
		this.monsterPoints =  new JPanel[6];
		this.monsterTitel = new JLabel[6];
		this.monsterImages = new JLabel[6];
		this.imageVictoryPoints = new JLabel[6];
		this.monsterVictoryPoints = new JLabel[6];
		this.imageLifes = new JLabel[6];
		this.monsterLifes = new JLabel[6];
		this.imageEnergy = new JLabel[6];
		this.monsterEnergy = new JLabel[6];
		
		//Sechs Monsterslots werden erstellt. Zwei Reihen, drei Spalten
		for(int i = 0; i < 6; i++){
			this.monsterSlots[i] = new JPanel();
			this.monsterSlots[i].setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
						
			//MonsterWindow wird im MonsterSlot eingefügt.
			this.monsterWindows[i] = new JPanel();
			this.monsterWindows[i].setLayout(new BorderLayout());
			this.monsterWindows[i].setBorder(BorderFactory.createLineBorder(Color.BLACK,2));
			Dimension mWSize = this.monsterWindows[i].getPreferredSize();
			mWSize.setSize(260, 210);
			this.monsterWindows[i].setPreferredSize(mWSize);
			this.monsterSlots[i].add(this.monsterWindows[i]);
			
			//Monstername und Username: Oben im Slot
			this.monsterTitel[i] = new JLabel(textLabelNoMonster);
			this.monsterTitel[i].setFont(new Font(null,Font.BOLD,20));
			this.monsterTitel[i].setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
			this.monsterTitel[i].setOpaque(true);
			this.monsterWindows[i].add(this.monsterTitel[i], BorderLayout.NORTH);
			
			//Monsterbild: Unten links im Slot
			this.monsterImages[i] = new JLabel(" ");
			Dimension mISize = this.monsterImages[i].getPreferredSize();
			mISize.setSize(175, 175);
			this.monsterImages[i].setPreferredSize(mISize);
			this.monsterWindows[i].add(this.monsterImages[i], BorderLayout.WEST);
			
			//Die Monsterpunkte und die Punkte-Icons (Herz, Stern, Blitz) werden im JPanel monsterPoint angezeigt: unten rechts  im Slot
			this.monsterPoints[i] = new JPanel();
			this.monsterPoints[i].setLayout(new GridLayout(3,2));
			this.monsterWindows[i].add(this.monsterPoints[i], BorderLayout.CENTER);
			
			this.monsterPoints[i].add((this.imageVictoryPoints[i] = new JLabel(" ")));
			this.monsterPoints[i].add((this.monsterVictoryPoints[i] = new JLabel(" ", SwingConstants.CENTER)));
			this.monsterVictoryPoints[i].setFont(new Font(null,Font.BOLD,20));
			this.monsterVictoryPoints[i].setOpaque(true);
			this.monsterPoints[i].add((this.imageLifes[i] = new JLabel(" ")));
			this.monsterPoints[i].add((this.monsterLifes[i] = new JLabel(" ", SwingConstants.CENTER)));
			this.monsterLifes[i].setFont(new Font(null,Font.BOLD,20));
			this.monsterLifes[i].setOpaque(true);
			this.monsterPoints[i].add((this.imageEnergy[i] = new JLabel(" ")));
			this.monsterPoints[i].add((this.monsterEnergy[i] = new JLabel(" ", SwingConstants.CENTER)));
			this.monsterEnergy[i].setFont(new Font(null,Font.BOLD,20));
			this.monsterEnergy[i].setOpaque(true);
			
			this.add(monsterSlots[i]);
						
		}
	
		
	}
	
	
}//end MonsterBoard_View
