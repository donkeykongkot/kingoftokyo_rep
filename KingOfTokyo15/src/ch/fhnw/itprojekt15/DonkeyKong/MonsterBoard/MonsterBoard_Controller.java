package ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard;

import java.awt.Color;
import java.io.Serializable;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

import ch.fhnw.itprojekt15.*;

/**
 *In dieser Klasse werden die Monster clientseitig verwaltet.
 * 
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:14:50
 */
public class MonsterBoard_Controller implements Constants_Monster, Runnable {

	private MonsterBoard_Model model;
	private Monsters<Monster> monsterList;
	private static MonsterBoard_Controller singelton;
	private MonsterBoard_View view;
	private int arrayPosition; //Hilfszahl f�r Fade-Thread

	
	
	private MonsterBoard_Controller() {
		this.view = new MonsterBoard_View();
		this.model = new MonsterBoard_Model();
		this.monsterList = Monsters.getMonsters();
		this.updateMonsterView();
	}

	//Gibt aktuells MonsterBoard-Objekt zur�ck oder instanziert sonst ein Neues
	public static MonsterBoard_Controller getController() {
		if (singelton == null){
			singelton = new MonsterBoard_Controller();
			return singelton;
		}
		
		return singelton;
	}

	//Beim entsprechenden Monster im enum wird Wert gesetzt
	public void activePlayer(String message) {
		System.out.println("Monster: ActivePlayer: " + message);
		for (Monster monster: Monster.values()){
			if(monster.getActivePlayer()){
				monster.setActivePlayer(false);
			}
		}
		Monster.valueOf(message).setActivePlayer(true);
		this.updateMonsterView();
	}

	//Sobald ein Monster (Spieler) nicht mehr am Spiel teilnimmt, wird es aus der Monster-ArrayList entfernt
	public void connectionLost(String message) {
		if(!Monster.valueOf(message).getBelongsToThisPlayer()){
			this.monsterList.remove(Monster.valueOf(message));
			this.updateMonsterView();
		}
	}

	//Sobald ein Monster (Spieler) nicht mehr am Spiel teilnimmt, wird es aus der Monster-ArrayList entfernt
	public void playerLeave(String message) {
		if(!Monster.valueOf(message).getBelongsToThisPlayer()){
			this.monsterList.remove(Monster.valueOf(message));
			this.updateMonsterView();
		}
	}

	//Sobald ein Monster (Spieler) nicht mehr am Spiel teilnimmt, wird es aus der Monster-ArrayList entfernt
	public void playerSuspended(String message) {
		if(!Monster.valueOf(message).getBelongsToThisPlayer()){
			this.monsterList.remove(Monster.valueOf(message));
			this.updateMonsterView();
		}
	}

	//Punktestand des Monsters wird angepasst. Synchronized, weil diese Meldung realtiv h�ufig reinkommt.
	public  synchronized void  updatePlayerData(String message) {
		this.model.updateMonster(message);
		this.updateMonsterView();
				
	}

	//Sobald ein Monster (Spieler) nicht mehr am Spiel teilnimmt, wird es aus der Monster-ArrayList entfernt
	public void playerLost(String message) {
		if(!Monster.valueOf(message).getBelongsToThisPlayer()){
			this.monsterList.remove(Monster.valueOf(message));
			this.updateMonsterView();
		}
	}
	
	//Gibt View zur�ck (Wichtig f�r GameBoard)
	public MonsterBoard_View getGUI(){
		return this.view;
	}
	
	//Die View wird arrangiert. Sobald eine Eigenschaft eines Monsters ge�ndert hat, wird diese Methode aufgerufen.
	private void updateMonsterView(){
		
		//Anordnung der Monster, die in der Monster-ArrayList sind. In der Monster-ArrayList sind noch alle im Spiel verbleibenden Monster.
		for (int i = 0; i < monsterList.size(); i++){
			Monster monster = (Monster) monsterList.get(i);
			this.view.monsterTitel[i].setText(monster.name() + " (" + monster.getUsername() + ")");
			this.view.monsterImages[i].setIcon(monster.getImageIcon());
			this.view.imageVictoryPoints[i].setIcon(new ImageIcon(pathPictureVictoryPoints));
			this.view.imageEnergy[i].setIcon(new ImageIcon(pathPictureEnergy));
			this.view.imageLifes[i].setIcon(new ImageIcon(pathPictureLife));
			this.view.monsterVictoryPoints[i].setText("" + monster.getVicotryPoints());
			this.view.monsterLifes[i].setText("" + monster.getLife());
			this.view.monsterEnergy[i].setText("" + monster.getEnergy());
			
			//Blauer Rand und Hintergrund bei dem Monster, das diesem Client geh�rt.
			if (monster.getBelongsToThisPlayer()){
				this.view.monsterTitel[i].setBackground(new Color(203,233,243));
				this.view.monsterTitel[i].setBorder(BorderFactory.createLineBorder(new Color(88,177,245), 3));
				this.view.monsterWindows[i].setBackground(Color.WHITE);
				this.view.monsterPoints[i].setBackground(Color.WHITE);
				this.view.monsterEnergy[i].setBackground(Color.WHITE);
				this.view.monsterLifes[i].setBackground(Color.WHITE);
				this.view.monsterVictoryPoints[i].setBackground(Color.WHITE);
			} else {
				this.view.monsterWindows[i].setBackground(Color.WHITE);
				this.view.monsterPoints[i].setBackground(Color.WHITE);
				this.view.monsterTitel[i].setBackground(Color.WHITE);
				this.view.monsterTitel[i].setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
				this.view.monsterEnergy[i].setBackground(Color.WHITE);
				this.view.monsterLifes[i].setBackground(Color.WHITE);
				this.view.monsterVictoryPoints[i].setBackground(Color.WHITE);
			}
			
			//Gr�ner Rand bzw. Background bei dem Monster, dass im Moment an der Reihe ist.
			if (monster.getActivePlayer()){
				this.view.monsterSlots[i].setBackground(new Color(147,199,62)); //gr�n
			} else {
				this.view.monsterSlots[i].setBackground(null);
			}
			
			//Das Monster, das soeben (im Model) seine Eigenschaften ge�ndert hat werden Fade-Threads ausgel�st
			if (monster.equals(this.model.monster)){
				this.start(i);
			}
			
			
		}
		
		//Die restlichen MonsterSlots werden geblankt
		for (int i = monsterList.size(); i< 6; i++){
			this.view.monsterTitel[i].setText(textLabelNoMonster);
			this.view.monsterTitel[i].setBorder(BorderFactory.createEmptyBorder());
			this.view.monsterTitel[i].setBackground(null);
			this.view.monsterImages[i].setIcon(null);
			this.view.monsterImages[i].setText(" ");
			this.view.imageVictoryPoints[i].setIcon(null);
			this.view.imageVictoryPoints[i].setText(" ");
			this.view.imageEnergy[i].setIcon(null);
			this.view.imageEnergy[i].setText(" ");
			this.view.imageLifes[i].setIcon(null);
			this.view.imageLifes[i].setText(" ");
			this.view.monsterVictoryPoints[i].setText(" ");
			this.view.monsterLifes[i].setText(" ");
			this.view.monsterEnergy[i].setText(" ");
			this.view.monsterWindows[i].setBackground(null);
			this.view.monsterPoints[i].setBackground(null);
			this.view.monsterSlots[i].setBackground(null);
			this.view.monsterTitel[i].setBackground(null);
			this.view.monsterEnergy[i].setBackground(null);
			this.view.monsterLifes[i].setBackground(null);
			this.view.monsterVictoryPoints[i].setBackground(null);
		}
	}
	
	//Es werden drei Fade-Threads ausgel�st. F�r Victory, Life und Energy Punkte je einer
	private void start(int arrayPosition){
		new Thread(this,"victory" + this.model.victoryRaise + arrayPosition).start();
		new Thread(this,"life" + this.model.lifeRaise + arrayPosition).start();
		new Thread(this,"energy" + this.model.energyRaise + arrayPosition).start();
	}
	
	//Je nach dem, wie der ausgel�ste Thread heisst, wird auf dem GUI beim jeweiligen Punktestand ein Fade-Effekt ausgel�st. 
	public void run(){
		String whichThread = Thread.currentThread().getName();
		int arrayPosition = Integer.parseInt(whichThread.substring(whichThread.length()-1));
		whichThread = whichThread.substring(0,whichThread.length()-1);
		
		//Je nach Thread-Name wird ein Fade-Effekt (rot oder gr�n) ausgel�st oder eben nicht (wenn sich der Punktestand nicht ver�ndert hat)
		try {
			switch(whichThread){
				case "victory0": 
					break;
				case "victory-1":
					for (int i = 0; i < 256;i++){this.view.monsterVictoryPoints[arrayPosition].setBackground(new Color(255,i,i)); Thread.sleep(10);}
					break;
				case "victory1":
					for (int i = 0; i < 256;i++){this.view.monsterVictoryPoints[arrayPosition].setBackground(new Color(i,255,i)); Thread.sleep(10);}
					break;
				case "life0": 
					break;
				case "life-1":
					for (int i = 0; i < 256;i++){this.view.monsterLifes[arrayPosition].setBackground(new Color(255,i,i)); Thread.sleep(10);}
					break;
				case "life1":
					for (int i = 0; i < 256;i++){this.view.monsterLifes[arrayPosition].setBackground(new Color(i,255,i)); Thread.sleep(10);}
					break;	
				case "energy0": 
					break;
				case "energy-1":
					for (int i = 0; i < 256;i++){this.view.monsterEnergy[arrayPosition].setBackground(new Color(255,i,i)); Thread.sleep(10);}
					break;
				case "energy1":
					for (int i = 0; i < 256;i++){this.view.monsterEnergy[arrayPosition].setBackground(new Color(i,255,i)); Thread.sleep(10);}
					break;	
		}
		} catch (InterruptedException e) {
				System.out.println("Etwas funktioniert mit den Threads auf dem Monster-Controller nicht. Fade-In und Fade-Out der JLABELS.");
				}
	}
	
	//Wird diese Methode ausgel�st, muss beim Aufruf der get-Methode ein neues Objekt instanziert werden.
	public static void kill(){
		singelton = null;
	}

}// end MonsterBoard_Controller