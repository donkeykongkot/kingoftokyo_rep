package ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard;

import java.util.ArrayList;

import ch.fhnw.itprojekt15.*;
/**
 * Diese Monster-ArrayList soll jeweils diese Monster enthalten,
 * die zuerzeit am Spiel beteiligt sind.
 * 
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:14:50
 */
public class Monsters<Monster> extends ArrayList implements Constants_Monster {

	private static Monsters singelton;
	
	private Monsters(){
		super();
	}

	public static Monsters getMonsters(){
		if(singelton == null){
			singelton= new Monsters();
			return singelton;
		}
		return singelton;
	}
	
	public static void kill(){
		singelton = null;
	}


}//end Monsters