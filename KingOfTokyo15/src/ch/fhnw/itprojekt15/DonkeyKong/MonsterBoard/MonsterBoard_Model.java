package ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard;


import java.util.Scanner;

import ch.fhnw.itprojekt15.*;
/**
 * Hier werden die Eigenschaften der Monster angepasst.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:14:50
 */
public class MonsterBoard_Model implements Constants_Monster {

	private Monsters monsterList;
	protected int energyRaise;
	protected int lifeRaise;
	protected int victoryRaise;
	protected Monster monster;

	public MonsterBoard_Model(){
		this.energyRaise = 0;
		this.lifeRaise = 0;
		this.victoryRaise = 0;
	}

	
	//Hier werden die Punktest�nde des der Methode �bergebenen Monsters angepasst
	//Ebenfalls wird markiert, wie sich der Punktestand jeweils ver�ndert hat (f�r Fade-Effekte auf dem Controller)
	//1 --> Punktestand ging nach oben --> Fade-Effekt gr�n
	//-1 --> Punktestand ging nach unten --> Fade-Effekt rot
	//0 --> Punktestand hat sich nicht ver�ndert --> kein Fade-Effekt
	protected void updateMonster(String message){
		Scanner scanner = new Scanner(message);
		scanner.useDelimiter(delimiter2);
		
		this.monster = Monster.valueOf(scanner.next());
				
		int life = Integer.parseInt(scanner.next());
		int victory = Integer.parseInt(scanner.next());
		int energy = Integer.parseInt(scanner.next());
		
		if (life < this.monster.getLife()){
			this.lifeRaise = -1;
		} else if (life > this.monster.getLife()){
			this.lifeRaise = 1;
		} else {
			this.lifeRaise = 0;
		} this.monster.setLife(life);
		
		if (victory < this.monster.getVicotryPoints()){
			this.victoryRaise = -1;
		} else if (victory > this.monster.getVicotryPoints()){
			this.victoryRaise = 1;
		} else {
			this.victoryRaise = 0;
		} this.monster.setVictoryPoints(victory);
		
		if (energy < this.monster.getEnergy()){
			this.energyRaise = -1;
		} else if (energy > this.monster.getEnergy()){
			this.energyRaise = 1;
		} else {
			this.energyRaise = 0;
		} this.monster.setEnergy(energy);
	}


	
}//end MonsterBoard_Model