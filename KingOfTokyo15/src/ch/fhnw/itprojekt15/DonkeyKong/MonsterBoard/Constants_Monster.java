package ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import ch.fhnw.itprojekt15.*;

/**
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:14:50
 */
public interface Constants_Monster {
	public static final String iconPath = "src/ch/fhnw/itprojekt15/DonkeyKong/MonsterBoard/";
	public static final String textGuiName = "Monsters";
	public static final String textLabelNoMonster = "Leerer Monsterslot";
	public static final String pathPictureLife = iconPath + "Life_Small.png";
	public static final String pathPictureVictoryPoints = iconPath + "Fame_Small.png";
	public static final String pathPictureEnergy = iconPath + "Energy_Small.png";
	public static final String delimiter1 = "\\|";
	public static final String delimiter2 = ",";
	public static final String delimiter3 = ";";

	//Hier ein Enum mit allen Monstern und ihren Eigenschaften inkl getter und setter
	public static enum Monster {
		Alienoid("MonsterAlienoid_shine.png"),
		CyberBunny("MonsterCyberBunny_shine.png"),
		GigaZaur("MonsterGigaZaur_shine.png"),
		Kraken("MonsterKraken_shine.png"),
		MekaDragon("MonsterMekaDragon_shine.png"),
		TheKing("MonsterTheKing_shine.png");

		private String picturePath;
		private boolean tokyoOccupied;
		private int victoryPoints;
		private boolean belongsToThisPlayer;
		private int energy;
		private int life;
		private ImageIcon monsterImageIcon;
		private JLabel monsterImage;
		private boolean activePlayer;
		private String username;

		//
		/**
		 * 
		 * @param picturePath
		 */
		private Monster(String picturePath) {
			this.picturePath = iconPath + picturePath;
			this.monsterImageIcon = new ImageIcon(iconPath + picturePath);
			this.tokyoOccupied = false;
			this.victoryPoints = 0;
			this.energy = 0;
			this.life = 10;
			this.activePlayer = false;
			this.belongsToThisPlayer = false;
			this.username = "";

		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public boolean getActivePlayer() {
			return activePlayer;
		}

		public void setActivePlayer(boolean activePlayer) {
			this.activePlayer = activePlayer;
		}

		public boolean getBelongsToThisPlayer() {
			return this.belongsToThisPlayer;
		}

		public int getEnergy() {
			return this.energy;
		}

		public int getLife() {
			return this.life;
		}

		public String getPicturePath() {
			return this.picturePath;
		}

		public boolean getTokyoOccupied() {
			return this.tokyoOccupied;
		}

		public int getVicotryPoints() {
			return this.victoryPoints;
		}

		public JLabel getImage() {
			this.monsterImageIcon = this.getImageIcon();
			this.monsterImage = new JLabel(monsterImageIcon);
			return monsterImage;
		}

		public ImageIcon getImageIcon() {
			return this.monsterImageIcon;
		}

		/**
		 * 
		 * @param belongsToThisPlayer
		 */
		public void setBelongsToThisPlayer(boolean belongsToThisPlayer) {
			this.belongsToThisPlayer = belongsToThisPlayer;
		}

		/**
		 * 
		 * @param energy
		 */
		public void setEnergy(int energy) {
			this.energy = energy;
		}

		/**
		 * 
		 * @param life
		 */
		public void setLife(int life) {
			this.life = life;
		}

		/**
		 * 
		 * @param tokyo
		 */
		public void setTokyoOccupied(boolean tokyo) {
			this.tokyoOccupied = tokyo;
		}

		/**
		 * 
		 * @param victoryPoints
		 */
		public void setVictoryPoints(int victoryPoints) {
			this.victoryPoints = victoryPoints;
		}

		public static Monster getMyMonster() { 
			for (Monster m : Monster.values()) {
				if (m.belongsToThisPlayer) {
					return m;
				}
			}
			return null;
		}
		
		public static Monster getTokyoMonster() { 
			for (Monster m : Monster.values()) {
				if (m.tokyoOccupied) {
					return m;
				}
			}
			return null;
		}
		
		public static Monster getActiveMonster() { 
			for (Monster m : Monster.values()) {
				if (m.activePlayer) {
					return m;
				}
			}
			return null;
		}
		
		public static boolean checkENUMCompatibility(Object[] enumType, String value) {
			for (Object o : enumType) {
				if (o.toString().equals(value)) {
					return true;
				}
			}
			return false;
		}
	};



}