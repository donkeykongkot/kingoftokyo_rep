package ch.fhnw.itprojekt15.DonkeyKong.DiceBoard;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

import ch.fhnw.itprojekt15.*;
import ch.fhnw.itprojekt15.DonkeyKong.CardBoard.CardBoard_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.CardBoard.Constants_Cards.Card;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Constants_Monster.Monster;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Constants_Services.Service;

/**
 * Beim aktiven Player wird der Button "roll" aktiviert. Dieser kann nun
 * W�rfeln. Hierbei wird diceRoll vom Model ausgef�ht. Das Ergebnis geht an den
 * Server, welcher die Message an alle Spieler weiterleitet. Dies geschieht drei
 * Mal. Beim dritten Mal wird aber diceRollFinal vom Model ausgel�st und das
 * letztmalige W�rfeln signalisiert. <b> </b><b> </b><b>DiceBoard_Controller</b>
 * <b>actionPerformed</b> Zweimal w�rfeln, W�rfel behalten danach diceRoll auf
 * dem Model wird zweimal aufgerufen um Ergebnis an Server zu schicken. Letztes
 * mal w�rfel, W�rfel behalten danach diceRollFinal auf dem Model wird
 * aufgerufen um Ergebnis an Server zu schicken. <b>activePlayer</b> Hier muss
 * der Button roll aktiviert werden, insofern es sich um das Monster handelt,
 * das sich in der Meldung befindet. Diese kann man mit dem Monsters- Objekt
 * feststellen Meldung: GameActivePlayer|Monster <b>diceRoll</b> Die W�rfel auf
 * dem GUI m�ssen entsprechend der Meldung angezeigt werden. Meldung: DiceRoll|
 * W�rfelergebnis getrennt durch "," <b>diceRollFinal</b> Die W�rfel auf dem GUI
 * m�ssen entsprechend angezeigt werden. Meldung: DiceRollFinal| W�rfelergebnis
 * getrennt durch ","
 * 
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:12:30
 */
public class DiceBoard_Controller implements ActionListener, Constants_Dices {

	private static DiceBoard_Controller singleton;
	protected DiceBoard_Model model;
	protected DiceBoard_View view;

	public DiceBoard_Controller() {
		this.view = new DiceBoard_View();
		this.model = new DiceBoard_Model(this.view);
		this.view.roll.addActionListener(this);
		for (JButton dButton : this.view.diceButtons) {
			dButton.addActionListener(this);
		}
	}

	/**
	 * 
	 * @param message
	 */
	public void activePlayer(String message) {
		this.activateGUI();
		if (Monster.valueOf(message) == Monster.getMyMonster()) {
			this.view.roll.setEnabled(true);
		}
	}

	/**
	 * 
	 * @param message
	 */
	public void diceRoll(String message) {
		this.model.diceRoll(message);
	}

	/**
	 * Beim aktiven Player wird der Button "roll" aktiviert. Dieser kann nun
	 * W�rfeln. Hierbei wird diceRoll vom Model ausgef�ht. Das Ergebnis geht an
	 * den Server, dieser verteilt es an alle Spieler. Dies geschieht dreimal.
	 * Beim dritten mal wird aber diceRollFinal vom Model ausgel�st.
	 * 
	 * @param message
	 */
	public void diceRollFinal(String message) {
		this.model.diceRoll(message);
		this.model.deactivateGUI();
	}

	public static DiceBoard_Controller getController() {
		if (singleton == null) {
			singleton = new DiceBoard_Controller();
		}
		return singleton;
	}

	public void activateGUI() {
		this.model.activateGUI();
	}

	public JPanel gui() {
		return this.view;
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		// System.out.println("Run vorher: " + this.model.run);
		if (ae.getSource() == this.view.roll) {
			if (this.model.run < 2) {
				this.model.diceRoll();
				this.model.sendDices(Service.DiceRoll);
				this.model.updateDiceFaces();
				return;
			} else if (this.model.run >= 2) {
				this.model.diceRoll();
				this.model.sendDices(Service.DiceRollFinal);
				this.model.updateDiceFaces();
				for (JButton b : this.view.diceButtons) {
					b.setBorder(BorderFactory.createLineBorder(Color.BLACK));
				}
				this.model.deactivateGUI();
				return;
			}
		}
		if (this.model.run > 0 && this.model.run < 3) {
			diceButtons: for (int i = 0; i < noOfDices; i++) {
				if (ae.getSource() == this.view.diceButtons[i]) {
					this.model.dices[i].switchKeepState();
					if (this.model.dices[i].getKeepState()) {
						this.view.diceButtons[i].setBorder(BorderFactory.createLineBorder(Color.RED));
					} else {
						this.view.diceButtons[i].setBorder(BorderFactory.createLineBorder(Color.BLACK));
					}
					break diceButtons;
				}
			}
		}
	}
	public static void kill(){
		singleton = null;
	}
}// end DiceBoard_Controller