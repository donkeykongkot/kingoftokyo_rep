package ch.fhnw.itprojekt15.DonkeyKong.DiceBoard;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;

import ch.fhnw.itprojekt15.*;

/**
 * <b>DiceBoard_View</b> DiceBoard_View erbt von der Klasse JPanel. Hier wird
 * das in der Anforderungsspezifikation aufgezeigte Teil-GUI erstellt. Die Texte
 * der Elemente werden vom Interface bezogen. Dieses Panel wird dann in der
 * GameBoard (JFrame) angezeigt.
 * 
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:12:30
 */
public class DiceBoard_View extends JPanel implements Constants_Dices {

	protected JButton[] diceButtons = new JButton[noOfDices];
	protected JButton roll = new JButton(textButtonDiceRoll);

	public DiceBoard_View() {
		super();
		this.setBorder(BorderFactory.createTitledBorder(textGuiName));
		this.setLayout(new BoxLayout(this,BoxLayout.PAGE_AXIS));
		for (int i = 0; i < noOfDices; i++) {
			this.diceButtons[i] = new JButton();
			this.diceButtons[i].setBorder(BorderFactory.createLineBorder(Color.BLACK));
		}
		JPanel north = new JPanel(new FlowLayout());
		for (JButton b : this.diceButtons) {
			north.add(b);
		}
		this.add(north);
		
		JPanel south = new JPanel(new FlowLayout());
		south.add(this.roll);
		this.roll.setEnabled(false);
		this.add(south);
	}


}// end DiceBoard_View