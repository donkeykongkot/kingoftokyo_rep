package ch.fhnw.itprojekt15.DonkeyKong.DiceBoard;

import java.io.IOException;
import javax.swing.JButton;
import javax.swing.BorderFactory;
import ch.fhnw.itprojekt15.DonkeyKong.DiceBoard.Constants_Dices.GenericDiceValues;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Constants_Services;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Server;

//import ch.fhnw.itprojekt15.*;

/**
 * <b>DiceBoard_Model</b> <b>diceRollFinal</b> Meldung: DiceRollFinal|
 * Würfelergebnis getrennt durch "," <b>diceRoll</b> Hier wird gewürfelt und das
 * Ergebnis an den Server verschickt. Meldung: DiceRoll| Würfelergebnis getrennt
 * durch ","
 * 
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:12:30
 */
public class DiceBoard_Model implements Constants_Dices {

	protected int run = 0;
	private DiceBoard_View view;
	protected Dice[] dices = new Dice[noOfDices];

	public DiceBoard_Model(DiceBoard_View view) {
		this.view = view;
		for (int i = 0; i < noOfDices; i++) {
			this.dices[i] = new Dice();
			//this.dices[i].setDice(Constants_Dices.GenericDiceValues.Blank);
		}
		//this.updateDiceFaces();

	}

	private String getDiceString() {
		String message = new String();
		for (Dice d : this.dices) {
			message += String.valueOf(d.value) + ",";
		}
		return message.substring(0, message.length() - 1); // ohne letztes Komma
	}

	public void diceRoll() {
		/**
		 * Diese Methode würfelt alle Würfel, welche nicht behalten werden
		 * sollen.
		 */
		for (Dice d : this.dices) {
			if (!d.getKeepState()) {
				int random = (int) (Math.random() * (GenericDiceValues.values().length - 1)) + 1;
				// -1/+1, da "Blank" nicht als Würfelergebnis möglich ist.
				d.setDice(GenericDiceValues.values()[random]);
			}
		}
	}

	public void sendDices(Constants_Services.Service service) {
		String message = String.valueOf(service) + "|";
		message += getDiceString();
		System.out.println(message);
		Server.getServer().send(message);
		
	}

	public void setDice(Dice dice, GenericDiceValues value) {
		/**
		 * Diese universelle Methode setzt ein beliebiges Würfelbild, zum
		 * Beispiel jenes, welches im DiceRoll-String enthalten ist.
		 */
		dice.setDice(value);
	}

	protected void diceRoll(String message) {
		String[] dice = message.split("\\,");
		if (dice.length == Constants_Dices.noOfDices) {
			for (int i = 0; i < dice.length; i++) {
				if (!Constants_Dices.checkENUMCompatibility(Constants_Dices.GenericDiceValues.values(), dice[i])) {
					System.out.println("Dice " + dice[i] + " not found!");
					return;
				}
				this.dices[i].setDice(GenericDiceValues.valueOf(dice[i]));
			}
			this.updateDiceFaces();
			this.run++;
		} else {
			System.out.println("Wrong no of Dices received!");
		}
	}

	public void resetDices() {
		for (Dice d : this.dices) {
			d.setDice(GenericDiceValues.Blank);
		}
		this.updateDiceFaces();
		this.view.roll.setEnabled(true);
	}

	public boolean getKeepThisDice() {
		return false;
	}

	public String getPathPicture(Dice dice) {
		return dice.getPathPicture();
	}

	public void deactivateGUI() {
		this.view.roll.setEnabled(false);
		this.run = 99;
	}

	public void activateGUI() {
		this.run = 0;
		for (Dice d : this.dices) {
			d.reset();
		}
		this.updateDiceFaces();
	}

	public void switchKeepingState(Dice dice) {
		dice.switchKeepState();
	}

	public void updateDiceFaces() {
		for (int i = 0; i < noOfDices; i++) {
			this.view.diceButtons[i].setIcon(this.dices[i].getIcon());
		}
	}
}// end DiceBoard_Model