package ch.fhnw.itprojekt15.DonkeyKong.DiceBoard;

import javax.swing.ImageIcon;

/**
 * Konstanten-Klasse f�r die W�rfel. Ein ENUM-Generator bildet die generischen
 * W�rfelwerte mit all ihren Eigenschaften. (Zahl, Attacke, Heilen, Energie)
 * 
 * 
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:12:30
 */
public interface Constants_Dices {

	public static final int noOfDices = 6;

	public static final int gainedVictoryPointsOnConquerTokyo = 1;
	public static final int gainedVictoryPointsOnEveryTurn = 2;
	public static final int maxLifes = 10;
	public static final String textGuiName = "W�rfel";
	public static final String textButtonDiceRoll = "W�rfeln";

	public static final String iconPath = "src/ch/fhnw/itprojekt15/DonkeyKong/DiceBoard/";

	public enum GenericDiceValues {
		Blank("blank.png", false, 0, false, false, false),
		One("one.jpg", true, 1, false, false, false),
		Two("Two.jpg", true, 2, false, false, false),
		Three("three.jpg", true, 3, false, false, false),
		Energy("energy.jpg", false, 0, true, false, false),
		Life("lifes.jpg", false, 0, false, false, true),
		Attack("attack.jpg", false, 0, false, true, false);

		public final String pathPicture;
		public final ImageIcon icon;
		public final boolean isNumeric;
		public final int value;
		public final boolean energy;
		public final boolean attack;
		public final boolean lifes;

		private GenericDiceValues(String picture, boolean numeric, int value, boolean energy, boolean attack,
				boolean lifes) {
			this.pathPicture = iconPath + picture;
			this.isNumeric = numeric;
			this.value = value;
			this.energy = energy;
			this.attack = attack;
			this.lifes = lifes;
			this.icon = new ImageIcon(this.pathPicture);
		}
	}

	public static boolean checkENUMCompatibility(Object[] enumType, String value) {
		for (Object o : enumType) {
			if (o.toString().equals(value)) {
				return true;
			}
		}
		return false;
	}
}
