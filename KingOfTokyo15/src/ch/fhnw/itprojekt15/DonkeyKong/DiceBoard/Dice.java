package ch.fhnw.itprojekt15.DonkeyKong.DiceBoard;

import javax.swing.ImageIcon;

public class Dice implements Constants_Dices{
	
	private String pathPicture;
	private ImageIcon icon;
	private boolean keepThisDice = false;
	private boolean numeric;
	protected GenericDiceValues value;
	
	
	public Dice(){
		this.reset();
	}
	
	public void setDice(GenericDiceValues value){
		this.value = value;
		this.icon = value.icon;
		this.updateDiceValues();
	}
	
	public void reset(){
		this.value = GenericDiceValues.Blank;
		this.keepThisDice = false;
		this.updateDiceValues();
	}
	public void updateDiceValues(){
		this.pathPicture = this.value.pathPicture;
		this.numeric = this.value.isNumeric;
		this.icon = this.value.icon;
	}
	public String getPathPicture(){
		return this.pathPicture;
	}
	
	public ImageIcon getIcon(){
		return this.icon;
	}
	
	public boolean getKeepState(){
		return this.keepThisDice;
	}
	
	public boolean getIsNumeric(){
		return this.numeric;
	}

	public void switchKeepState(){
		if(this.keepThisDice){
			this.keepThisDice = false;
		}else{
			this.keepThisDice = true;
		}
	}
}
