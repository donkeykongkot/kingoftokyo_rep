package ch.fhnw.itprojekt15.DonkeyKong.TickerBoard;

import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Constants_Monster.Monster;

/**
 * Hier werden die Messages zusammengesetzt und bem TickerBoard_View als String
 * übergeben, der im TickerBoard-JPanel (siehe GameBoard-GUI) dann angezeigt werden.
 * 
 * @author Michael
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class TickerBoard_Model implements Constants_Tickerboad {

	// append fügt jedes Mal eine neue Meldung DARUNTER ein..Scrollbalken kam nicht mit, Text verschwand unten...
	// Die Methoden sind alle protected. Dies bedeutet, dass nur jeweils im gleichen Package auf sie zugegriffen werden kann.
	
	private TickerBoard_View view;
	
	/**
	 * Das Tickerboard_View wird dem TickerBoard_Model übergeben. Somit "kennt" nun das Model auch das View.
	 * 
	 * @param view
	 */
	public TickerBoard_Model(TickerBoard_View view) {
		this.view = view;
	}
	
	/**
	 * Das Monster, das an der Reihe ist, wird ausgegeben.
	 * 
	 * @param message
	 */
	protected void activePlayer(String message) {
		String user = message;
		this.view.textArea.insert(user + textActivePlayer + "\n", 0); 
	}
	
	/**
	 * Die gekaufte Karte wird allen angezeigt.
	 * 
	 * @param message
	 */	
	protected void cardBought(String message) {  // HEILUNG;Karte1,Karte2,neueKarte3
		String msg = message.replace(";", ","); // HEILUNG,Karte1,Karte2,neueKarte3
		String[] msg2 = msg.split("\\,"); // HEILUNG Karte1 Karte2 neueKarte3
		String karte1 = msg2[0]; // HEILUNG
		this.view.textArea.insert(Monster.getActiveMonster() + textCardBought + karte1 + "\n", 0);  
	}
	
	/**
	 * Falls das aktuelle Monster keine Karte kauft, wird eine entsprechende Meldung ausgegeben.
	 * Falls das aktuelle Monster eine Karte kauft, gibt es keine Ausgabe hier. Die Kartenauswahl wird freigeschaltet
	 * und nach dem Kauf via Methode cardBought() ausgegeben.
	 * 
	 * @param message
	 */
	protected void cardBuyAnswer(String message) {
		String msg = message;
		if (msg.equals(String.valueOf(false))) {
			this.view.textArea.insert(textCardBuyAnswer + "\n", 0);
		}
	}
	
	/**
	 * 3 neue Karten werden auf dem Gameboard-GUI angezeigt.
	 * 
	 * @param message
	 */	
	protected void cardNewCards(String message) {
		String[] msg = message.split("\\,");
		if (msg.length == 3) {
			String karte1 = msg[0];
			String karte2 = msg[1];
			String karte3 = msg[2];
			this.view.textArea.insert(textCardNewCards + karte1 + ",  " + karte2
					+ ",  " + karte3 + "\n", 0);
		}
	}
	
	/**
	 * Falls ein Spieler die Verbindung verliert, wird eine entsprechende Meldung ausgegeben.
	 * 
	 * @param message
	 */	
	protected void connectionLost(String message) {
		String user = message;
		this.view.textArea.insert(user + textConnectionLost + "\n", 0);
	}
	
	/**
	 * Würfelergebnis des 1. und 2. Durchgangs.
	 * 
	 * @param message
	 */	
	protected void diceRoll(String message) { 
		String[] msg = message.split("\\,"); 
		if (msg.length == 6) {
			String würfelergebnis1 = msg[0];
			String würfelergebnis2 = msg[1];
			String würfelergebnis3 = msg[2];
			String würfelergebnis4 = msg[3];
			String würfelergebnis5 = msg[4];
			String würfelergebnis6 = msg[5];
			this.view.textArea.insert(textDiceRoll + würfelergebnis1 + ",  "
					+ würfelergebnis2 + ",  " + würfelergebnis3 + ",  "
					+ würfelergebnis4 + ",  " + würfelergebnis5 + ",  "
					+ würfelergebnis6 + "\n", 0);
		}
	}
	
	/**
	 * Definitives Würfelergebnis (3. Mal)
	 * 
	 * @param message
	 */	
	protected void diceRollFinal(String message) { 
		String[] msg = message.split("\\,");
		if (msg.length == 6) {
			String würfelergebnis1 = msg[0];
			String würfelergebnis2 = msg[1];
			String würfelergebnis3 = msg[2];
			String würfelergebnis4 = msg[3];
			String würfelergebnis5 = msg[4];
			String würfelergebnis6 = msg[5];
			this.view.textArea.insert(textDiceRollFinal + würfelergebnis1
					+ ",  " + würfelergebnis2 + ",  " + würfelergebnis3 + ",  "
					+ würfelergebnis4 + ",  " + würfelergebnis5 + ",  "
					+ würfelergebnis6 + "\n", 0);
		}
	}
	
	/**
	 * Ein Spieler hat das Spiel verlassen.
	 * 
	 * @param message
	 */	
	protected void playerLeave(String message) {
		String user = message;
		this.view.textArea.insert(user + textPlayerLeave + "\n", 0);
	}
	
	/**
	 * Ein Spieler wird suspendiert. Dies kann passieren, wenn der Spieler innert 30 Sekunden keinen Spielzug unternimmt.
	 * 
	 * @param message
	 */	
	protected void playerSuspended(String message) {
		String user = message;
		this.view.textArea.insert(user + textPlayerSuspended + "\n", 0);
	}
	
	/**
	 * Ein Spieler hat das Spiel verloren.
	 * 
	 * @param message
	 */	
	protected void playerLost(String message) {
		String user = message;
		this.view.textArea.insert(user + textPlayerLost + "\n", 0);
	}
	
	/**
	 * Ein Spieler hat das Spiel gewonnen.
	 * 
	 * @param message
	 */	
	protected void playerWon(String message) {
		String user = message;
		this.view.textArea.insert(user + textPlayerWon + "\n", 0);
	}
	
	/**
	 * Falls das aktuelle Monster Tokyo nicht verlassen will, wird hier eine entsprechende Meldung ausgegeben.
	 * Falls das aktuelle Monster Tokyo verlässt, gibt es keine Ausgabe hier. Die Meldung erfolgt dann
	 * via Methode tokyoLeave().
	 * 
	 * @param message
	 */	
	protected void tokyoLeaveAnswer(String message) {
		String msg = message;
		if (msg.equals(String.valueOf(false))) {
			this.view.textArea.insert(textTokyoLeaveAnswer + "\n", 0);
		}
	}

	/**
	 * Das aktuelle Monster verlässt Tokyo.
	 * 
	 * @param message
	 */	
	protected void tokyoLeave(String message) {
		String user = message;
		this.view.textArea.insert(user + textTokyoLeave + "\n", 0);
	}
	
	/**
	 * Das aktuelle Monster geht nach Tokyo.
	 * 
	 * @param message
	 */	
	protected void tokyoGo(String message) {
		String user = message;
		this.view.textArea.insert(user + textTokyoGo + "\n", 0);
	}

	/**
	 * Hier erfolgt die Ausgabe des Chats. Alle Buchstaben werden in Grossbuchstaben im TickerBoard (siehe GameBoard-GUI) ausgegeben,
	 * damit man Chat-Messages von den anderen Messages besser unterscheiden kann.
	 * 
	 * @param message
	 */	
	protected void chat(String message) {
		
		String[] msg = message.split("\\,");
		if (msg.length > 1) {
			String user = msg[0];
			String chatmsg = message.substring(msg[0].length() + 1);
			this.view.textArea.insert(user + ": " + chatmsg.toUpperCase() + "\n", 0);
		}
	}
} // end TickerBoard_Model