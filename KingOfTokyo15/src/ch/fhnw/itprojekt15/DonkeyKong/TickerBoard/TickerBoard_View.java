package ch.fhnw.itprojekt15.DonkeyKong.TickerBoard;


import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

/**
 * Hier wird das TickerBoard_View erstellt, inkl. Scrollbalken.
 * 
 * @author Michael
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */

public class TickerBoard_View extends JPanel implements Constants_Tickerboad {
	
	private JScrollPane scrollPane;
	protected JTextArea textArea; // protected, damit das TickerBoard_Model darauf zugreifen kann.
	
	public TickerBoard_View(){
       
		this.setBorder(BorderFactory.createTitledBorder(textGuiName));
		
        // JTextArea wird erzeugt
        this.textArea = new JTextArea(26, 49); // Anzahl Zeilen und Spalten
        this.textArea.setEnabled(false); // Text nicht editierbar
        this.textArea.setLineWrap(true); // Textumbruch = true
        this.textArea.setDisabledTextColor(Color.black);
        
       
        // JScrollPane wird erzeugt; dabei wird �ber den Konstruktor direkt unser JTextArea hinzugef�gt
        this.scrollPane = new JScrollPane (this.textArea, 
          ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
          ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        
        // JScrollPane wird dem jPanel im GameBoard hinzugef�gt!
        this.add(scrollPane);
    }	
}//end TickerBoard_View