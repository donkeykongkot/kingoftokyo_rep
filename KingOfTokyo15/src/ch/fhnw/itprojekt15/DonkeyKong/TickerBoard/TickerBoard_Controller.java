package ch.fhnw.itprojekt15.DonkeyKong.TickerBoard;

/**
 * Je nach dem, welche Message vom MessagHandler-IN (Im Package "ServerConnection") �bergeben wurde, wird eine
 * entsprechende Meldung auf dem GUI angezeigt. Sie werden durch die Methoden im TickerBoard_Model zusammengesetzt.
 * 
 * @author Michael
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class TickerBoard_Controller implements Constants_Tickerboad {

	private TickerBoard_Model model;
	private TickerBoard_View view;
	private static TickerBoard_Controller singleton;
	
	/**
	 * Das TickerBoard_View und das TickerBoard_Model werden instanziert.
	 * 
	 */
	private TickerBoard_Controller(){
		this.view = new TickerBoard_View();
		this.model = new TickerBoard_Model(this.view); // TickerBoard_View wird dem TickerBoard_Model �bergeben.
	}
	
	/**
	 * Singelton: Variable wird definiert, welche es nur einmal gibt.
	 * Identisches Objekt bei Ausf�hrung.
	 */
	public static TickerBoard_Controller getController(){
		if(singleton == null){
			singleton = new TickerBoard_Controller(); // Der TickerBoard_Controller wird instanziert.
		}
		return singleton;
	}

	/**
	 * Im GameBoard wird das TickerBoard_View angezeigt.
	 * 
	 * @return TickerBoard_View
	 */
	public TickerBoard_View getGui(){
		return this.view;
	}
	
	/**
	 * Message vom MessageHandler_IN wird empfangen und ans Model �bergeben.
	 * 
	 * @param message
	 */
	public void activePlayer(String message){
		this.model.activePlayer(message);
	}

	/**
	 * Message vom MessageHandler_IN wird empfangen und ans Model �bergeben.
	 * 
	 * @param message
	 */
	public void cardBought(String message){ 
		this.model.cardBought(message);
	}

	/**
	 * Message vom MessageHandler_IN wird empfangen und ans Model �bergeben.
	 * 
	 * @param message
	 */
	public void cardBuyAnswer(String message){ 
		this.model.cardBuyAnswer(message);
	}

	/**
	 * Message vom MessageHandler_IN wird empfangen und ans Model �bergeben.
	 * 
	 * @param message
	 */
	public void cardNewCards(String message){ 
		this.model.cardNewCards(message);
	}

	/**
	 * Message vom MessageHandler_IN wird empfangen und ans Model �bergeben.
	 * 
	 * @param message
	 */
	public void connectionLost(String message){ 
		this.model.connectionLost(message);
	}

	/**
	 * Message vom MessageHandler_IN wird empfangen und ans Model �bergeben.
	 * 
	 * @param message
	 */
	public void diceRoll(String message){ 
		this.model.diceRoll(message);
	}

	/**
	 * Message vom MessageHandler_IN wird empfangen und ans Model �bergeben.
	 * 
	 * @param message
	 */
	public void diceRollFinal(String message){ 
		this.model.diceRollFinal(message);
	}

	/**
	 * Message vom MessageHandler_IN wird empfangen und ans Model �bergeben.
	 * 
	 * @param message
	 */
	public void playerLeave(String message){ 
		this.model.playerLeave(message);
	}

	/**
	 * Message vom MessageHandler_IN wird empfangen und ans Model �bergeben.
	 * 
	 * @param message
	 */
	public void playerSuspended(String message){ 
		this.model.playerSuspended(message);
	}
	
	/**
	 * Message vom MessageHandler_IN wird empfangen und ans Model �bergeben.
	 * 
	 * @param message
	 */
	public void playerLost(String message){ 
		this.model.playerLost(message);
	}
	
	/**
	 * Message vom MessageHandler_IN wird empfangen und ans Model �bergeben.
	 * 
	 * @param message
	 */
	public void playerWon(String message){ 
		this.model.playerWon(message);
	}

	/**
	 * Message vom MessageHandler_IN wird empfangen und ans Model �bergeben.
	 * 
	 * @param message
	 */
	public void tokyoLeaveAnswer(String message){
		this.model.tokyoLeaveAnswer(message);
	}
	
	/**
	 * Message vom MessageHandler_IN wird empfangen und ans Model �bergeben.
	 * 
	 * @param message
	 */
	public void tokyoGo(String message){ 
		this.model.tokyoGo(message);
	}
	
	/**
	 * Message vom MessageHandler_IN wird empfangen und ans Model �bergeben.
	 * 
	 * @param message
	 */
	public void tokyoLeave(String message){ 
		this.model.tokyoLeave(message);
	}
	
	/**
	 * Message vom MessageHandler_IN wird empfangen und ans Model �bergeben.
	 * 
	 * @param message
	 */
	public void chat(String message){ 
		this.model.chat(message);
	}
	
	/**
	 * Das GameBoard wird mit dieser Methode zur�ckgesetzt. Ansonsten w�rden die Messages vom Spiel vorhin noch angezeigt werden.
	 */
	public static void kill(){
		singleton = null;
	}
}//end TickerBoard_Controller