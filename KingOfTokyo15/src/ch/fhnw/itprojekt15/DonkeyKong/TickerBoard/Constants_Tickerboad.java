package ch.fhnw.itprojekt15.DonkeyKong.TickerBoard;

/**
 * In diesem Interface werden alle Strings festgehalten, die innerhalb des
 * Package TickerBoard auftreten k�nnen.
 * 
 * @author Michael
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public interface Constants_Tickerboad {

	// F�r TickerBoard_Model
	
	// Ausgaben betreffend Karten
	public static final String textCardBought = " kauft diese Karte: "; // Nur bei true
	public static final String textCardBuyAnswer = "Das aktuelle Monster kauft keine Karten!"; // Nur bei false
	public static final String textCardNewCards = "Das aktuelle Monster erh�lt 3 neue Karten zur Auswahl: ";
	
	// Ausgaben betreffend W�rfel
	public static final String textDiceRoll = "W�rfelergebnis: ";
	public static final String textDiceRollFinal = "Definitives W�rfelergebnis: ";
	
	// Ausgaben betreffend User
	public static final String textActivePlayer = " ist an der Reihe!";
	public static final String textPlayerLeave = " hat das Spiel verlassen!";
	public static final String textPlayerSuspended = " wurde aus dem Spiel suspendiert!";
	public static final String textPlayerLost = " hat das Spiel verloren!"; 
	public static final String textPlayerWon = " hat das Spiel gewonnen!";
	
	// Ausgaben betreffend Tokyo
	public static final String textTokyoGo = " geht nach Tokyo!";
	public static final String textTokyoLeave = " verl�sst Tokyo!"; // Nur bei true
	public static final String textTokyoLeaveAnswer = "Das aktuelle Monster bleibt in Tokyo"; // nur bei false
	
	// Ausgabe betreffend Vebindungsabbruch
	public static final String textConnectionLost = "' Verbindung ist abgebrochen!";
	
	
	// F�r TickerBoard_View
	
	public static final String textGuiName = "Spielverlauf";
	
}