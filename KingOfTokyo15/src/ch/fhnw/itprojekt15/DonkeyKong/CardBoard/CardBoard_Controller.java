package ch.fhnw.itprojekt15.DonkeyKong.CardBoard;

import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Constants_Monster.Monster;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * <b>CardBoard_Controller</b> <b>actionPerformed</b> F�r GUI-Interaktionen.
 * Sobald Button "3 neue Karten" geklickt wird, wird gepr�ft, ob der aktuelle
 * Spieler �berhaupt Karten kaufen darf, da er dies nur an bestimmten Stellen im
 * Spiel tun darf. Im Anschluss wird der Energie-Stand �berpr�ft und bei
 * Vorhandensein von gen�gend Energie entsprechend die drei neuen Karten
 * ausgel�st.
 * 
 * Sobald einer der drei Karten-Buttons geklickt werden, wird ebenfalls der
 * Kontostand gepr�ft und der Kauf entsprechend (nicht) ausgef�hrt.
 * 
 * Rest: "Weiterleitungen" ans Model.
 * 
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:12:02
 */
public class CardBoard_Controller implements Constants_Cards, ActionListener {

	private static CardBoard_Controller singleton = null;
	protected CardBoard_Model model;
	protected CardBoard_View view;
	private Thread msg = null;
	protected boolean kill = false;

	public CardBoard_Controller() {
		this.view = new CardBoard_View();
		this.model = new CardBoard_Model(this.view);

		for (JButton button : this.view.cardButtons) {
			button.addActionListener(this);
		}
		this.view.newCards.addActionListener(this);
	}
	public static CardBoard_Controller getController() {
		if (singleton == null) {
			singleton = new CardBoard_Controller();
		}else if(singleton.kill){
			System.out.println("KILLED");
			singleton = new CardBoard_Controller();
		}
		return singleton;
	}
	
	public void buyQuestion() {
		CardBoard_Controller.getController().model.buyQuestion();
	}

	public void buyAnswer(String message) {
		this.model.buyAnswer(message);
	}

	public void cardBought(String message) {
		this.model.cardBought(message);
	}

	public void receiveCards(String message) {
		this.model.receiveCards(message);
	}

	public void activateGUI() {
		this.view.activateGUI();
	}

	public void inactivateGUI() {
		this.view.inactivateGUI();
	}

	public JPanel gui() {
		return this.view;
	}

	public void initialCardSetup() {
		this.model.initialCardSetup();
		this.model.newCards();
		this.model.updateCardButtons();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (this.view.buyCardsAccepted) {
			if (arg0.getSource() == this.view.newCards) {
				// Alle Karten ersetzen

				if (Monster.getActiveMonster().getEnergy() < newCardsCosts) {
					this.msg = new Thread(new Runnable() {
						public void run() {
							JOptionPane.showMessageDialog(null, "Sie haben nicht gen�gend Energie f�r diese Karte!",
									"Kauf nicht m�glich!", JOptionPane.ERROR_MESSAGE);
						}
					});
					this.msg.start();
				} else {
					this.model.newCards();
					this.model.updateCardButtons();
					this.view.buyCardsAccepted = false;
					this.view.newCards.setEnabled(false);
				}
			}
			for (int i = 0; i < noOfCards; i++) {
				// Eine Karte wurde angeklickt.

				if (arg0.getSource() == this.view.cardButtons[i]) {
					if (Monster.getActiveMonster().getEnergy() < (Card.cards[i].getCost() * (-1))) {
						this.msg = new Thread(new Runnable() {
							public void run() {
								JOptionPane.showMessageDialog(null, "Sie haben nicht gen�gend Energie f�r diese Karte!",
										"Kauf nicht m�glich!", JOptionPane.ERROR_MESSAGE);
							}
						});
						this.msg.start();
					} else {
						this.model.buyCard(Card.cards[i], i);
						this.view.buyCardsAccepted = false;
					}
				}
			}
		}
	}

	public static void kill() {
		singleton = null;
		//this.kill = true;
	}

}// end CardBoard_Controller