package ch.fhnw.itprojekt15.DonkeyKong.CardBoard;

import javax.swing.ImageIcon;


/**
 * Klasse für Karten-Konstanten.
 * Ein ENUM-Generator bildet Karten-Objekte mit all ihren Eigenschaften.
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:12:02
 */
public interface Constants_Cards {

	public static final String textGuiName = "Karten";
	public static final String textButton3newCards = "3 neue Karten";
	public static final int newCardsCosts = 2;

	public static enum Card {
		Eckkneipe("Eckkneipe", "3_Eckkneipe.jpg", -3, +1, 0, 0, 0, 0, false, "Der aktive Spieler gewinnt 1 Ruhmpunkt."),
		Feuerstrahl("Feuerstrahl", "3_Feuerstrahl.jpg", -3, 0, 0, -2, 0, 0, false, "Jedes andere Monster erleidet 2 Schaden."),
		Heilung("Heilung", "3_Heilung.jpg", -3, 0, +2, 0, 0, 0, false, "Der aktive Spieler heilt 2 Schaden."),
		Nationalgarde("Nationalgarde", "3_Nationalgarde.jpg", -3, +2, -2, 0, 0, 0, false, "Der aktive Spieler gewinnt 2 Ruhmpunkte und erleidet 2 Schaden."),
		Bombardement("Bombardement", "4_Bombardement.jpg", -4, 0, -3, -3, 0, 0, false, "Jedes Monster erhält 3 Schaden."),
		Hochbahn("Hochbahn","4_Hochbahn.jpg", -4, +2, 0, 0, 0, 0, false,"Der aktive Spieler gewinnt 2 Ruhmpunkte."),
		Panzer("Panzer", "4_Panzer.jpg", -4, +4, -3, 0, 0, 0, false, "Der aktive Spieler gewinnt 4 Ruhmpunkte und erleidet 3 Schaden."),
		Düsenjäger("Düsenjäger", "5_Düsenjäger.jpg", -5, +5, -4, 0, 0, 0, false, "Der aktive Spieler gewinnt 5 Ruhmpunkte und erleidet 4 Schaden."),
		SprungAusGrosserHöhe("Sprung aus grosser Höhe","5_SprungAusGrosserHöhe.jpg", -5, +1, 0, 0, 0, 0, true, "Der aktive Spieler gewinnt 2 Ruhmpunkte und geht nach Tokyo."),
		Wohnblock("Wohnblock", "5_Wohnblock.jpg",-5, +3, 0, 0, 0, 0, false,"Der aktive Spieler gewinnt 3 Ruhmpunkte"),
		KernkraftWerk("Kernkraftwerk","6_Kernkraftwerk.jpg",-6, +2, +3, 0, 0, 0, false, "Der aktive Spieler erhält 3 Ruhmpunkte und heilt 3 Schaden."),
		Ölraffinerie("Ölraffinerie","6_Ölraffinerie.jpg",-6, +2, 0, -3, 0, 0, false, "Der aktive Spieler gewinnt 2 Ruhmpunkte und jedes andere Monster erhält 3 Schaden."),
		Wolkenkratzer("Wolkenkratzer","6_Wolkenkratzer.jpg",-6, +4, 0, 0, 0, 0, false, "Der aktive Spieler bekommt 4 Ruhmpunkte"),
		Evakuierungsbefehl1("Evakuierungsbefehl","7_Evakuierungsbefehl.jpg", -7, 0, 0, 0,-5, 0, false,"Jedes andere Monster verliert 5 Ruhmpunkte."),
		Evakuierungsbefehl2("Evakuierungsbefehl","7_Evakuierungsbefehl.jpg", -7, 0, 0, 0, -5, 0, false,"Jedes andere Monster verliert 5 Ruhmpunkte."),
		Impulsaufladung("Impulsaufladung","8_ImpulsAufladung.jpg",-8, 0, 0, 0, 0, +9, false, "Der aktive Spieler erhält 9 Energie.");

		public final String name;
		public final String pathPicture;
		public final int cost;
		public final int ownFame;
		public final int otherFame;
		public final int ownHealth;
		public final int otherHealth;
		public final int ownEnergy;
		public final boolean goToTokyo;
		public final ImageIcon icon;
		public final String tickerBoardMessage;
		public static final String iconPath = "src/ch/fhnw/itprojekt15/DonkeyKong/CardBoard/";

		private Card(String n, String imageUrl, int cost, int ownFame, int ownHealth, int otherHealth, int otherFame,
				int ownEnergy, boolean goToTokyo, String message) {
			// this.card = card;
			this.name = n;
			this.pathPicture = iconPath + imageUrl;
			this.cost = cost;
			this.ownFame = ownFame;
			this.otherFame = otherFame;
			this.ownHealth = ownHealth;
			this.otherHealth = otherHealth;
			this.ownEnergy = ownEnergy;
			this.goToTokyo = goToTokyo;
			this.icon = new ImageIcon(this.pathPicture);
			this.tickerBoardMessage = message;
		}

		public String getPathPicture() {
			return this.pathPicture;
		}

		public ImageIcon getIcon() {
			return this.icon;
		}

		public int getCost() {
			return this.cost;
		}

		public int getOwnFame() {
			return this.ownFame;
		}

		public int getOtherFame() {
			return this.otherFame;
		}

		public int getOwnHealth() {
			return this.ownHealth;
		}

		public int getOtherHealth() {
			return this.otherHealth;
		}

		public int getOwnEnergy() {
			return this.ownEnergy;
		}

		public boolean getGoToTokyo() {
			return this.goToTokyo;
		}

		public static Card oneNewCard() {
			Card card = Card.values()[(int) (Math.random() * Card.values().length)];
			while (hasDuplicate(card)) {
				card = Card.values()[(int) (Math.random() * Card.values().length)];
			}
			return card;
		}

		public static int minimalEnergy() {
			int min = Integer.MAX_VALUE;
			for (Card c : cards) {
				if ((c.cost * (-1)) < min) {
					min = (c.cost * (-1));
				}
			}
			return min;
		}

		public static void renewCards() {
			for(int i = 0; i < cards.length; i++){
				cards[i] = oneNewCard();
			}
		}

		public static final String energyIcon = Card.iconPath + "Energy_Small.png";
		public static final String fameIcon = Card.iconPath + "Fame_Small.png";
		public static final String lifeIcon = Card.iconPath + "Life_Small.png";

		protected static Card[] cards = new Card[noOfCards];

		public static boolean hasDuplicate(Card card) {
			if (cards.length > 0) {
				for (Card c : cards) {
					if (c == card) {
						return true;
					}
				}
			}
			return false;
		}

		public static void initialCardSetup() {
			for (int i = 0; i < noOfCards; i++) {
				cards[i] = oneNewCard();
			}
		}

		public static void setCard(int slot, Card card) {
			Card.cards[slot] = card;
		}
	}

	public static final int noOfCards = 3;

	public static boolean checkENUMCompatibility(Object[] enumType, String value) {
		for (Object o : enumType) {
			if (o.toString().equals(value)) {
				return true;
			}
		}
		return false;
	}
}
