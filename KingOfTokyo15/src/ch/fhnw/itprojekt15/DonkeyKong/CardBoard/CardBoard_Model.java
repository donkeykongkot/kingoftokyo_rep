package ch.fhnw.itprojekt15.DonkeyKong.CardBoard;

import java.io.IOException;
import javax.swing.JOptionPane;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Server;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Constants_Monster.Monster;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Constants_Services.Service;

/**
 * <b>CardBoard_Model</b>
 * 
 * <b>buyQuestion</b> Hier wird der User gefragt, ob er Karten kaufen m�chte. Es
 * wird die Frage nach dem Kauf einer Karte oder dem Auslegen von drei neuen
 * Karten gestellt. Dies jeweils in Abh�ngigkeit vom Energiestand und den Kosten
 * der aktuell aufliegenden Karten.
 * 
 * <b>buyAnswer</b> Hier wird dem Server mitgeteilt, ob der Spieler Karten
 * kaufen m�chte oder nicht. Meldung: CardBuyAnswer|true oder false
 * 
 * <b>cardBought</b> Hier wird dem Server mitgeteilt, welche Karte der Spieler
 * gekauft hat. Meldung: CardBought|Karte;alteKarte1,alteKarte2,neueKarte3
 * 
 * <b>newCards</b> Es m�ssen 3 neue Karten eruiert werden. Es wird eine Meldung
 * an den Server ausgel�st werden, welche drei Karte das sind. Meldung:
 * CardNewCards|Karten (getrennt durch Kommas)
 * 
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:12:02
 */
public class CardBoard_Model implements Constants_Cards {

	private CardBoard_View view;

	public CardBoard_Model(CardBoard_View view) {
		this.view = view;
	}

	protected void buyAnswer(String message) {
		String[] msg = message.split("\\|");
		if (msg.length > 1) {
			if (msg[1].equals(String.valueOf(true)) && Monster.getActiveMonster() == Monster.getMyMonster()) {
				this.view.activateGUI();
			} else {
				this.view.inactivateGUI();
			}
		}
	}

	protected void cardBought(String message) {
		String[] messages = message.split("\\;");
		Card cardBought;
		if (messages.length > 1) {
			if (Constants_Cards.checkENUMCompatibility(Card.values(), messages[0])) {
				cardBought = Card.valueOf(messages[0]);
			} else {
				System.out.println("Card " + messages[0] + " not found!");
				return;
			}

			String[] otherCards = messages[1].split("\\,");
			if (otherCards.length == noOfCards) {
				for (int i = 0; i < noOfCards; i++) {
					if (Constants_Cards.checkENUMCompatibility(Card.values(), otherCards[i])) {
						Card.cards[i] = Card.valueOf(otherCards[i]);
					} else {
						System.out.println("Card " + otherCards[i] + " not found!");
					}
				}
			} else {
				System.out.println("Wrong no of Other Cards!");
			}
		} else {
			System.out.println("Too less cards transmitted");
		}
		this.updateCardButtons();
	}

	protected void newCards() {
		Card.renewCards();
		Server.getServer().send(this.getAllCards(true));
		
	}

	protected String getAllCards(boolean prefix) {
		// Gibt alle Karten in einem String zur�ck.
		String message = new String();
		if (prefix) {
			message += String.valueOf(Service.CardNewCards) + "|";
		}
		for (Card c : Card.cards) {
			message += c + ",";
		}
		return message.substring(0, message.length() - 1); // ohne letztes
															// Komma!
	}

	protected void oneNewCard(int slot) {
		Card.setCard(slot, Card.oneNewCard());
	}

	protected void updateCardButtons() {
		for (int nr = 0; nr < noOfCards; nr++) {
			this.view.cardButtons[nr].setIcon(Card.cards[nr].getIcon());
		}
	}

	protected void buyQuestion() {
		Thread t = new Thread(new Runnable() {
			public void run() {
				try {
					if (Monster.getMyMonster().getEnergy() >= Card.minimalEnergy()) {
						if (JOptionPane.showConfirmDialog(null, "Wollen Sie eine Karte kaufen?", null,
								JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
							Server.getServer().send(Service.CardBuyAnswer + "|" + true);
							CardBoard_Controller.getController().view.buyCardsAccepted = true;
							CardBoard_Controller.getController().view.newCards.setEnabled(true);
						} else {
							Server.getServer().send(Service.CardBuyAnswer + "|" + false);
							CardBoard_Controller.getController().view.buyCardsAccepted = false;
							CardBoard_Controller.getController().view.newCards.setEnabled(false);
						}
					} else if (Monster.getMyMonster().getEnergy() >= newCardsCosts) {
						if (JOptionPane.showConfirmDialog(null, "Wollen Sie drei neue Karten aufdecken?", null,
								JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
							CardBoard_Controller.getController().model.newCards();
							CardBoard_Controller.getController().view.buyCardsAccepted = true;
							CardBoard_Controller.getController().view.newCards.setEnabled(false);
						} else {
							Server.getServer().send(Service.CardBuyAnswer + "|" + String.valueOf(false));
							CardBoard_Controller.getController().view.buyCardsAccepted = false;
							CardBoard_Controller.getController().view.newCards.setEnabled(false);
						}
					} else {
						Server.getServer().send(Service.CardBuyAnswer + "|" + String.valueOf(false));
						CardBoard_Controller.getController().view.buyCardsAccepted = false;
						CardBoard_Controller.getController().view.newCards.setEnabled(false);
					}
				} catch (Exception ex) {
					System.out.println("Fehler bei CardBuyQuestion.");
					ex.printStackTrace();
				}
			}
		});
		t.start();
	}

	protected void buyCard(Card card, int slot) {
		/**
		 * 1. Teil der Message: gekaufte Karte in der Message speichern +
		 * Trennzeichen Semikolon
		 * 
		 * 2. Teil: leer gewordenen Kartenslot f�llen
		 * 
		 * 3. Teil: Alle 3 Karten komma-getrennt in die Message integrieren, so
		 * dass schlussendlich alle Clients dieselben Karten haben.
		 */

		String message = String.valueOf(Service.CardBought) + "|";
		message += String.valueOf(card) + ";"; // gekaufte Karte
		this.oneNewCard(slot); // neue Karte vergeben
		message += getAllCards(false); // inkl. neuer Karte
		Server.getServer().send(message);
		this.view.buyCardsAccepted = false;
		
	}

	protected void receiveCards(String message) {

		String[] cards = message.split("\\,");
		for (String card : cards) {
			if (!Constants_Cards.checkENUMCompatibility(Card.values(), card)) {
				System.out.println("Card " + card + " not found!");
				return;
			}
		}
		if (Card.cards.length == cards.length) {
			for (int i = 0; i < noOfCards; i++) {
				Card.cards[i] = Card.valueOf(cards[i]);
			}
			this.updateCardButtons();
		} else {
			System.out.println("Unequal no of cards!");
		}
	}

	protected void initialCardSetup() {
		Card.initialCardSetup();
	}

}// end CardBoard_Model