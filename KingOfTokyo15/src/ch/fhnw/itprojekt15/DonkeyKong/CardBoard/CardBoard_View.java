package ch.fhnw.itprojekt15.DonkeyKong.CardBoard;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import javax.swing.*;

/**
 * <b>CardBoard_View</b> CardBoard_View erbt von der Klasse JPanel. Hier wird
 * das in der Anforderungsspezifikation aufgezeigte Teil-GUI erstellt. Dieses
 * Panel wird dann im GameBoard (JFrame) angezeigt.
 * 
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:12:02
 */
public class CardBoard_View extends JPanel implements Constants_Cards {

	protected JButton[] cardButtons = new JButton[noOfCards];
	protected JButton newCards = new JButton(textButton3newCards);
	protected boolean buyCardsAccepted = false;

	public CardBoard_View() {
		super();
		this.setBorder(BorderFactory.createTitledBorder(textGuiName));
		for (int i = 0; i < noOfCards; i++) {
			cardButtons[i] = new JButton();
			cardButtons[i].setBorder(BorderFactory.createLineBorder(Color.BLACK));
			cardButtons[i].addMouseListener(new MouseAdapter() {
				public void mouseEntered(MouseEvent evt) {
					JButton button = (JButton) evt.getSource();
					button.setBorder(BorderFactory.createLineBorder(Color.ORANGE));
				}

				public void mouseExited(MouseEvent evt) {
					JButton button = (JButton) evt.getSource();
					button.setBorder(BorderFactory.createLineBorder(Color.BLACK));
				}

				public void mousePressed(MouseEvent evt) {
					JButton button = (JButton) evt.getSource();
					button.setBorder(BorderFactory.createLineBorder(Color.RED));
				}

				public void mouseReleased(MouseEvent evt) {
					JButton button = (JButton) evt.getSource();
					button.setBorder(BorderFactory.createLineBorder(Color.GREEN));
				}
			});
		}
		this.inactivateGUI();
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		JPanel north = new JPanel(new FlowLayout());
		JPanel south = new JPanel(new FlowLayout());
		for (JButton button : this.cardButtons) {
			north.add(button);
		}
		this.add(north);
		south.add(this.newCards);
		this.add(south);
	}

	protected void activateGUI() {
		this.newCards.setEnabled(true);
		this.buyCardsAccepted = true;
	}

	protected void inactivateGUI() {
		this.newCards.setEnabled(false);
		this.buyCardsAccepted = false;
	}
}// end CardBoard_View