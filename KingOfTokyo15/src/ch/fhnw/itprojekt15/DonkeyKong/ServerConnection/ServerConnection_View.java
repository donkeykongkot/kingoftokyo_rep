package ch.fhnw.itprojekt15.DonkeyKong.ServerConnection;

import java.awt.GridLayout;

import javax.swing.*;


/**
 * <b>ServerConnection_View:</b> ServerConnection_View erbt von der Klasse
 * JFrame. Hier wird das in der Anforderungsspezifikation aufgezeigte GUI
 * erstellt. Die Texte der Elemente werden vom Interface bezogen.
 * 
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:16:30
 */
public class ServerConnection_View extends JFrame implements Constants_ServerConnection {
	private JLabel logo;
	private JLabel label;
	protected JLabel errorMessage;
	protected JTextField ip;
	protected JFrame frame;
	protected JButton next;
	protected JPanel jPNorth;
	protected JPanel jPSouth;
	protected ServerConnection_Model model;

	public ServerConnection_View(ServerConnection_Model model) {
		super(textGuiName);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		this.jPNorth = new JPanel(new GridLayout(0, 1));
		this.jPSouth = new JPanel(new GridLayout(0, 1));

		this.logo = new JLabel();
		this.logo.setIcon(new ImageIcon(pathPictureLogo));
		this.jPNorth.add(this.logo);

		errorMessage = new JLabel();
		next = new JButton(textButtonNext);
		ip = new JTextField();
		label = new JLabel("Server-IP:");
		jPSouth.add(label);
		jPSouth.add(ip);
		jPSouth.add(next);
		jPSouth.add(errorMessage);
		frame = this;
		this.add(jPNorth);
		jPNorth.add(jPSouth);
		this.pack();
		this.setVisible(true);
		this.model = model;
	}

}// end ServerConnection_View