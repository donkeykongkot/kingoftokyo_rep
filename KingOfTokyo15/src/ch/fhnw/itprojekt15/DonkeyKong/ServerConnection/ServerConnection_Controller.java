package ch.fhnw.itprojekt15.DonkeyKong.ServerConnection;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import ch.fhnw.itprojekt15.DonkeyKong.Login.Login_Controller;

/**
 * <b>ServerConnection_Controller:</b> Der Konstruktor instanziert das Model- und
 * View-Objekt und speichert die Referenzen in den entsprechenden Instanzvariablen.
 * 
 * <b>actionPerformed</b> Beim Bet�tigen des Buttons wird die Serververbindung
 * aufgebaut. Hierbei wird die eingegebene IP-Nr. als String �bergeben. Wird
 * hierbei eine IOException ausgel�st (Server ist somit nicht erreichbar), soll
 * die im Interface definierte Message angezeigt werden. Im Anschluss wird ein
 * Listener auf dem neuen Socket er�ffnet, der Login-Controller instanziert und
 * die ServerConnection_View danach disabled.
 * 
 * @author Andy
 * @version 2.0
 * @created 06-Nov-2015 12:16:30
 */
public class ServerConnection_Controller implements ActionListener, Constants_ServerConnection {

	private ServerConnection_Model model;
	private ServerConnection_View view;

	public ServerConnection_Controller() {
		this.model = new ServerConnection_Model();
		this.view = new ServerConnection_View(model);
		this.view.next.addActionListener(this);
	}

	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource() == this.view.next) {
			if (this.view.ip.getText().length() >= 7
					&& ServerConnection_Model.validateIPString(this.view.ip.getText())) {
				Server server;
				try {
					server = Server.getServer(port, this.view.ip.getText());
					if (server.socket.isConnected()) {
						System.out.println("Verbunden");
						this.view.setVisible(false);
						new MessageHandler_IN(server);
						Login_Controller.getController();
					} else {
						this.view.errorMessage.setText(textConnectionLost);
					}
				} catch (Exception ce) {
					this.view.errorMessage.setText(textNoConnectionPossible);
				}
			} else {
				this.view.errorMessage.setText(textNoValidIP);
			}
		}
	}
}// end ServerConnection_Controller