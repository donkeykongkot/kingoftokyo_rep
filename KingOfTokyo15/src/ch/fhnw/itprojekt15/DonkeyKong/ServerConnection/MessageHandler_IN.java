package ch.fhnw.itprojekt15.DonkeyKong.ServerConnection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

import javax.swing.JOptionPane;

import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Constants_Monster.Monster;

/**
 * <b>MessageHandler_IN</b> Die Klasse erbt von der Klasse Thread
 * 
 * <b>Konstruktor</b><b>:</b> Der Konstruktor instanziert einen BufferedReader
 * und weisst das Socket vom Server der Instanzvariablen socket zu.
 * Anschliessend wird Thread gestartet.
 * 
 * <b>run:</b> Ruft die eigene listen-Methode auf.
 * 
 * <b>listen:</b> in einer while-Schlaufe wartet der BufferdReader (welcher
 * wiederum ein InputStreamReader instanziert) auf Messages vom Server und
 * weisst diese der String-Variablen zu. Danach wird die Hilfsmethode
 * searchService aufgerufen.
 * 
 * <b>searchService:</b> Die �bergebene Message wird aufgesplittet. Im ersten
 * Teil der Messages ist jeweils der Service enthalten. Im zweiten Teil ist die
 * Message f�r den Service enthalten. Mit dem ersten Teil der Message wird der
 * Service aufgerufen, welche im Interface Constants_Services im <u>enum</u>
 * definiert sind. Mit der Methode valueOf (von der Klasse Enum) kann man den
 * richtigen Service vom interface holen. Danach wird mit der Methode
 * searchController des Service die auszuf�hrende Methode der Controller
 * gesucht. Hierbei m�ssen beide Teile der Message der Methode �bergeben werden.
 * 
 * Hinweis: die Controller m�ssen zwingend singleton sein.
 * 
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:16:30
 */
public class MessageHandler_IN extends Thread implements Constants_Services {

	private BufferedReader inputMessage;
	private String message;
	private Server server;
	private Socket socket;
	private boolean stopThread = false;

	public MessageHandler_IN(Server server) {
		this.server = server;
		this.socket = this.server.socket;
		try {
			this.inputMessage = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.listen();
	}

	private void listen() {
		this.start();
	}

	public void run() {
		try {
			while ((this.message = inputMessage.readLine()) != null && !this.stopThread) {
				System.out.println("Received: " + message); // DEBUG
				this.searchService(this.message);
			}
		} catch (IOException e) {
			System.out.println("ERROR: " + e.toString()); // DEBUG
			this.stopThread = true;
			JOptionPane.showMessageDialog(null, "Die Verbindung wurde unterbrochen. Bitte starten Sie das Spiel neu!",
					"..::: Verbindungsprobleme :::..", JOptionPane.WARNING_MESSAGE);
			System.exit(0);
		}
	}

	private void searchService(String message) {
		String element[] = message.split("\\|");
		if (element.length == 2) {
			Service.searchController(element[1], element[0]);
		} else if (element.length > 2) {
			Service.searchController(message.substring(element[0].length() + 1), element[0]);
		}
	}

}// end MessageHandler_IN