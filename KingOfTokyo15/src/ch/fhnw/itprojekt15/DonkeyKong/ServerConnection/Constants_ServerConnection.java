package ch.fhnw.itprojekt15.DonkeyKong.ServerConnection;
/**
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:16:29
 */
public interface Constants_ServerConnection {

	public static final String Msg_Error = "Keine Verbindung m�glich.";
	public static final String pathPictureLogo = "logo_small.png";
	public static final int port = 42935;
	public static final String textGuiName = "King of Tokyo - by Donkey Kong";
	public static final String textButtonNext = "Verbinden";
	public static final String textLabelError = "Keine Verbindung m�glich.";
	public static final String textTextFieldIP = "Bitte Server-IP eingeben: ";
	
	public static final String textConnectionLost = "Verbindung abgebrochen!";
	public static final String textNoConnectionPossible = "Keine Verbindung m�glich.";
	public static final String textNoValidIP = "Keine g�ltige IP Adresse!";

}