package ch.fhnw.itprojekt15.DonkeyKong.ServerConnection;

/**
 * <b>ServerConnection_Model: </b>
 * 
 * <b>validateIPString:</b> Diese Methode �berpr�ft, ob die eingegebene
 * Server-IP Adresse �berhaupt eine IP Adresse ist.
 * 
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:16:30
 */
public class ServerConnection_Model implements Constants_ServerConnection {

	protected ServerConnection_Model() {

	}

	protected static boolean validateIPString(String ip) {
		if (ip.equals(String.valueOf("localhost"))) {
			return true;
		}
		String[] elements = ip.split("\\.");
		if (elements.length != 4) { // 4 Ziffern-Bl�cke bei IPv4
			return false;
		}
		for (String fragment : elements) {
			try {
				int nr = Integer.parseInt(fragment);
				if (nr < 0 || nr > 255) {
					return false;
				}
			} catch (NumberFormatException nfe) {
				return false;
			}
		}
		return true;
	}

}// end ServerConnection_Model