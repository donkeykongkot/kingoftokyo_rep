package ch.fhnw.itprojekt15.DonkeyKong.ServerConnection;

import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import javax.swing.JOptionPane;
import ch.fhnw.itprojekt15.DonkeyKong.GameBoard.GameBoard;
import ch.fhnw.itprojekt15.DonkeyKong.Lobby.Lobby;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Constants_Monster.Monster;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Constants_Services.Service;

/**
 * <b>Server</b> Bei dieser Klasse handelt es sich um eine singleton-Klasse. Es
 * darf n�mlich nur ein Server-Objekt instanziert werden, da zum Server nicht
 * mehrere Verbindungen aufgebaut werden m�ssen. <b>Konstruktor (throws
 * IOException):</b> Der Konstruktor versucht ein Socket zu erhalten. Hierf�r
 * wird die IP und Port ben�tigt. Das Socket wird in der Variablen socket
 * abgespeichert. Das Server- Objekt wird der singleton-Variablen zugewiesen.
 * Ebenfalls soll ein PrintWriter- Objekt mit dem getOutputStream des Sockets
 * instanziert werden. <b>getServer(int, String) (throws IOException)</b> Ruft
 * den Konstruktor auf. Existiert bereits ein Server-Objekt, wird dieses
 * zur�ckgegeben (singleton). <b>getServer</b> Diese Methode gibt das einzige
 * Server-Objekt zur�ck (singleton). <b>send:</b> Mit dieser Methode kann man
 * Messages an den Server schicken. Befehl lautet also:
 * getServer().send(�blablabla�). Sobald eine IOException geworfen wird, m�ssen
 * entsprechende Aufr�umarbeiten stattfinden (z.B. Game schliessen) und dem
 * Spieler eine Hinweismeldung erscheinen (mit JOptionPane).
 * 
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:16:30
 */
public class Server {

	private JOptionPane connectionLost;
	private String ip;
	private String message;
	private PrintWriter outputMessage;
	private int port;
	protected static Server singleton;
	protected Socket socket;
	protected Thread heartbeat;
	// protected ServerConnection_Model model;

	/**
	 * 
	 * @param port
	 * @param ip
	 */
	private Server(int port, String ip) {
		this.port = port;
		this.ip = ip;
		// this.model = model;
		try {
			socket = new Socket(this.ip, this.port);
		} catch (UnknownHostException e) {
			System.out.println("Keine Verbindung zum Host m�glich");
		} catch (IOException e) {

		}

		this.heartbeat = new Thread(new Runnable() {
			public void run() {
				while (true) {
					Server.getServer().send(String.valueOf(Service.HeartbeatFromClient) + "|"
							+ String.valueOf(Service.HeartbeatFromClient));
					try {
						Thread.sleep(15000);
					} catch (Exception e) {
						System.out.println("Thread.sleep beim heartbeat ist fehlgeschlagen");
					}
				}
			}
		});
		this.heartbeat.start();

	}

	public static Server getServer(int port, String ip) {
		// INITIALES SETUP
		if (singleton == null) {
			singleton = new Server(port, ip);
		}
		return singleton;
	}

	public static Server getServer() {
		// GENERELLER AUFRUF
		if (singleton == null) {
			// NOTFALL KONSTRUKTOR, falls Singleton undefiniert
			singleton = new Server(Constants_ServerConnection.port, "localhost");
		}
		return singleton;
	}

	/**
	 * Sende-Methode: Schickt eine Message an den Server und �bernimmt bei
	 * abgebrochener Verbindung Client-seitig ein Exception Handling.
	 * 
	 * @param message
	 *            Zu sendende Nachricht an den Server
	 * @throws IOException
	 */
	public void send(String message) {
		if (this.socket != null) {
			try {
				OutputStream os = this.socket.getOutputStream();
				OutputStreamWriter osw = new OutputStreamWriter(os);
				BufferedWriter bw = new BufferedWriter(osw);
				bw.write(message + "\n"); // Zeilenabschluss
				bw.flush();
				System.out.println("Client > Server: " + message); //// DEBUG!!!
			} catch (IOException e) {
				System.out.println("Fehler beim Versenden der MSG: " + message);
				try {
					this.socket.close();
				} catch (IOException e1) {
				}
				JOptionPane.showMessageDialog(null,
						"Die Verbindung wurde unterbrochen. Bitte starten Sie das Spiel neu!",
						"..::: Verbindungsprobleme :::..", JOptionPane.ERROR_MESSAGE);

				System.exit(0);
			}
		} else {
			Server.killServer();
		}
	}

	public Socket getSocket() {
		return this.socket;
	}

	public static void killServer() {
		singleton = null;
	}

}// end Server