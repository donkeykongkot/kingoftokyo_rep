package ch.fhnw.itprojekt15.DonkeyKong.ServerConnection;

import ch.fhnw.itprojekt15.DonkeyKong.CardBoard.CardBoard_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.DiceBoard.DiceBoard_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.GameBoard.GameBoard;
import ch.fhnw.itprojekt15.DonkeyKong.GameRequest.GameRequest_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.Highscore.Highscore_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.Lobby.Lobby;
import ch.fhnw.itprojekt15.DonkeyKong.Login.Login_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.MonsterBoard_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.PlayerList.PlayerList_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.TickerBoard.TickerBoard_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.Registration.Registration_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.Tokyo.Tokyo_Controller;

/**
 * <b>Constants</b><b>_Services</b> Hier wird ein Enum mit den vorkommenden
 * Services deklariert und festgehalten, welche Message in welche
 * Programmkomponente verteilt wird.
 * 
 * 
 * <b>searchController:</b> Es handelt es sich hier um eine Methode, die
 * innerhalb des enums definiert wird! Im ersten String wird nun �ber ein
 * Switch-Case-Konstrukt die richtige Methode auf dem richtigen Controller
 * ausgef�hrt und hierbei der zweite Teil (String) der urspr�nglichen Message
 * �bergeben. Je nach Service k�nnen mehrere Controller bzw. Methoden der
 * Controller aufgerufen werden.
 * 
 * @author Andy
 * @version 3.0
 * @created 06-Nov-2015 12:16:30
 */
public interface Constants_Services {

	public enum Service {
		LoginValidation, LoginAlreadyLoggedIn, LoginUserNotFound, LoginPasswordWrong, LoginOK, RegistrationValidation, RegistrationUserAlreadyExists, RegistrationOK, HighscoreRequestData, HighscoreSendData, PlayerListRequestData, PlayerListSendData, GameRequest, GameRequestJustOnePlayer, CountdownGameRequest, GameStartGame, GameBoardPlayerLeave, GamePlayerLeave, CountdownGame, GamePlayerSuspended, GameActivePlayer, GameDiceTokyoBonus, DiceRoll, DiceRollFinal, GameTokyoLeaveQuestion, TokyoLeaveAnswer, TokyoGo, TokyoLeave, CardBuyQuestion, CardBuyAnswer, CardNewCards, CardOneNewCard, CardBought, GameProoverMonsterLoose, GameProoverMonsterWin, PlayerDataUpdate, Chat, Testing, StopCountdown, LeaveLobby, GameProoverNobodyWins, HeartbeatFromClient, HeartbeatFromServer;

		/**
		 * 
		 * @param message
		 * @param service
		 */
		public static void searchController(String message, String service) {
			if (message.length() > 0 && service.length() > 0) {
				if (checkENUMCompatibility(Service.values(), service)) {
					switch (Service.valueOf(service)) {

					case LoginUserNotFound:
						Login_Controller.getController().usernameNotExist(message);
						break;
					case LoginPasswordWrong:
						Login_Controller.getController().passwordWrong(message);
						break;
					case LoginOK:
						Login_Controller.getController().ok(message);
						break;
					case RegistrationValidation:
						break;
					case RegistrationUserAlreadyExists:
						Registration_Controller.getController().userAlreadyExists(message);
						break;
					case RegistrationOK:
						Registration_Controller.getController().ok(message);
						break;
					case HighscoreSendData:
						if (Highscore_Controller.getController() != null) {
							Highscore_Controller.getController().sendData(message);
						}
						break;
					case PlayerListSendData:
						if (PlayerList_Controller.getController() != null) {
							PlayerList_Controller.getController().sendData(message);
						}
						break;
					case GameRequestJustOnePlayer:
						GameRequest_Controller.getController().justOnePlayer(message);
						break;
					case CountdownGameRequest:
						GameRequest_Controller.getController().countdown(message);
						break;
					case GameStartGame:
						GameRequest_Controller.getController().startGame(message);
						break;
					case GamePlayerLeave:
						if (Lobby.getLobby().getGameActiv()) {
							MonsterBoard_Controller.getController().playerLeave(message);
							TickerBoard_Controller.getController().playerLeave(message);
						}
						break;
					case CountdownGame:
						if (Lobby.getLobby().getGameActiv()) {
							GameBoard.getGameBoard().countdown(message);
						}
						break;
					case GamePlayerSuspended:
						if (Lobby.getLobby().getGameActiv()) {
							TickerBoard_Controller.getController().playerSuspended(message);
							MonsterBoard_Controller.getController().playerSuspended(message);
							GameBoard.getGameBoard().playerSuspended(message);
						}
						break;

					case GameActivePlayer:
						if (Lobby.getLobby().getGameActiv()) {
							TickerBoard_Controller.getController().activePlayer(message);
							DiceBoard_Controller.getController().activePlayer(message);
							MonsterBoard_Controller.getController().activePlayer(message);
							GameBoard.getGameBoard().activePlayer(message);
						}
						break;
					case DiceRoll:
						if (Lobby.getLobby().getGameActiv()) {
							DiceBoard_Controller.getController().diceRoll(message);
							TickerBoard_Controller.getController().diceRoll(message);
						}
						break;
					case DiceRollFinal:
						if (Lobby.getLobby().getGameActiv()) {
							DiceBoard_Controller.getController().diceRollFinal(message);
							TickerBoard_Controller.getController().diceRollFinal(message);
						}
						break;
					case GameTokyoLeaveQuestion:
						if (Lobby.getLobby().getGameActiv()) {
							Tokyo_Controller.getController().tokyoLeaveQuestion(message);
						}
						break;
					case TokyoLeaveAnswer:
						if (Lobby.getLobby().getGameActiv()) {
							// Tokyo_Controller.getController().tokyoLeaveAnswer(message);
							// Irrelevant, da immer FALSE
							TickerBoard_Controller.getController().tokyoLeaveAnswer(message);
						}
						break;
					case TokyoGo:
						if (Lobby.getLobby().getGameActiv()) {
							Tokyo_Controller.getController().tokyoGo(message);
							TickerBoard_Controller.getController().tokyoGo(message);
						}
						break;
					case TokyoLeave:
						if (Lobby.getLobby().getGameActiv()) {
							Tokyo_Controller.getController().tokyoLeave(message);
							TickerBoard_Controller.getController().tokyoLeave(message);
						}
						break;
					case CardBuyQuestion:
						if (Lobby.getLobby().getGameActiv()) {
							CardBoard_Controller.getController().buyQuestion();
						}
						break;
					case CardBuyAnswer:
						if (Lobby.getLobby().getGameActiv()) {
							CardBoard_Controller.getController().buyAnswer(message);
							TickerBoard_Controller.getController().cardBuyAnswer(message);
						}
						break;
					case CardNewCards:
						if (Lobby.getLobby().getGameActiv()) {
							CardBoard_Controller.getController().receiveCards(message);
							TickerBoard_Controller.getController().cardNewCards(message);
						}
						break;
					case CardBought:
						if (Lobby.getLobby().getGameActiv()) {
							CardBoard_Controller.getController().cardBought(message);
							TickerBoard_Controller.getController().cardBought(message);
						}
						break;
					case GameProoverMonsterLoose:
						if (Lobby.getLobby().getGameActiv()) {
							TickerBoard_Controller.getController().playerLost(message);
							MonsterBoard_Controller.getController().playerLost(message);
							GameBoard.getGameBoard().playerLost(message);
						}
						break;
					case GameProoverMonsterWin:
						if (Lobby.getLobby().getGameActiv()) {
							TickerBoard_Controller.getController().playerWon(message);
							GameBoard.getGameBoard().playerWon(message);
						}
						break;
					case GameProoverNobodyWins:
						if (Lobby.getLobby().getGameActiv()) {
							GameBoard.getGameBoard().nobodyWins(message);
						}
						break;
					case PlayerDataUpdate:
						if (Lobby.getLobby().getGameActiv()) {
							MonsterBoard_Controller.getController().updatePlayerData(message);
						}
						break;
					case Chat:
						if (Lobby.getLobby().getGameActiv()) {
							TickerBoard_Controller.getController().chat(message);
						}
						break;
					case LoginAlreadyLoggedIn:
						Login_Controller.getController().alreadyLoggedIn(message);
						break;
					case HeartbeatFromServer:
						break;
					case Testing:
						String[] msg = message.split("\\|");
						if (msg.length == 2) {
							// wobei msg1 = Message, msg0 = Service.
							searchController(msg[1], msg[0]);
						} else {
							System.out.println("Fehler Testing: " + message);
						}
						break;
					default:
						System.out.println("NO ENUM FOUND FOR SERVICE " + service);
						break;
					}
				} else {
					System.out.println("No message or service defined!");
				}
			}
		};
	}

	public static boolean checkENUMCompatibility(Object[] enumType, String value) {
		for (Object o : enumType) {
			if (o.toString().equals(value)) {
				return true;
			}
		}
		return false;
	}
}
