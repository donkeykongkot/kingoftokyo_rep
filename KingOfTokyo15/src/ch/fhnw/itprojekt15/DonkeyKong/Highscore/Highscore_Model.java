package ch.fhnw.itprojekt15.DonkeyKong.Highscore;

import java.io.IOException;

import ch.fhnw.itprojekt15.*;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Server;

/**
 * Hier sollen die Highscore-Daten vom Server verlangt werden. Hierf�r kann die
 * Methode send von der Klasse Server zur Hilfe beigezogen werden.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:13:48
 */
public class Highscore_Model implements Constants_Highscore {

	public Highscore_Model(){
		
	}

	
	protected void requestData(){
		String message;
		message = serviceRequestData + delimiter1 + messageRequestData;
		Server.getServer().send(message);
	}
}//end Highscore_Model