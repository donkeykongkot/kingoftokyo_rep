package ch.fhnw.itprojekt15.DonkeyKong.Highscore;

import ch.fhnw.itprojekt15.*;

/**
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:13:48
 */
public interface Constants_Highscore {

	public static String delimiter1 = "|";
	public static String delimiter2 = ",";
	public static String delimiter3 = ";";
	public static final String textButtonRefresh = "Tabelle aktualisieren";
	public static String textGuiName = "Highscore";
	public static final String[] columnName = {"Rang","Spieler", "Siege"};
	public static final String serviceRequestData = "HighscoreRequestData";
	public static String messageRequestData = "HighscoreRequestData";

}