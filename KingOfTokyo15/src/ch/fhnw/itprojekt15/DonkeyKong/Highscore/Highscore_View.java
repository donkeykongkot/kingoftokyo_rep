package ch.fhnw.itprojekt15.DonkeyKong.Highscore;


import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import ch.fhnw.itprojekt15.*;

/**
 * Highscore_View erbt von der Klasse JPanel. Hier wird das in der
 * Anforderungsspezifikation aufgezeigte Teil-GUI erstellt. Die Texte der Elemente
 * werden vom Interface bezogen. Dieses Panel wird dann in der Lobby_View (JFrame)
 * angezeigt.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:13:48
 */
public class Highscore_View extends JPanel implements Constants_Highscore {

	protected String[] column;
	protected JButton refresh;
	protected String[][] row;
	protected JTable tablePane;
	protected DefaultTableModel table;
	protected JScrollPane scroll;
	
	public Highscore_View(){
		
		//Init JPanel
		this.setBorder(BorderFactory.createTitledBorder(textGuiName));
		this.setLayout(new BorderLayout());
					
		//Tabelle wird instanziiert. Zellen d�rfen nicht editierbar sein.
		this.tablePane = new JTable(){
			public boolean isCellEditable(int x, int y){
				return false;
			}
		};
								
		//Tabelle wird in ein ScrollPane integriert
		this.scroll = new JScrollPane(this.tablePane);
		this.scroll.getViewport().setBackground(Color.WHITE);
		this.add(scroll, BorderLayout.CENTER);
	
		//Aktualisieren-Button		
		refresh = new JButton(textButtonRefresh);
		this.add(refresh, BorderLayout.SOUTH);
		
	}
}//end Highscore_View