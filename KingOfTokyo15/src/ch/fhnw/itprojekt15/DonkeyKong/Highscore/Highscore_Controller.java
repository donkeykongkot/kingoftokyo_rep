package ch.fhnw.itprojekt15.DonkeyKong.Highscore;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.table.DefaultTableModel;

import ch.fhnw.itprojekt15.*;

/**
 * Beim Erstellen des Highscore Controllers wird �ber die Methode requestData()
 * des Models ein Request an den Server abgesetzt. Dieser liefert die Highscore-
 * Daten zur�ck. Um den Datenverkehr zu minimieren, werden diese also nur einmalig
 * geliefert. K�nnen aber mit einem Aktualisieren-Button interaktiv neu angefordert werden.
 * Ebenfalls werden auf dem WindowsListener der Lobby die Daten neu angefordert.
 * 
 * Sobald die Methode sendData() aufgerufen wird, m�ssen die Daten aus der �bergebenen
 * Message auf dem GUI in einer Tabelle angezeigt werden.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:13:48
 */
public class Highscore_Controller implements ActionListener, Constants_Highscore {

	private Highscore_Model model;
	private static Highscore_Controller singelton;
	private Highscore_View view;

	private Highscore_Controller(Highscore_Model model, Highscore_View view){ //EA
		this.model = model;
		this.view = view;
		
		this.view.refresh.addActionListener(this);
		this.model.requestData();
	}
	
	//Gibt das Highscore_Controller-Objekt zur�ck oder instanziiert sonst eines.
	public static Highscore_Controller getController(Highscore_Model model, Highscore_View view){
		if(singelton == null){
			singelton = new Highscore_Controller(model, view);
			return singelton;
		}
		
		return singelton;
	}
	
	//Gibt Highscore_Controller-Objekt zur�ck
	public static Highscore_Controller getController(){
		return singelton;
	}

	
	//Aktualisieren-Button
	public void actionPerformed(ActionEvent e){
		this.model.requestData();
	}

	//Methode wird vom WindowListener in der Lobby aufgerufen.
	public void requestData(){
		this.model.requestData();
	}
	

	//MessageIN: Die Daten vom Server werden in die Tabelle abgef�llt. Der Rang wird hier berechnet.
	//Beispiel-Message: HighscoreSendData|xxxx,xxxx;x,xxxxx;�..
	public void sendData(String message){
		this.view.tablePane.setModel(new DefaultTableModel(columnName,0));
		this.view.table = (DefaultTableModel) this.view.tablePane.getModel();
		
		Scanner rowScanner = new Scanner(message);
		rowScanner.useDelimiter(delimiter3);
		int rank = 0;
				
		while(rowScanner.hasNext()){
			rank++;
			Scanner valueScanner = new Scanner(rowScanner.next());
			valueScanner.useDelimiter(delimiter2);
			
			String[] rowValue = {rank + ".",valueScanner.next(),valueScanner.next()};
			
			this.view.table.addRow(rowValue);
		}
	}
		
}//end Highscore_Controller