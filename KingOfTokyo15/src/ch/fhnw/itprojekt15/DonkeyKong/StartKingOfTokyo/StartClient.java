package ch.fhnw.itprojekt15.DonkeyKong.StartKingOfTokyo;

import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.ServerConnection_Controller;

/**
 * <b>StartClient</b>
 * 
 * Die main-Methode l�dt jene Programmkomponente, die die Server-Verbindung
 * aufbaut.
 * 
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:16:30
 */
public class StartClient {

	/**
	 * START-KOMPONENTE DES CLIENTS
	 */
	public static void main(String[] args) {
		new ServerConnection_Controller();
	}
}