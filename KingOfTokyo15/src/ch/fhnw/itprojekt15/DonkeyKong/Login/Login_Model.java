package ch.fhnw.itprojekt15.DonkeyKong.Login;

import java.io.IOException;

import ch.fhnw.itprojekt15.DonkeyKong.Lobby.Lobby;
import ch.fhnw.itprojekt15.DonkeyKong.Registration.Registration_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.Registration.Registration_Model;
import ch.fhnw.itprojekt15.DonkeyKong.Registration.Registration_View;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Constants_Services;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Server;

/**
 * <b>validation</b> Hier sollen die Logindaten an den Server geschickt werden.
 * Hierf�r kann die Methode "send" von der Klasse Server zur Hilfe beigezogen
 * werden. Meldung: LoginValidation| Benutzername,Passwort
 * 
 * @author Michael
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class Login_Model implements Constants_Login {

	private Login_View view;
	
	/**
	 * Das Login_View wird dem Login_Model �bergeben. Somit "kennt" nun das Model auch das View.
	 * 
	 * @param view
	 */
	public Login_Model(Login_View view) {
		this.view = view;
	}
	
	/**
	 * Das Registration_View und das Registration_Model werden instanziert und dem Konstruktor Registration_Controller �bergeben.
	 * 
	 * @param view
	 * @param model
	 */
	protected void buildRegistrationMVC() {
		Registration_Model model = new Registration_Model();
		Registration_View view = new Registration_View();
		Registration_Controller.getController(view, model);
	}

	/**
	 * Das Lobby wird instanziert.
	 */
	protected void buildLobbyMVC() {
		Lobby.getLobby().setVisible(true);
		;
	}
	
	
	/**
	 * Hier wird das eingegebene Passwort und der eingegebene Username f�r den Server vorbereitet und ihm geschickt.
	 * 
	 * @param password
	 * @param username
	 */
	protected void validation(char[] password, String username) {
		String message;
		message = Constants_Services.Service.LoginValidation + delimiter1;
		message += username + delimiter2;
		for (char c : password) {
			message += c;
		}
		Server.getServer().send(message);
		
	}
	
	/**
	 * Mit dem gleichen Login-Username ist es nur m�glich, sich 1x einzuloggen. Ansonsten kommt eine entsprechende Meldung.
	 * 
	 * @param message
	 */	
	protected void alreadyLoggedIn(String message) {
		this.view.message.setText(textAlreadyLoggedIn);
	}
	
	/**
	 * Falls das Passwort falsch ist, kommt eine entsprechende Meldung.
	 * 
	 * @param message
	 */
	protected void passwordWrong(String message) {
		this.view.message.setText(textPasswordWrong);
	}
	
	/**
	 * Falls der Benutzername nicht existiert, kommt eine entsprechende Meldung.
	 * 
	 * @param message
	 */
	protected void usernameNotExist(String message) {
		this.view.message.setText(textUsernameNotExist);
	}
}// end Login_Model