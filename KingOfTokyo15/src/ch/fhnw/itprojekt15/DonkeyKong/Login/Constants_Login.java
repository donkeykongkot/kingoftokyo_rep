package ch.fhnw.itprojekt15.DonkeyKong.Login;

/**
 * In diesem Interface werden alle Strings festgehalten, die innerhalb des
 * Package Logins auftreten k�nnen.
 * 
 * @author Michael
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public interface Constants_Login {

	// F�r Login_Controller:

	public static final String textUsePassEmpty = "Keine Eingaben vorhanden!";
	public static final String textUsernameEmpty = "Benutzername fehlt!";
	public static final String textPasswordEmpty = "Passwort fehlt!";
	public static final String textPasswordWrong = "Falsches Passwort!"; 
	public static final String textUsernameNotExist = "Dieser Username existiert nicht!"; 
	public static final String textAlreadyLoggedIn = "User bereits eingeloggt!";

	// F�r Login_Model:

	public static final String delimiter1 = "|"; 
	public static final String delimiter2 = ","; 

	// F�r Login_View:

	public static final String textGuiName = "Login"; 
	public static final String pathPictureLogo = "logo_small.png"; 
	public static final String textTextFieldUsername = "Benutzername";
	public static final String textPasswordFieldPassword = "Passwort"; 
	public static final String textButtonLogin = "Login"; 
	public static final String textButtonRegistration = "Registrieren";
}