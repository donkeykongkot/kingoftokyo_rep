package ch.fhnw.itprojekt15.DonkeyKong.Login;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.*;

/**
 * <b>Login_View:</b> 
 * Login_View erbt von der Klasse JFrame. Hier wird das in
 * der Anforderungsspezifikation aufgezeigte GUI erstellt. Die Texte der
 * Elemente werden vom Interface bezogen.
 * 
 * @author Michael
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class Login_View extends JFrame implements Constants_Login {

	// Logo
	protected JLabel logo;
	protected ImageIcon logoSmall;
	
	// Benutzername
	protected JLabel usernameJL;
	protected JTextField username;
	
	// Passwort
	protected JLabel passwordText;
	protected JPasswordField password;
	
	// Login-Button
	protected JButton login;
	
	// Registration-Button
	protected JButton registration;
	
	// Message-Bereich
	protected JLabel message;
	protected JPanel[] areasJPSouth;
	
	// BorderLayout-Bereiche
	protected JPanel jPNorth;
	protected JPanel jPSouth;
	
	
	public Login_View() {
		
		// Erstellung des Layouts
		
		super(textGuiName);
		this.setLayout(new BorderLayout());
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		this.jPNorth = new JPanel(new FlowLayout());
		this.jPSouth = new JPanel(new GridLayout(0, 1));
		this.areasJPSouth = new JPanel[8];

		for (int i = 0; i < 8; i++) {
			this.areasJPSouth[i] = new JPanel(new FlowLayout());
			JPanel jp = this.areasJPSouth[i];
			jPSouth.add(this.areasJPSouth[i]);
		}
		
		// Logo und dessen Positionierung
		
		this.logoSmall = new ImageIcon(pathPictureLogo);
		this.logo = new JLabel(logoSmall);
		this.jPNorth.add(this.logo);
		Dimension size = this.jPNorth.getPreferredSize();
		size.width = 250;
		this.jPNorth.setPreferredSize(size);

		// Benutzername-Feld
		
		this.usernameJL = new JLabel(textTextFieldUsername);
		this.areasJPSouth[0].add(usernameJL);
		this.username = new JTextField();
		this.username.setColumns(12);
		this.areasJPSouth[1].add(username);

		// Passwort-Feld
		
		this.passwordText = new JLabel(textPasswordFieldPassword);
		this.areasJPSouth[2].add(passwordText);
		this.password = new JPasswordField();
		this.password.setColumns(12);
		this.areasJPSouth[3].add(password);

		// Login-Button
		
		this.login = new JButton(textButtonLogin);
		this.areasJPSouth[4].add(login);

		// Registration-Button
		
		this.registration = new JButton(textButtonRegistration);
		this.areasJPSouth[5].add(registration);

		// Message-Bereich
		
		this.message = new JLabel();
		this.areasJPSouth[6].add(message);

		// BorderLayout-Bereiche
		
		this.add(jPNorth, BorderLayout.NORTH);
		this.add(jPSouth, BorderLayout.SOUTH);

		this.pack();
		this.setVisible(true);
	}
}// end Login_View