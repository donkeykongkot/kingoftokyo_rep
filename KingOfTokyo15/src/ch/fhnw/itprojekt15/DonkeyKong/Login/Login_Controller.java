package ch.fhnw.itprojekt15.DonkeyKong.Login;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Michael
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class Login_Controller implements ActionListener, Constants_Login {

	private static Login_Controller singelton;
	private Login_Model model;
	private Login_View view;

	/**
	 * Das Login_View und das Login_Model werden instanziert und dem Login-Button und dem Registration-Button einen ActionListener hinzugef�gt.
	 * 
	 * @param view
	 * @param model
	 */
	private Login_Controller(){
		this.view = new Login_View();
		this.model = new Login_Model(this.view); // Login_View wird dem Login_Model �bergeben.
		this.view.login.addActionListener(this); // this = ActionListener ist im Login_Controller und h�rt auf den Login-Button
		this.view.registration.addActionListener(this); // this = ActionListener ist im Login_Controller und h�rt auf den Registration-Button
	}

	/**
	 * Singelton: Variable wird definiert, welche es nur einmal gibt.
	 * Identisches Objekt bei Ausf�hrung.
	 */
	public static Login_Controller getController(){
		if (singelton == null) {
			singelton = new Login_Controller(); // Der Login_Controller wird instanziert.
		}
		return singelton;
	}
	
	/**
	 * Entweder wird der Login-Button oder der Registration-Button angeklickt (ActionEvent).
	 * Wenn Pr�fung in Ordnung, kann die Methode im Login_Model "validation" ausgef�hrt werden.
	 * 
	 * Wird der Registration-Button angeklickt, wird die Login_Methode "buildRegistrationMVC"
	 * im Model ausgef�hrt.
	 * 
	 * @param e
	 */
	public void actionPerformed(ActionEvent e) {
		// Login-Button wird angeklickt
		if (e.getSource() == view.login) {
			String username = this.view.username.getText();
			char[] password = this.view.password.getPassword();
			
			if(username.equals("") && password.length == 0){
				this.view.message.setText(textUsePassEmpty);
			} else if (username.equals("")) {
				this.view.message.setText(textUsernameEmpty);
				this.view.pack(); // so gross das JFrame, wie n�tig
			} else if (password.length == 0) {
				this.view.message.setText(textPasswordEmpty);
				this.view.pack(); // so gross das JFrame, wie n�tig
			} else {
				this.model.validation(password, username);
				this.view.pack();
			}
		}
		// Registration-Button wird angeklickt
		if (e.getSource() == view.registration) {
			this.model.buildRegistrationMVC();
			view.setVisible(false);
		}
	}

	/**
	 * Falls die Login-Daten korrekt sind, wird das Lobby gestartet und die Login_View wird unsichtbar.
	 * Die Methode "buildLobbyMVC" im Login_Model wird ausgef�hrt.
	 * 
	 * @param message
	 */
	public void ok(String message) {
		this.model.buildLobbyMVC();
		this.view.setVisible(false);
	}
	
	/**
	 * Mit dem gleichen Login-Username ist es nur m�glich, sich 1x einzuloggen. 
	 * Ansonsten kommt eine entsprechende Meldung durch die Methode im Login_Model.
	 * 
	 * @param message
	 */
	public void alreadyLoggedIn(String message){
		this.model.alreadyLoggedIn(message);
	}
	
	/**
	 * Falls das Passwort falsch ist, kommt eine entsprechende Meldung durch die Methode im Login_Model.
	 * 
	 * @param message
	 */
	public void passwordWrong(String message) {
		this.model.passwordWrong(message);
	}

	/**
	 * Falls der Benutzername nicht existiert, kommt eine entsprechende Meldung durch die Methode im Login_Model.
	 * 
	 * @param message
	 */
	public void usernameNotExist(String message) {
		this.model.usernameNotExist(message);
	}
}// end Login_Controller
