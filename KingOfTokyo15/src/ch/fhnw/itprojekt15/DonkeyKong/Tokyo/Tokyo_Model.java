package ch.fhnw.itprojekt15.DonkeyKong.Tokyo;

import javax.swing.JOptionPane;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Constants_Monster.Monster;
import ch.fhnw.itprojekt15.DonkeyKong.Server.Constants_Services.Services;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Server;

/**
 * Aktualisiert den Tokyo Status auf den Monstern.
 * 
 * leavetokyoQuestion: Hier wird dem Spieler die Frage gestellt, ob er Tokyo
 * verlassen m�chte.
 * 
 * sendTokyoStatus: Hier wird dem Server eine Meldung geschickt, sollte sich in
 * Tokyo etwas tun.
 * 
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:17:00
 */
public class Tokyo_Model implements Constants_Tokyo {

	private Tokyo_View view;

	public Tokyo_Model() {

	}

	public Tokyo_Model(Tokyo_View view) {
		this.view = view;
		//this.tokyoGo("TheKing"); // TESTING!!!
	}

	protected void tokyoGo(String message) {
		// TokyoGo|CYBERBUNNY
		if (Monster.checkENUMCompatibility(Monster.values(), message)) {
			Monster monster = Monster.valueOf(message);
			this.view.monster = monster;
			this.view.monsterIcon.setIcon(monster.getImageIcon());
		} else {
			System.out.println("Service or Monster not found in ENUM: " + message);
		}
	}

	protected void tokyoLeave(String message) {
		// TokyoLeave|CYBERBUNNY
		if (Monster.checkENUMCompatibility(Monster.values(), message)) {
			Monster monster = Monster.valueOf(message);

			// Falls der Spieler Tokyo besetzt, wird Tokyo leer.
			// Pr�fung notwendig, da nicht garantiert ist, dass die Message
			// nur von tokyoLeaveQuestion her stammt!

			if (monster == this.view.monster) {
				this.view.monster = null;
				this.view.monsterIcon.setIcon(null);
			}
		}
	}

	protected void tokyoLeaveQuestion() {
		Thread t = new Thread ( new Runnable(){
			public void run(){
		try {
			if (JOptionPane.showConfirmDialog(null, "Wollen Sie Tokyo verlassen?", null,
				JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				Server.getServer().send(Services.TokyoLeaveAnswer + "|" + String.valueOf(true));
			} else {
				Server.getServer().send(Services.TokyoLeaveAnswer + "|" + String.valueOf(false));
			}
		} catch (Exception ex) {
			System.out.println("Fehler beim Senden der \"TokyoLeaveQuestion\".");
		}
			}});
		t.start();
	}
}// end Tokyo_Model