package ch.fhnw.itprojekt15.DonkeyKong.Tokyo;

import java.awt.FlowLayout;
import javax.swing.*;

import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Constants_Monster.Monster;

/**
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:17:00
 */
public class Tokyo_View extends JPanel implements Constants_Tokyo {

	private JLabel playboard = new JLabel(new ImageIcon(pathPictureTokyo));;
	protected JLabel monsterIcon;
	protected Monster monster;

	public Tokyo_View() {
		this.monsterIcon = new JLabel();
		this.setBorder(BorderFactory.createTitledBorder(textGuiName));
		FlowLayout fl = new FlowLayout();
		fl.setAlignment(FlowLayout.LEFT);
		this.playboard.setLayout(fl); // Auch JLabels k�nnen mit Layout versehen werden!
		this.playboard.add(monsterIcon);
		// Platziere das MonsterIcon auf die absolute Position
		// des roten Tokyo-Buttons
		this.monsterIcon.setBorder(BorderFactory.createEmptyBorder(198, 73, 0, 0));
		this.add(playboard); // Playboard-JLabel dem JPanel hinzuf�gen.
	}

	public JPanel gui() {
		return this;
	}

}// end Tokyo_View