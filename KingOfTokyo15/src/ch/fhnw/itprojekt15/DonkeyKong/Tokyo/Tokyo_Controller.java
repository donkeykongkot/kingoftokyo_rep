package ch.fhnw.itprojekt15.DonkeyKong.Tokyo;

import javax.swing.JPanel;


/**
 * in-Message: Diese Methode wird vom Service_Enum aufgerufen. Hier wird nun
 * entschieden wie weiter zu verfahren ist. Ebenfalls wird das GUI mit dem
 * korrekten Monster besetzt.
 * 
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:17:00
 */
public class Tokyo_Controller implements Constants_Tokyo {

	private Tokyo_Model model;
	private static Tokyo_Controller singleton;
	private Tokyo_View view;

	public Tokyo_Controller() {
		this.view = new Tokyo_View();
		this.model = new Tokyo_Model(view);
	}

	/**
	 * 
	 * @param message
	 */
	public void connectionLost(String message) {
		this.model.tokyoLeave(message);
	}

	public static Tokyo_Controller getController() {
		if (singleton == null) {
			singleton = new Tokyo_Controller();
		}
		return singleton;
	}

	/**
	 * 
	 * @param message
	 */
	public void playerLeave(String message) {
		this.model.tokyoLeave(message);
	}

	/**
	 * 
	 * @param message
	 */
	public void playerSuspended(String message) {
		this.model.tokyoLeave(message);
	}

	/**
	 * 
	 * @param message
	 */
	public void tokyoGo(String message) {
		this.model.tokyoGo(message);
	}

//	/**
//	 * 
//	 * @param message
//	 */
//	public void tokyoLeaveAnswer(String message) {
//		this.model.tokyoLeaveAnswer(message);
//	}

	/**
	 * 
	 * @param message
	 */
	public void tokyoLeaveQuestion(String message) {
		this.model.tokyoLeaveQuestion();
	}

	public void tokyoLeave(String message) {
		this.model.tokyoLeave(message);
	}
	
	public JPanel gui(){
		return this.view;
	}
	
	public static void kill(){
		singleton = null;
	}

}// end Tokyo_Controller