package ch.fhnw.itprojekt15.DonkeyKong.Tokyo;

/**
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:17:00
 */
public interface Constants_Tokyo {

	public static final String pathPictureTokyo = "src/ch/fhnw/itprojekt15/DonkeyKong/Tokyo/Board_Small_half.jpg";
	public static final String pathPictureMonsters = "src/ch/fhnw/itprojekt15/DonkeyKong/MonsterBoard/";
	public static final String textGuiName = "Tokyo";

}