package ch.fhnw.itprojekt15.DonkeyKong.Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import ch.fhnw.itprojekt15.DonkeyKong.Server.Constants_Server;

/**
 * DataBase_Access: Stellt die verbindung zur Datenbank her.
 *  erzeugt neue Datenbank
 * und Tabelle fals diese noch nicht vorhanden sind.
 * 
 * REGISTRIERUNG:
 * 
 * checkUsernameReg: Bevor der neue Spieler in die Datenbank gespeichert wird, pr�ft man ob der 
 * Benutzername noch zur verf�gung steht.
 * 
 * insertNewUser:Die Spieler (Benutzername und Passwort)werden in die Datenbank eingef�gt, die Anzahlsiege
 * werden auf 0 gesetzt. 
 * 
 * LOGIN:
 * checkUsername: Pr�ft ob der eingegebene Benutzername mit dem Username in der Datenbank �bereinstimmt,
 * wenn ja wird ein true zur�ckgesendet, ansonsten false.
 * 
 * checkPassword: Pr�ft ob das eingegebene Password mit dem Password des Benutzernamen �bereinstimmt,
 * welcher eingegeben wurde. True =korrekt, Flase = inkorrekt.
 * 
 * WINS:
 * updateWins: Aktualisiert die Anzahl Siege (wins) des Spielers welcher Gewonnen hat. Methode gibt
 * nicht zur�ck.
 * 
 * selectWins:  Die Methode ruft die Anzahlsiege aller registrierten Benutzer ab. 
 * Gibt eine Message zur�ck, wobei der Benurtzername und die Anzahlsiege mit dem Delimiter 2 getrennt
 * werden und der n�chste Datensatz mit dem Delimiter 3.
 * 
 * 
 * 
 * 
 *  * 
 * 
 * 
 * @author Roman
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class Database_Access implements Constants_Database, Constants_Server {

	private String driver;
	private String host;
	private String dbUser;
	private String dbPassword;
	private String url;
	public static Database_Access singleton;

	public Database_Access() {
		this.driver = Constants_Database.driver;
		this.host = Constants_Database.host;
		this.dbUser = Constants_Database.dbUser;
		this.dbPassword = Constants_Database.dbPassword;
		this.url = driver + host;
	}

	public static Database_Access getController() {
		if (singleton == null) {
			singleton = new Database_Access();
		}
		return singleton;
	}

	public void executeUpdate(String command) {
		executeUpdate(null, command);
	}

	public void executeUpdate(String database, String command) {
		if (database != null) {
			url += "/" + database;
		}
		try (Connection connection = DriverManager.getConnection(url, dbUser, dbPassword); // "jdbc:mysql://localhost:3306/mydatabase?"+"user=dbuser&password=mysecretpw");
				Statement s = connection.createStatement();) {

			s.executeUpdate(command);

		} catch (Exception ex) {
			System.out.println("Fehlermeldung: " + ex.toString());
		}
	}


	//REGISTRIERUNG
	
	public boolean checkUsernameReg(String username) {
		boolean result = false;

		try {
			Connection connection = DriverManager.getConnection(url, dbUser, dbPassword);
			Statement s = connection.createStatement();

			StringBuilder sb = new StringBuilder();
			sb.append("Select username from ");
			sb.append(database + "." + table + " where ");
			sb.append("username = '" + username + "'");
			System.out.println(sb);

			ResultSet rs = s.executeQuery(sb.toString());

			if (rs.next()) {
				result = true;
				System.out.println("Username Existiert bereits!");
			} else {
				result = false;
				System.out.println("Username ist frei!");
			}

			// Close
			rs.close();
			s.close();
			connection.close();

		} catch (Exception ex) {
			System.out.println("Fehlermeldung: " + ex.toString());
		}
		return result;

	}
	

	//REGISTRIERUNG
	
	public boolean insertNewUser(String username, String passwort, int wins) {
		boolean result = false;

		try {
			Connection connection = DriverManager.getConnection(url, dbUser, dbPassword);
			Statement s = connection.createStatement();

			StringBuilder sb = new StringBuilder();
			sb.append("Insert Into ");
			sb.append(database + "." + table + " ");
			sb.append("(username, password, wins) values ");
			sb.append("('" + username + "', '" + passwort + "', '" + wins + "')");
			System.out.println(sb);

			s.executeUpdate(sb.toString());
			result = true;
			System.out.println("User erfolgreich gespeichert!");

			// Close
			s.close();
			connection.close();

		} catch (Exception ex) {
			result = false;
			System.out.println("User wurde nicht in der Datenbank gespeichert!");
			System.out.println("Fehlermeldung: " + ex.toString());

		}
		return result;

	}

	
// LOGIN
	
	public boolean checkUsername(String username) {
		boolean result = false;

		try {
			Connection connection = DriverManager.getConnection(url, dbUser, dbPassword);
			Statement s = connection.createStatement();

			StringBuilder sb = new StringBuilder();
			sb.append("Select username from ");
			sb.append(database + "." + table + " where ");
			sb.append("username = '" + username + "'");
			System.out.println(sb);

			ResultSet rs = s.executeQuery(sb.toString());

			if (rs.next()) {
				result = true;
				System.out.println("Username stimmt");
			} else {
				result = false;
				System.out.println("Username ist falsch");
			}
			// Close
			rs.close();
			s.close();
			connection.close();

		} catch (Exception ex) {
			System.out.println("Fehlermeldung: " + ex.toString());
		}
		return result;

	}

// LOGIN
	
	public boolean checkPassword(String username, String password) {
		boolean result = false;

		try {
			Connection connection = DriverManager.getConnection(url, dbUser, dbPassword);
			Statement s = connection.createStatement();

			StringBuilder sb = new StringBuilder();
			sb.append("Select username, password from ");
			sb.append(database + "." + table + " where ");
			sb.append("username = '" + username + "' and ");
			sb.append("password = '" + password + "'");
			System.out.println(sb);

			ResultSet rs = s.executeQuery(sb.toString());
			
			if(rs.next()){
				String rsPassword = rs.getString("password");
				String rsUsername = rs.getString("username");
				System.out.println(rsPassword);
				System.out.println(rsUsername);
				
				
				if(password.equals(rsPassword)){
					System.out.println("Passwort stimmt �berein");
					result=true;
				}
				
			}else{
				System.out.println("Passwort stimmt nicht �berein");
			}

			// Close
			rs.close();
			s.close();
			connection.close();

		} catch (Exception ex) {
			System.out.println("Fehlermeldung: " + ex.toString());
		}

		return result;
	}

// WINS
	
// Einstellungen => SQL EDitor => Safeupdates => unbox
	public void updateWins(String username, int wins) {

		try {
			Connection connection = DriverManager.getConnection(url, dbUser, dbPassword);
			Statement s = connection.createStatement();
			StringBuilder sb = new StringBuilder();
			sb.append("Update ");
			sb.append(database + "." + table + " set ");
			sb.append("wins = wins + '" + wins + "'");
			sb.append(" Where username = '" + username + "'");
			System.out.println(sb);

			s.executeUpdate(sb.toString());

			System.out.println("Wins erfolgreich upgedatet");

			// CLOSE
			s.close();
			connection.close();

		} catch (Exception ex) {
			System.out.println("Fehlermeldung: " + ex.toString());
		}
	}
	
//WINS

	public String selectWins() {

		String message = "";

		try {
			Connection connection = DriverManager.getConnection(url, dbUser, dbPassword);
			Statement s = connection.createStatement();

			StringBuilder sb = new StringBuilder();
			sb.append("Select username, wins from ");
			sb.append(database + "." + table + " order by wins desc ");
			System.out.println(sb);

			ResultSet rs = s.executeQuery(sb.toString());
			while (rs.next()) {
				String user = rs.getString("username");
				int wins = rs.getInt("wins");
				message += user + delimiter2 + wins + delimiter3;
			}
			System.out.println(message);

			// Close
			rs.close();
			s.close();
			connection.close();

		} catch (Exception ex) {
			System.out.println("Fehlermeldung: " + ex.toString());

		}
		return message;
	}

	public void initDB() {
		//Datenbank erzeugen
		this.executeUpdate(Constants_Database.createDatabase());
		//Tabelle erzeugen
		this.executeUpdate(database, Constants_Database.createTablePlayers);

		//Datenbank l�schen
//		this.executeUpdate(Constants_Database.dropDatabase());
	
	}
	

}
