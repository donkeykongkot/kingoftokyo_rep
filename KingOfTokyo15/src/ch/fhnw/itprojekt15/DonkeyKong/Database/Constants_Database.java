package ch.fhnw.itprojekt15.DonkeyKong.Database;
/**
 * Constants_Database:
 * 
 * @author Roman
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public interface Constants_Database {
	
	public static final String driver = "jdbc:mysql://";
	public static final String host ="Localhost";
	public static final String database ="DonkeyKong";
	public static final String table = "Players";
	public static final String dbUser = "root";
	public static final String dbPassword = "root";
	
	public static final String createTablePlayers = "CREATE TABLE IF NOT EXISTS Players "
	+ "(ID_Player int NOT NULL AUTO_INCREMENT, "
	+ "Username varchar(40) NOT NULL, "
	+ "Password varchar(40) NOT NULL, PRIMARY KEY (ID_Player), "
	+ "Wins SMALLINT)";

	
	//Datenbank l�schen
	public static String dropDatabase(){
	return "DROP DATABASE IF EXISTS " + database;
	}
	
	// DB erstellen
	public static String createDatabase(){
	return "CREATE DATABASE IF NOT EXISTS " + database;
	}
	
	//Alle Daten der Datenbank abrufen
	public static String selectAll(){
	return "Select * from " + table;
	}
}
