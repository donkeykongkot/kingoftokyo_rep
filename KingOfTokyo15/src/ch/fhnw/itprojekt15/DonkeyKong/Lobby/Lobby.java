package ch.fhnw.itprojekt15.DonkeyKong.Lobby;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.util.ListIterator;

import javax.swing.*;

import ch.fhnw.itprojekt15.*;
import ch.fhnw.itprojekt15.DonkeyKong.GameRequest.GameRequest_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.GameRequest.GameRequest_Model;
import ch.fhnw.itprojekt15.DonkeyKong.GameRequest.GameRequest_View;
import ch.fhnw.itprojekt15.DonkeyKong.Highscore.Highscore_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.Highscore.Highscore_Model;
import ch.fhnw.itprojekt15.DonkeyKong.Highscore.Highscore_View;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Constants_Monster;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Constants_Monster.Monster;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Monsters;
import ch.fhnw.itprojekt15.DonkeyKong.PlayerList.PlayerList_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.PlayerList.PlayerList_Model;
import ch.fhnw.itprojekt15.DonkeyKong.PlayerList.PlayerList_View;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Server;

/**
 * In der Lobby werden die JPanels (Highscore, PlayerList und GamRequest) angezeigt.
 * 
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:14:03
 */
public class Lobby extends JFrame implements Constants_Lobby, Constants_Monster, WindowListener {

	private GameRequest_View gView; 
	private GameRequest_Model gModel; 
	private GameRequest_Controller gController; 
	private Highscore_View hView; 
	private Highscore_Model hModel; 
	private Highscore_Controller hController;
	private PlayerList_View pView; 
	private PlayerList_Model pModel; 
	private PlayerList_Controller pController; 
	private boolean gameActiv;
	private static Lobby singelton; 

	private Lobby() {
		//init JFrame		
		super(textGUIName);
		this.setVisible(true);
		this.setLayout(new BorderLayout());
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.addWindowListener(this);
		
		//PlayerList-Tabelle: Oben Links
		this.pView = new PlayerList_View();
		Dimension pSize = pView.getPreferredSize();
		pSize.width = 375;
		this.pView.setPreferredSize(pSize);
		this.pModel = new PlayerList_Model();
		this.pController = PlayerList_Controller.getController(pModel, pView);
		this.add(pView, BorderLayout.CENTER);

		//Highscore-Tabelle: Oben Rechts
		this.hView = new Highscore_View();
		Dimension hSize = hView.getPreferredSize();
		hSize.width = 375;
		this.hView.setPreferredSize(hSize);
		this.hModel = new Highscore_Model();
		this.hController = Highscore_Controller.getController(hModel, hView);
		this.add(hView, BorderLayout.EAST);

		//GameRequest-Bereich: Unten
		this.gView = new GameRequest_View();
		Dimension gSize = gView.getPreferredSize();
		gSize.width = 750;
		this.gView.setPreferredSize(gSize);
		this.gModel = new GameRequest_Model();
		this.gController = GameRequest_Controller.getController(gModel, gView);
		this.add(gView, BorderLayout.SOUTH);

		this.pack();

		this.setGameActiv(false);

	}

	//Gibt das Lobby-Objekt zur�ck oder instanziiert sonst eines.
	public static Lobby getLobby() {
		if (singelton == null) {
			singelton = new Lobby();
			return singelton;
		}
		return singelton;
	}

	//Schliesst Lobby
	public void disableLobby() {
		this.setVisible(false);
	}

	//Information �ber Gamestatus
	public boolean getGameActiv() {
		return this.gameActiv;
	}

	//Setzen des Game-Status
	public void setGameActiv(boolean gameActiv) {
		this.gameActiv = gameActiv;
	}

	
	public void windowActivated(WindowEvent arg0) {}

	//Sobald Lobby geschlossen wird, bedeutet dies, dass der Spieler beim Server abgemeldet werden muss.
	public void windowClosed(WindowEvent arg0) {
		String message = textLeaveMessage + Constants_Lobby.delimiter1 + textLeaveMessage;
		Server.getServer().send(message);

		System.out.println("Message wurde noch an Server geschickt: " + message);
		System.exit(0);
	}

	//Frage an User, ob er KingOfTokyo wirklich schliessen m�chte
	public void windowClosing(WindowEvent arg0) {
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		int leaveAnswer = JOptionPane.showConfirmDialog(this, textLeaveGui, "Beenden?", JOptionPane.YES_NO_OPTION);

		if (leaveAnswer == 0) {

		} else {
			this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		}
	}

	public void windowDeactivated(WindowEvent arg0) {}
	public void windowDeiconified(WindowEvent arg0) {}
	public void windowIconified(WindowEvent arg0) {}
	public void windowOpened(WindowEvent arg0) {}
	
}// end Lobby