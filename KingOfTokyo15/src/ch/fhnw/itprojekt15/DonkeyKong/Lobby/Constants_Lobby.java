package ch.fhnw.itprojekt15.DonkeyKong.Lobby;

public interface Constants_Lobby { 
	public final String textGUIName = "Lobby - King of Tokyo";	
	public final String textLeaveGui = "Wollen Sie sich wirklich abmelden?";
	public final String textLeaveMessage = "LeaveLobby";
	public final String delimiter1 = "|";
}
