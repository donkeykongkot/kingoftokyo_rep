package ch.fhnw.itprojekt15.DonkeyKong.GameBoard;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import ch.fhnw.itprojekt15.DonkeyKong.CardBoard.CardBoard_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.CardBoard.Constants_Cards;
import ch.fhnw.itprojekt15.DonkeyKong.Chat.Chat_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.DiceBoard.DiceBoard_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.Lobby.Lobby;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.MonsterBoard_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Monsters;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Constants_Monster.Monster;
import ch.fhnw.itprojekt15.DonkeyKong.PlayerList.PlayerList_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.PlayerList.PlayerList_Model;
import ch.fhnw.itprojekt15.DonkeyKong.PlayerList.PlayerList_View;
import ch.fhnw.itprojekt15.DonkeyKong.Server.Constants_Server;
import ch.fhnw.itprojekt15.DonkeyKong.Server.Constants_Services.Services;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Server;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.ServerConnection_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Constants_Services.Service;
import ch.fhnw.itprojekt15.DonkeyKong.TickerBoard.TickerBoard_Controller;
import ch.fhnw.itprojekt15.DonkeyKong.TickerBoard.TickerBoard_Model;
import ch.fhnw.itprojekt15.DonkeyKong.TickerBoard.TickerBoard_View;
import ch.fhnw.itprojekt15.DonkeyKong.Tokyo.Tokyo_Controller;

/**
 * <b>GameBoard</b> <b>Konstruktor:</b> Der Konstruktor kann nur durch die
 * getGameBoard-Methode aufgerufen werden. Der Konstruktor erstellt die
 * einzelnen MVCs CardBoard, DiceBoard, MonsterBoard, Toyko, TickerBoard. Die
 * f�nf Views dieser MVC werden als JPanel-Objekte zur�ckgegeben und auf dem
 * GameBoard (ist ein JFrame) angezeigt. Siehe hierf�r die entsprechenden
 * GUI-Anforderungen in der Anforderungsspezifikation. <b>closeGame: </b> Diese
 * Methode soll immer aufgerufen werden, wenn der Spieler das aktuelle Spiel
 * verl�sst (egal auf welche Art). Diese Methode dient dazu, hier allf�llige
 * Aufr�um-Arbeiten implementieren zu k�nnen. <b>countdown</b> Vom Server wird
 * jede Sekunde eine Meldung geschickt. In dieser Meldung ist die f�r den
 * aktuellen Spielzug verbleibende Zeit enthalten (Sekunden). Die Sekunde muss
 * jeweils auf dem GameBoard angezeigt werden. Meldung: CountdownGame|Sekunden
 * <b>playerSuspended</b> Wenn das betroffene Monster vom Server suspendiert
 * wird, m�ssen mit der close- Methode die Aufr�um-Arbeiten vollzogen und das
 * Game-GUI disabled werden. Welches Monster dem Spieler geh�rt kann man anhand
 * des Objektes Monsters eruiert werden. Meldung: GamePlayerSuspended|Monster
 * <b>playerLeave</b> Sobald der Spieler das GameBoard-GUI schliesst - also das
 * Spiel von sich aus verl�sst - muss zuerst noch diese Meldung abgesetzt
 * werden. Meldung: GameBoardPlayerLeave|GameBoardPlayerLeave
 * 
 * @author Roman, Mattias, Andy
 * @version 1.0
 * @created 06-Nov-2015 12:13:13
 */
public class GameBoard extends JFrame implements Constants_Server, Constants_GameBoard, WindowListener {

	//Die einzelnen MVC's
	private JPanel cardBoard;
	private JPanel diceBoard;
	private JPanel gameCountdown;
	private JPanel monsterBoard;
	private JPanel tickerBoard;
	private JPanel tokyo;
	private JPanel chat;
	public JLabel countdown;
	
	//Anordung auf GameBoard
	private JPanel north;
	private JPanel south;
	private JPanel northEast;
	private JPanel topNorthEast;	
	private static GameBoard singelton;
	
	//MenuBar
	private JMenuBar menuBar;
	private JMenu menu;
	private JMenuItem rules;
	
	//Hilfsvariable
	private int round = 0;
	
	private GameBoard() {
		//init GUI
		super(textGuiName);
		this.addWindowListener(this);
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
		//Containers f�r Anordnung auf GameBoard
		this.north = new JPanel(new FlowLayout());
		this.south = new JPanel(new FlowLayout());
		this.northEast = new JPanel();
		this.northEast.setLayout(new BoxLayout(this.northEast, BoxLayout.Y_AXIS));
		this.northEast.setPreferredSize(new Dimension(570, 535));
		this.topNorthEast = new JPanel();
		this.topNorthEast.setLayout(new BoxLayout(this.topNorthEast, BoxLayout.LINE_AXIS));

		//Tokyo (JPanle): Oben Links
		this.tokyo = Tokyo_Controller.getController().gui();
		this.tokyo.setPreferredSize(new Dimension(820, 535));
		this.north.add(tokyo);

		//GameCountdown (JLabel): Oben Mitte
		this.gameCountdown = new JPanel(new FlowLayout());
		this.gameCountdown.setBorder(BorderFactory.createTitledBorder(Constants_GameBoard.textGuiCountodwn));
		this.countdown = new JLabel(" ");
		this.countdown.setFont(new Font(null, Font.BOLD, 80));
		this.gameCountdown.add(this.countdown);
		this.gameCountdown.setPreferredSize(new Dimension(140, 140));
		this.topNorthEast.add(this.gameCountdown);

		//DiceBoard (JPanel): Oben Rechts
		this.diceBoard = DiceBoard_Controller.getController().gui();
		this.diceBoard.setPreferredSize(new Dimension(430, 140));
		this.topNorthEast.add(diceBoard);

		//Anordnung
		this.northEast.add(topNorthEast);
		
		//CardBoad (JPanel): Mitte Rechts
		this.cardBoard = CardBoard_Controller.getController().gui();
		this.cardBoard.setPreferredSize(new Dimension(0, 315));
		this.northEast.add(this.cardBoard);

		//Chat (JPanel): Mitte Rechts
		this.chat = Chat_Controller.getController().gui();
		this.chat.setPreferredSize(new Dimension(0, 80));
		this.northEast.add(this.chat);
		
		//Anordnung
		this.north.add(this.northEast);
		
		//MonsterBoard (JPanel): Unten Links
		this.monsterBoard = MonsterBoard_Controller.getController().getGUI();
		this.monsterBoard.setPreferredSize(new Dimension(820, 470));
		this.south.add(monsterBoard);
		
		//TickerBoard (JPanel): Unten Rechts
		this.tickerBoard = TickerBoard_Controller.getController().getGui();
		this.tickerBoard.setPreferredSize(new Dimension(570, 470));
		this.south.add(this.tickerBoard);

		//Anordnung
		this.add(north, BorderLayout.CENTER);
		this.add(south, BorderLayout.SOUTH);

		this.pack();
		this.setVisible(true);
		
		//Menu: Spielregeln
		this.menuBar = new JMenuBar();
		setJMenuBar(this.menuBar);

		this.menu = new JMenu("Hilfe");
		this.menuBar.add(this.menu);

		this.rules = new JMenuItem("Spielregeln");
		this.rules.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				File file = new File("src\\King of Tokyo Regeln Deutsch.pdf");
				Desktop desktop = Desktop.getDesktop();
				if (e.getSource() == rules) {

					try {
						desktop.open(file);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		menu.add(rules);
		
		this.setVisible(true);
	}

	
	//Instanzierungsmethode: Gibt bestehendes GameBoard-Objekt zur�ck, ansonsten wird ein Neues erstellt
	public static GameBoard getGameBoard() {
		if (singelton == null) {
			singelton = new GameBoard();
			return singelton;
		}
		return singelton;
	}
	
	
	//Hilfsmethode: Schliesst bestehendes GameBoard-Objekt
	public static void kill() {
		singelton = null;
	}
	
	
	//Hilfsmethode: Aufr�um-Arbeiten beim schliessen eines Games
	public void closeGame() {
		// Setze GameBoard komplett zur�ck f�r allf�lligen sp�teren Aufruf.
		this.setVisible(false);
		DiceBoard_Controller.kill();
		CardBoard_Controller.kill();
		MonsterBoard_Controller.kill();
		Tokyo_Controller.kill();
		TickerBoard_Controller.kill();
		GameBoard.kill();

		// Monster-Array wird gel�scht
		Monsters.kill();

		// Monsters werden auf default gesetzt
		for (Monster monster : Monster.values()) {
			monster.setTokyoOccupied(false);
			monster.setVictoryPoints(0);
			monster.setEnergy(0);
			monster.setLife(10);
			monster.setActivePlayer(false);
			monster.setBelongsToThisPlayer(false);
			monster.setUsername("");
		}
		
		//Lobby wird ge�ffnet
		if (!Server.getServer().getSocket().isClosed()) {
			Lobby.getLobby().setVisible(true);
		}else{
			System.exit(0);
		}
	}

	
	//MessageIN: Zeige Countdown vom Server an
	//Beispiel-Message: CountdownGame|25,CyberBunny --> 25,CyberBunny
	public void countdown(String message) {
		Scanner scanner = new Scanner(message);
		scanner.useDelimiter(delimiter2);

		String countdownText = scanner.next();
		String monster = scanner.next();

		if (Monster.valueOf(monster).getBelongsToThisPlayer()) {
			this.countdown.setForeground(Color.RED);
		} else {
			this.countdown.setForeground(Color.GRAY);
		}

		this.countdown.setText(countdownText);
		this.pack();
	}

	
	//MessageIN: Game wird geschlossen, insofern beim aktuellen Spieler Countdown abgelaufen ist	
	//Beispiel-Message: GamePlayerSuspended|CyberBunny --> CyberBunny
	public void playerSuspended(String message) {
		if (Monster.valueOf(message).getBelongsToThisPlayer()) {
			Lobby.getLobby().setGameActiv(false);
			this.setVisible(false);
			Lobby.getLobby().setVisible(true);
			this.closeGame();
			JOptionPane.showMessageDialog(null,
					"Der Countdown ist abgelaufen. Sie wurden deshalb aus dem Spiel verbannt!");
		}
	}

	
	//MessageIN: Irgendein Spieler hat gewonnen. Meldung wird gezeigt und Game geschlossen.
	//Beispiel-Message: MonsterWin|CyberBunny --> CyberBunny
	public void playerWon(String message) {
		Lobby.getLobby().setGameActiv(false);
		JOptionPane.showMessageDialog(null, "Das Monster " + message + " hat das Spiel gewonnen!",
				"..::: Spiel beendet :::..", JOptionPane.WARNING_MESSAGE);
		this.closeGame();
	}

	//MessageIN: Monster des Spielers verliert, dann wird Game geschlossen
	//Beispiel-Message: MonsterLoose|CyberBunny --> CyberBunny 
	public void playerLost(String message) {
		if (Monster.valueOf(message).getBelongsToThisPlayer()) {
			Lobby.getLobby().setGameActiv(false);
			JOptionPane.showMessageDialog(null, "Ihr Monster hat verloren!", "..::: Spiel beendet :::..",
					JOptionPane.ERROR_MESSAGE);
			this.closeGame();
		}
		
		//Ist nur noch ein Spieler im Monsterarry, wird Methode playerLost() aufgerufen.
		if (Monsters.getMonsters().size() == 1) {
			Lobby.getLobby().setGameActiv(false);
			this.playerWon("" + Monsters.getMonsters().get(0));
			this.closeGame();
		}
	}

	
	//MessageIN: Alle Monster verlieren und Game wird geschlossen
	//Beispiel-Message: GameProoverNobodyWins|GameProoverNobodyWins  --> GameProoverNobodyWins
	public void nobodyWins(String message) {
		Lobby.getLobby().setGameActiv(false);
		JOptionPane.showMessageDialog(null, "Es haben alle Monster verloren!", "..::: Spiel beendet :::..",
				JOptionPane.ERROR_MESSAGE);
		this.closeGame();
	}

	
	//MessageIN: Wer ist als n�chste an der Reihe?
	//Beispiel-Message: GameActivePlayer|CyberBunny  --> CyberBunny
	public void activePlayer(String message) {
		if (Monster.valueOf(message) == this.getActiveMonster() && this.round == 0) {
			CardBoard_Controller.getController().initialCardSetup();
		}
		this.round++;
	}

	
	//Hilfsmethode
	public int getRound() {
		return this.round;
	}

	
	//Hilfsmethode: Aktives Monster ausfindig machen
	public Monster getActiveMonster() {
		for (Monster monster : Monster.values()) {
			if (monster.getBelongsToThisPlayer()) {
				return monster;
			}
		}
		return null;
	}

	
	//Beim schliessen des Game-Fensters wird GamePlayerLeave Message ausgel�st.
	public void windowClosing(WindowEvent arg0) {
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		int confirm = JOptionPane.showOptionDialog(this, "Wollen Sie das Spiel wirklich verlassen?", "Best�tigung",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
		//Message ausl�sen und Game schliessen bei Ja-Option
		if (confirm == JOptionPane.YES_OPTION) {
			Lobby.getLobby().setGameActiv(false);
			String message = String.valueOf(Services.GamePlayerLeave) + "|";
			message += this.getActiveMonster();
			Server.getServer().send(message);
			
			this.closeGame();
		} else {
			// Schliessen verhindern bei Nein-Option.
			this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		}
	}
	
	
	public void windowActivated(WindowEvent arg0) {}
	public void windowClosed(WindowEvent arg0) {}
	public void windowDeactivated(WindowEvent arg0) {}
	public void windowDeiconified(WindowEvent arg0) {}
	public void windowIconified(WindowEvent arg0) {}
	public void windowOpened(WindowEvent arg0) {}

}// end GameBoard