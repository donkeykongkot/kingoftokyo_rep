package ch.fhnw.itprojekt15.DonkeyKong.GameBoard;

public interface Constants_GameBoard {
	
		public static final String textGuiName = "Gameboard";
		public static final String textPlayerLeave = "Spieler hat das Spiel verlassen!";
		public static final String textPlayerSuspended = "Spieler wurde suspendiert!";
		public static final String textPlayerWon = "Du hast gewonnen!";
		public static final String textPlayerLost = "Du hast verloren!";
		public static final String textCountdown = "Anzahl Sekunden f�r deinen Spielzug";
		public static final String textGuiCountodwn = "Spiel Countdown";
		
}