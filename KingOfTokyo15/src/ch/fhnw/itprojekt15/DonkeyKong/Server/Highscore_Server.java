package ch.fhnw.itprojekt15.DonkeyKong.Server;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

import ch.fhnw.itprojekt15.DonkeyKong.Database.Constants_Database;
import ch.fhnw.itprojekt15.DonkeyKong.Database.Database_Access;

/**
 * Mit dieser Klasse wird der Highscore aktualisiert �ber Daternbank wird abgerufen und 
 * und die message wird zur�ckgesendet.
 * 
 * @author Roman
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class Highscore_Server implements Constants_Server, Constants_Database, Constants_Services {

	private String message;
	private static Highscore_Server singelton;
	private Socket socket;


	private Highscore_Server() {

	}

	public static Highscore_Server getHighscoreServer() {
		if (singelton == null) {
			singelton = new Highscore_Server();
			return singelton;
		}
		return singelton;
	}

	/**
	 * 
	 * @param message
	 * @param socket
	 */
	//Daten werden empfangen
	protected void requestData(String message, Socket socket) {

		this.socket = socket;
		this.message = message;
		sendData();

	}
	
	
// Messsage mit Highscore zur�ck senden
	private void sendData() {
		this.message = highscoreSendData + delimiter1 + Database_Access.getController().selectWins();
		Message.sendToOne(message, this.socket);

	}

}// end Highscore_Server