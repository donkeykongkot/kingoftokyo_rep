package ch.fhnw.itprojekt15.DonkeyKong.Server;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.ArrayList;

import ch.fhnw.itprojekt15.DonkeyKong.Server.Constants_Services.Services;

/**
 * Die statischen Methoden dieser Klasse senden Messages an bestimmte Sockets.
 * 
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class Message {

	protected static void sendToAll(String message, Socket[] sockets) {
		for (Socket socket : sockets) {
			sendToOne(message, socket);
			try {
				Thread.sleep(150);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	protected static void sendToAll(String message) {
		ArrayList<Client_Listener> cls = Server.getServer().getClients();

		for (Client_Listener cl : cls) {
			sendToOne(message, cl.getSocket());
		}
	}

	protected static void sendToAll(String message, ArrayList<Socket> sockets) {
		for (Socket socket : sockets) {
			sendToOne(message, socket);
		}
	}

	protected static void sendToOne(String message, Socket socket) {
		if (socket.isConnected()) {
			try {
				OutputStream os = socket.getOutputStream();
				OutputStreamWriter osw = new OutputStreamWriter(os);
				BufferedWriter bw = new BufferedWriter(osw);
				bw.write(message + "\n"); // Befehl beenden
				bw.flush(); // SENDEN
			} catch (IOException e) {
				Client_Listener gone = null;
				for (Client_Listener cl : Server.getServer().getClients()) {
					if (cl.getSocket().equals(socket)) {
						gone = cl;
						break;
					}
				}
				if (gone != null) {
					gone.searchService(Services.LeaveLobby + "|" + Services.LeaveLobby);
					if (gone.getMonster() != null) {
						gone.searchService(Services.GamePlayerLeave + "|" + gone.getMonster().type);
					}
				}
			}
		}
	}
}// end Message
