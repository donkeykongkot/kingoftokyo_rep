package ch.fhnw.itprojekt15.DonkeyKong.Server;

/**
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public interface Constants_Server {

	public static final int port = 42935;
	public static final String delimiter1 = "|";
	public static final String delimiter2 = ",";
	public static final String delimiter3 = ";";
	public static final String loginUserNotFound = "LoginUserNotFound";
	public static final String loginPasswordWrong = "LoginPasswordWrong";
	public static final String loginOK = "LoginOK";
	public static final String registrationUserAlreadyExists = "RegistrationUserAlreadyExists";
	public static final String registrationOK = "RegistrationOK";
	public static final String highscoreSendData = "HighscoreSendData";
	public static final String playerListSendData = "PlayerListSendData";
	public static final String gameRequestJustOnePlayer = "GameRequestJustOnePlayer";
	public static final String countdownGameRequest = "CountdownGameRequest";
	public static final String gameStartGame = "GameStartGame";
	public static final String gamePlayerLeave = "GamePlayerLeave";
	public static final String countdownGame = "CountdownGame";
	public static final String gamePlayerSuspended = "GamePlayerSuspended";
	public static final String gameConnectionLost = "GameConnectionLost";
	public static final String gameActivePlayer = "GameActivePlayer";
	public static final String gameDiceTokyoBonus = "GameDiceTokyoBonus";
	public static final String gameTokyoLeaveQuestion = "GameTokyoLeaveQuestion";
	public static final String tokyoGo = "TokyoGo";
	public static final String cardBuyQuestion = "CardBuyQuestion";
	public static final String gameProoverMonsterLoose = "GameProoverMonsterLoose";
	public static final String gameProoverMonsterWin = "GameProoverMonsterWin";
	public static final String loginAlreadyLoggedIn = "LoginAlreadyLoggedIn";

}