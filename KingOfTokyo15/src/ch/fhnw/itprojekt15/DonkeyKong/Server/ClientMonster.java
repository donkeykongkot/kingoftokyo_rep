package ch.fhnw.itprojekt15.DonkeyKong.Server;

import java.net.Socket;

import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Constants_Monster.Monster;

/**
 * Server-seitiges Monster-Objekt mit g�ngigen Attributen.
 * @author Andy
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class ClientMonster {

	public final Monster type;
	private int life;
	private Socket socket;
	private int victoryPoints;
	private int energy;
	protected Client_Listener listener;
	
	protected ClientMonster(Monster type, Socket socket, Client_Listener listener){
		this.type = type;
		this.socket = socket;
		this.life = 10;
		this.victoryPoints = 0;
		this.energy = 0;
		this.listener = listener;
	}
	
	protected Client_Listener getClientListener(){
		return this.listener;
	}
	
	protected Socket getSocket(){
		return this.socket;
	}
	protected int getLifes(){
		return this.life;
	}
	protected int getVictoryPoints(){
		return this.victoryPoints;
	}
	protected int getEnergy(){
		return this.energy;
	}
	protected void setLifes(int diff){
		this.life += diff;
	}
	protected void setVictoryPoints(int diff){
		this.victoryPoints += diff;
	}
	protected void setEnergy(int diff){
		this.energy += diff;
	}
	
}//end ClientMonster