package ch.fhnw.itprojekt15.DonkeyKong.Server;

import java.net.Socket;
import java.util.ArrayList;

/**
 * Hier werden die Daten zusammengestellt, die clientseitig in die Player-List-Tabellen abgebildet werden
 * 
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class PlayerList_Server implements Constants_Server {

	private String message;
	private static PlayerList_Server singelton;
	private Socket socket;
	private ArrayList<Client_Listener> clients;



	
	private PlayerList_Server(){

	}

	public static PlayerList_Server getPlayerList(){
		if(singelton ==null){
			singelton = new PlayerList_Server();
			return singelton;
		}
		return singelton;
	}

	//Client w�nscht PlayerList-Daten
	protected void requestData(String message, Socket socket){
		this.socket = socket;
		System.out.println("PlayerList sendData wird aufgerufen");
		this.sendData();
	}

	//PlayerList-Daten werden zusammengestellt, anhand den Eigenschaften,
	//die auf den Client_Listener gespeichert sind
	private void sendData(){
		
		this.message = playerListSendData + delimiter1;
		System.out.println("Message wird erstellt (PlayerList)");
		this.clients = Server.getServer().getClients();
		System.out.println("Es wurden alle Client_Listener abgeholt (ArrayList)");
		
		//Die Daten werden nun iterativ aufgrund Infos von jedem existierenden Client-Listener zusammengestellt.
		for (Client_Listener cl: this.clients){
			if (cl.getUsername() != null){
			this.message += cl.getUsername() + delimiter2;
			//weiter mit Zeitberechnung und anschliessender �bergabe an Message
			this.message += this.calcSessionDuration(cl.getConnectionSince());
			this.message += delimiter3;}
			
		}
		this.message = message.substring(0, message.length()-1);
		
		Message.sendToAll(this.message);
	}
	
	//Hier wird berechnet, wie lange der einzelne Client bereits online ist.
	private String calcSessionDuration(long connectionSince){
		long now = System.currentTimeMillis()/1000;
		long calc = (now - connectionSince/1000);
		String hours;
		String minutes;
		String seconds;
		
		String duration;
		
		hours = calc/3600 + "";
		calc = calc%3600;
		
		if (calc/60 < 10){
			minutes = "0" + calc/60;
		} else {minutes = calc/60 + "";}
		calc = calc%60;
		
		if(calc < 10){
			seconds = "0" + calc;
		} else{seconds = calc + "";}
		
		duration = hours + ":" + minutes + ":" + seconds;
		
		return duration;
		}
		
}//end PlayerList_Server