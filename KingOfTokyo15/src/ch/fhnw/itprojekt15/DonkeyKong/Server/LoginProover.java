package ch.fhnw.itprojekt15.DonkeyKong.Server;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

import ch.fhnw.itprojekt15.DonkeyKong.Database.Constants_Database;
import ch.fhnw.itprojekt15.DonkeyKong.Database.Database_Access;

/**
 * validation:
 * Die Methode validation pr�ft die Eingagben im Loginfenster und f�hrt jenachdem
 *  die drei methoden userNotFound, passwordWrong oder loginOK aus.
 * 
 * 
 * @author Roman
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class LoginProover implements Constants_Server, Constants_Database{

	private String message;
	private static LoginProover singelton;
	private Socket socket;
	private String username;
	private String password;
	private boolean isNotLoggedIn;
	

	
	private LoginProover(){

	}

	public static LoginProover getLoginProover(){
		if(singelton == null){
			singelton = new LoginProover();
			return singelton;
		}
		
		return singelton;
	}

	// Logindaten sind korrekt
	private void ok(){
		ArrayList<Client_Listener> clients = Server.getServer().getClients();
		
		alreadyIn:
		for (Client_Listener cl: clients){
			if(this.username.equals(cl.getUsername())){
				this.alreadyLoggedIn();
				this.isNotLoggedIn = false;
				break alreadyIn;
			} else {
				this.isNotLoggedIn = true;
			}
		}
		
		if(this.isNotLoggedIn){
		for (Client_Listener cl: clients){
			if(cl.getSocket() == this.socket){
				cl.setUsername(this.username);
				break;
			}				
		}
				
		String message;
		message = loginOK + delimiter1 + loginOK;
		Message.sendToOne(message, this.socket);
		}
	}

	//Bereits eingeloggt
	private void alreadyLoggedIn() {
		String message;
		message = loginAlreadyLoggedIn + delimiter1 + loginAlreadyLoggedIn;
		Message.sendToOne(message, this.socket);
		
	}

	//Passwort ist falsch
	private void passwordWrong(){		
		
		String message;
		message = loginPasswordWrong + delimiter1 + loginPasswordWrong;
		Message.sendToOne(message, this.socket);
	}

	
	//Benutzername ist nicht korrekt
	private void userNotFound(){		
		
		String message;
		message = loginUserNotFound + delimiter1 + loginUserNotFound;
		Message.sendToOne(message, this.socket);

	}

	/**
	 * 
	 * @param socket
	 * @param message
	 */
	protected synchronized void validation(Socket socket, String message){
	this.socket = socket;
	
	//Message auslesen
	Scanner scanner = new Scanner(message);
	scanner.useDelimiter(delimiter2);
	username = scanner.next();
	password = scanner.next();
	
	//Stimmt Username �berein
	if(Database_Access.getController().checkUsername(username)==false){
		this.userNotFound();
		
	//Stimmt Password �berein
	}else if(Database_Access.getController().checkPassword(username, password)==false){
		this.passwordWrong();
	
	//Alles korrekt
	}else{
		this.ok();
	}
	
	}
}//end LoginProover