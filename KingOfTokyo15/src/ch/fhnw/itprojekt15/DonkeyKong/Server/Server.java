package ch.fhnw.itprojekt15.DonkeyKong.Server;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import ch.fhnw.itprojekt15.DonkeyKong.Database.Database_Access;
import ch.fhnw.itprojekt15.DonkeyKong.Server.Constants_Services.Services;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Constants_Services.Service;

/**
 * Diese Klasse, wartet auf neue Clients, die sich anmelden.
 * Der Server erstellt danach einen Client-Listener und speichert diesen
 * in der ArrayList (clients)
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class Server extends Thread implements Constants_Server {

	private ServerSocket serverSocket;
	protected ArrayList<Client_Listener> clients;
	private Client_Listener clientListener;
	private Socket socket;
	private boolean keepOnHeartBeat;
	private Thread heartbeat;
	private static Server singelton;

	private Server() {
		this.clients = new ArrayList<Client_Listener>();
		Database_Access.getController().initDB();
		
		//Heartbeat, um zu schauen, ob ein Client keine Connection mehr haufweist.
		this.keepOnHeartBeat = true;
		this.heartbeat = new Thread(new Runnable() {
			public void run() {
				while(true){
				Message.sendToAll(String.valueOf(Services.HeartbeatFromServer) + "|" + String.valueOf(Services.HeartbeatFromServer));
					try {	
						Thread.sleep(15000);
					} catch (Exception e) {
						System.out.println("Thread.sleep beim heartbeat ist fehlgeschlagen");
					}
				}
			}
		});
		this.heartbeat.start();
		this.start();
	}

	public void run() {
		this.listenNewClients();
	}

	//Sobald diese Methode aufgrund einer Message des Clients aufgerufen wird. Wird der Client
	//sozusagen entfernt. Socket wird geschlossen und der Client_Listener wird in der Client-
	//ArrayList gel�scht.
	public void leaveLobby(String message, Socket socket) {
		Client_Listener toRemove = null;
		for (Client_Listener cl : this.clients) {
			if (cl.getSocket().equals(socket)) {
				toRemove = cl; //zu l�schender Client_Listener wird zwischengespeichert
				break;
			}
		}
		
		//Zwischengespeichert Client_Listener wird erst jetzt gel�scht (nach for-Schleife)
		if (toRemove != null) {
			try {
				Thread.sleep(100);
				this.clients.remove(toRemove);
				toRemove.getSocket().close();
				System.out.println("ClientListener entfernt und Socket geschlossen.");
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else{
		System.out.println("Keinen clientlistener gefunden");
		}
	}

	//Wartet auf neuen Client
	private void listenNewClients() {
		try {
			this.serverSocket = new ServerSocket(port);
			System.out.println("Ready for Connections on IP " + InetAddress.getLocalHost().getHostAddress());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			System.out.println("Es geht was nicht beim ServerSocket");
		}
		if (this.serverSocket != null) {
			try {
				while (true) {
					this.socket = this.serverSocket.accept();
					this.clientListener = new Client_Listener(this.socket);
					this.clients.add(this.clientListener);
					System.out.println("Klasse Server auf Server hat soeben ClientListener in ArrayList gespeichert.");
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("etwas mit accep funktioniert nicht");
				;
			}
		}
	}

	public static Server getServer() {
		if (singelton == null) {
			singelton = new Server();
		}
		return singelton;
	}

	protected ArrayList<Client_Listener> getClients() { 
		return this.clients;
	}

}// end Server