package ch.fhnw.itprojekt15.DonkeyKong.Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Scanner;

import ch.fhnw.itprojekt15.DonkeyKong.Server.Constants_Services.Services;

/**
 * F�r jeden angemeldeten Client wird vom Server ein Client_Listener instanziiert.
 * Hier k�nnen Eigenschaften des jeweiligen Clients hinterlegt werden.
 * Alle Client_Listener-Objekte werden in einer ArrayListe auf dem Server-Objekt gespeichert.
 * 
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class Client_Listener extends Thread implements Constants_Services, Constants_Server {

	private BufferedReader inputMessage;
	private String message;
	private Socket socket;
	private String username;
	private long connectionSince;
	private Game game;
	private ClientMonster clientMonster;

	/**
	 * 
	 * @param socket
	 */
	protected Client_Listener(Socket socket) {
		this.socket = socket;
		this.connectionSince = System.currentTimeMillis();
		try {
			this.inputMessage = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.start();
	}

	//Thread der Wartet, bis eine Message vom Client reinkommt
	private void listen() {
		try {
			while ((this.message = inputMessage.readLine()) != null) {
				System.out.println("Server: " + message);
				this.searchService(this.message);
			}
		} catch (IOException e) {
			this.searchService(Services.LeaveLobby + "|" + Services.LeaveLobby);
			if (this.clientMonster != null) {
				this.searchService(Services.GamePlayerLeave + "|" + this.clientMonster.type);
			}
		}
	}

	public void run() {
		this.listen();
	}

	//Sobald vom Client eine Message reinkommt, wird anhand des Services (erster Teil der Message)
	//im interface Constant_Services im Enum Services geschaut, welche Methode welcher Klasse aufgerufen werden muss
	protected void searchService(String message) {

		Scanner scanner = new Scanner(message);
		scanner.useDelimiter("\\" + delimiter1);

		String servicePart = scanner.next();
		String dataPart = scanner.next();

		Services service = Services.valueOf(servicePart);
		
		service.searchLobbyMethodes(servicePart, dataPart, this.socket);

		if (this.game != null) {
			if (this.game.finalState) {
				this.game = null;
			} else {
				this.game.searchGameService(message);
			}
		}
	}

	protected Socket getSocket() {
		return socket;
	}

	protected String getUsername() {
		return username;
	}

	protected void setUsername(String username) {
		this.username = username;
	}

	protected long getConnectionSince() {
		return connectionSince;
	}

	protected void setConnectionSince(long connectionSince) {
		this.connectionSince = connectionSince;
	}

	protected void setGame(Game game) {
		this.game = game;
	}

	protected void killGame() {
		this.game = null;
	}

	protected void setMonster(ClientMonster monster) {
		this.clientMonster = monster;
	}

	protected void killMonster() {
		this.clientMonster = null;
	}

	protected ClientMonster getMonster() {
		return this.clientMonster;
	}

}// end Client_Listener