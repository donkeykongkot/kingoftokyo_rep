package ch.fhnw.itprojekt15.DonkeyKong.Server;

import java.net.Socket;

/**
 * 
 * Dieser Thread schickt alle Sekunden den Countdown an die Clients, die dieser Gameinstanzt zugeordnet sind.
 * Diesem Thread wird das ganze Game �bergeben.
 * Dieser Thread wird vom Game aufgerufen.
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class Countdown_Game extends Thread implements Constants_Server{

	private boolean runCountdowndGameRequest;
	private int countdown;
	private Game game;
	private ClientMonster clientMonster;

	public Countdown_Game(Game game, ClientMonster clientMonster){
		this.runCountdowndGameRequest = true;
		this.countdown = 30;
		this.game = game;
		this.clientMonster = clientMonster;
		this.start();
	}

	//Message an Clients jede Sekunden
	protected void countdown(){
		while(this.runCountdowndGameRequest && !this.game.finalState){
			String message;
			message = countdownGame + delimiter1 + this.countdown + delimiter2 + this.clientMonster.type;
			System.out.println(message);
			Message.sendToAll(message, this.game.sockets);
			
			//Sobald der Countdown auf null ist, wird auf dem Game-Onjekt die playerSuspendet-Methode aufgerufen
			//Dadurch wird dem aktiven Spieler mitgeteilt, dass seine Zeit abgelaufen ist.
			//Der aktive Spieler wird suspendiert.
			if(this.countdown == 0){
				this.runCountdowndGameRequest = false;
				this.game.playerSuspended(this.clientMonster);
			}
			try {
				Thread.sleep(990);
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			this.countdown--;
		}
	}
	
	//Countdown wird gestoppt durch Game, sobald der aktive Spieler rechtzeitig geantwortet hat.
	protected void stopCountdown(){
		this.runCountdowndGameRequest = false;
	}

	public void run(){
		this.countdown();
	}
	
}//end Countdown_Game