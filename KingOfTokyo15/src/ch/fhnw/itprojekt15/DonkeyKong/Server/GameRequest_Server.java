package ch.fhnw.itprojekt15.DonkeyKong.Server;

import java.net.Socket;
import java.util.ArrayList;

/**
 * Sobald ein Client einem Spiel beitreten m�chte, wird das Socket einer ArrayList hinzugef�gt. 
 * Danach wird gepr�ft, ob erst einSocket im Array vorhanden ist. Wenn ja, wird die Methode 
 * justOnePlayer aufgerufen. Sollte es der zweite sein, wird ein Objekt Countdown_GameRequest
 * instanziert. Wird das Array mit sechs Sockets gef�llt, so wird startGame
 * ausgef�hrt und somit ein neues Game instanziert und die Sockets �bergeben.
 * Danach wird die ArrayList wieder geleert. 
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class GameRequest_Server implements Constants_Server{

	
	private static GameRequest_Server singelton;
	protected ArrayList<Socket> sockets;
	private Countdown_GameRequest countdown;
	
	

	private GameRequest_Server(){
		this.sockets = new ArrayList<Socket>();
	}

	protected static GameRequest_Server getGameRequest(){
		if (singelton == null){
			singelton = new GameRequest_Server();
			return singelton;
		}
		return singelton;
	}

	//Nur ein Socket ist in der ArrayList
	protected void justOnePlayer(){
		String message;
		message = gameRequestJustOnePlayer + delimiter1 + gameRequestJustOnePlayer;
		Message.sendToOne(message, this.sockets.get(0));
	}

	//Spielanfrage eines Clients. Entweder wird justOnePlayer()-Methode aufgerufen
	// (erst ein Spieler), Countdown gestartet (2-5 Sockets), oder Game gestartet (6 Sockets)
	protected void request(String message, Socket socket){
		if(this.sockets.isEmpty()){
			this.sockets.add(socket);
			this.justOnePlayer();
		} else if(this.sockets.size() == 5){
			this.sockets.add(socket);
			this.countdown.stopCountdown();
			this.startGame();
		} else if(this.sockets.size() == 1) {
			this.sockets.add(socket);
			this.countdown = new Countdown_GameRequest(this);
		} else{
			this.sockets.add(socket);
		} 
	}

	//Game wird gestartet und Sockets �bergeben, danach wird ArrayList geleert.
	protected void startGame(){
		new Game(this.sockets);
		this.sockets.removeAll(sockets);
		
	}
	
	//Das Socket des Clients, dass soeben Lobby verlassen hat, wird wieder aus der ArrayList entfernt
	protected void leaveLobby (String message, Socket socket){
		Socket toRemove = null;
		
		RemoveCheck:
		for(Socket playerSocket: this.sockets){
			if (playerSocket.equals(socket)){
				toRemove = playerSocket; //Zwischenspeichern
				break RemoveCheck;
			}
		}
		
		if(toRemove != null){
			this.sockets.remove(toRemove);
			System.out.println("Socket aus GameRequest-Liste gel�scht");
		} else {
			System.out.println("Socket war nicht in einer GameRequest-Liste.");		}
	}
}//end GameRequest_Server