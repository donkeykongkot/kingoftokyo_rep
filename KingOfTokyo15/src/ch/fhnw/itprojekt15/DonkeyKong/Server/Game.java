package ch.fhnw.itprojekt15.DonkeyKong.Server;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import ch.fhnw.itprojekt15.DonkeyKong.CardBoard.Constants_Cards;
import ch.fhnw.itprojekt15.DonkeyKong.CardBoard.Constants_Cards.Card;
import ch.fhnw.itprojekt15.DonkeyKong.Database.Constants_Database;
import ch.fhnw.itprojekt15.DonkeyKong.Database.Database_Access;
import ch.fhnw.itprojekt15.DonkeyKong.DiceBoard.Constants_Dices;
import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Constants_Monster.Monster;
import ch.fhnw.itprojekt15.DonkeyKong.Server.Constants_Services.Services;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Constants_Services;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Constants_Services.Service;

/**
 * Diese Klasse ist das Herzst�ck der serverseitigen Spielimplementierung. Es
 * empf�ngt die Messages von den Clients, verarbeitet sie und schickt Updates an
 * alle Clients heraus.
 * 
 * Spielprinzip: Initiale Meldung schicken; der aktive Spieler antwortet (durch
 * entsprechendes Freigeben des GUIs beim aktiven Spieler und Sperren des GUIs
 * der wartenden Spieler). Der Server bearbeitet lediglich diese eine Antwort
 * und schickt einen Folge-Impuls an den Client. Die eigentliche Spiellogik
 * entsteht durch Verbinden solcher Frage-Antwort Ketten.
 * 
 * Konstruktor: Anhand des Socket-Array vom GameRequest-Server und dem
 * monsters-Array vom Interface werden in einer for-Schlaufe so viele
 * ClientMonster generiert, wie im Socket-Array Anzahl Sockets sind. Diese
 * ClientMonster werden wiederum in einem eigenen Array gespeichert.
 * Verbindungsst�ck zwischen dem serverseitigen Listener und dem entsprechenden
 * Game ist die auf dem Client_Listener gespeicherten Spielinstanz, womit die
 * Zugeh�rigkeit von einem Spieler zu einem Spiel geregelt wird.
 * 
 * Danach wird die Hilfsmethode startGame ausgef�hrt.
 * 
 * @author Andy
 * @version 9.0
 * @created 06-Nov-2015 12:16:11
 */
public class Game implements Constants_Server, Constants_Database {

	protected ClientMonster activePlayer;
	protected ClientMonster monsterInTokyo;
	protected ArrayList<ClientMonster> clientMonsters = new ArrayList<ClientMonster>();
	private static int games = 0;
	private int gameID;
	public ArrayList<Client_Listener> gameListeners = new ArrayList<Client_Listener>();
	protected ArrayList<Socket> sockets = new ArrayList<Socket>();
	protected boolean finalState = false;
	private static final int victoryPointsToWin = 20;
	private static final int maxLifes = 10;
	private int round = 0;
	private boolean goTokyoInThisTurn;
	private boolean damageByCard;
	private boolean damageByDices;
	private Countdown_Game countdown;


	protected Game(ArrayList<Socket> sockets) {
		this.gameID = games++;
		System.out.println("Game ID " + this.gameID); // DEBUG

		// Generiere eine Komplettliste aller Monster
		ArrayList<Monster> monsters = new ArrayList<Monster>();
		for (Monster genericMonster : Monster.values()) {
			monsters.add(genericMonster);
		}

		for (Socket socket : sockets) {
			this.sockets.add(socket);
			Client_Listener listener = null;
			// Finde zugeh�rigen Client Listener
			for (Client_Listener cl : Server.getServer().getClients()) {
				if (cl.getSocket() == socket) {
					listener = cl;
					break;
				}
			}

			// Weise einen zuf�lligen Monster-Typen zu.
			Monster monster = monsters.get((int) (Math.random() * monsters.size()));
			ClientMonster cm = new ClientMonster(monster, socket, listener);
			this.clientMonsters.add(cm);

			// Setze dieses Game & das Monster in den serverseitigen Listener
			listener.setGame(this);
			listener.setMonster(cm);
			monsters.remove(monster);
		}

		for (ClientMonster cm : this.clientMonsters) {
			// Schickt an jeden Client den Startimpuls
			// Aufbau: GameStartGame|<eigenes Monster>;<Liste aller teilnehmenden Monster>
			String message = Services.GameStartGame + delimiter1 + cm.type + delimiter3 + this.getMonsters();
			Message.sendToOne(message, cm.getSocket());
		}
		this.startGame();
	}

	public void searchGameService(String message) {
		String[] elements = message.split("\\" + delimiter1);
		if (elements.length >= 2) {
			String service = elements[0];
			if (Constants_Services.checkENUMCompatibility(Constants_Services.Service.values(), service)) {
				switch (Constants_Services.Service.valueOf(service)) {
				case GamePlayerLeave:
					this.playerLeave(message);
					break;
				case GamePlayerSuspended:
					this.playerSuspended(message);
					break;
				case DiceRoll:
					this.diceRoll(message);
					break;
				case DiceRollFinal:
					this.diceRollFinal(message);
					break;
				case GameTokyoLeaveQuestion:
					this.tokyoLeaveQuestion();
					break;
				case TokyoLeaveAnswer:
					this.tokyoLeaveAnswer(message);
					break;
				case TokyoGo:
					this.tokyoGo(message);
					break;
				case TokyoLeave:
					this.tokyoLeave();
					break;
				case CardBuyQuestion:
					this.cardBuyQuestion();
					break;
				case CardBuyAnswer:
					this.cardBuyAnswer(message);
					break;
				case CardNewCards:
					this.newCards(message);
					break;
				// case CardOneNewCard:
				// break;
				case CardBought:
					this.cardBought(elements[1]);
					break;
				case GameProoverMonsterLoose:
					break;
				case GameProoverMonsterWin:
					this.finalState = true;
					break;
				case Chat:
					String[] outer = message.split("\\" + delimiter2);
					String inner = message.substring(outer[0].length() + 1);
					String[] testing = inner.split("\\" + delimiter1);
					if (testing.length > 1) {
						// Wenn Trennzeichen | mehrfach vorkommt = Indiz f�r Testing
						String srv = testing[0]; // Falls Testing, dann="Testing"
						String newCall = inner.substring(testing[0].length() + 1);
						if (srv.equals(String.valueOf(Service.Testing))) {
							this.searchGameService(newCall);
						} else {
							// Es war doch kein Testing-String
							this.chat(message);
						}
					} else {
						this.chat(message);
					}
					break;
				case StopCountdown:
					this.countdown.stopCountdown();
					break;
				case GameActivePlayer:
					this.nextPlayer();
					break;
				default:
					break;
				}
			}
		}
	}

	protected void startGame() {
		if (this.clientMonsters.size() > 0) {
			this.activePlayer = this.clientMonsters.get(0);
			this.sendActivePlayer();
		} else {
			System.out.println("Game ohne Spieler!");
			return;
		}
	}

	///// GETTER
	private String getMonsters() {
		if (this.clientMonsters.size() > 0) {
			String monsters = new String();
			for (ClientMonster cm : this.clientMonsters) {
				monsters += cm.type + delimiter2 + cm.getClientListener().getUsername() + delimiter3;
			}
			return monsters.substring(0, monsters.length() - 1); // Ohne Komma
		} else {
			return null;
		}
	}

	protected Socket[] getSocketsArray() {
		this.cleanupClosedSockets(); // L�sche zuerst alle geschlossenen Sockets.
		Socket[] sockets = new Socket[this.sockets.size()];
		for (int i = 0; i < this.sockets.size(); i++) {
			sockets[i] = this.sockets.get(i);
		}
		return sockets;
	}

	private String getPlayerData(ClientMonster cm) {
		String update = Services.PlayerDataUpdate + delimiter1 + cm.type + delimiter2;
		update += cm.getLifes() + delimiter2 + cm.getVictoryPoints() + delimiter2 + cm.getEnergy();
		return update;
	}

	///// GAME-Hilfsmethoden

	protected void sendActivePlayer() {
		// Sendet den aktiven Spieler.
		String message = Services.GameActivePlayer + delimiter1 + this.activePlayer.type;
		Message.sendToAll(message, this.getSocketsArray());
		this.countdown = new Countdown_Game(this, this.activePlayer);
	}

	protected void connectionLost() {
		this.cleanupClosedSockets();
	}

	protected void playerLeave(String message) {
		Message.sendToAll(message, this.getSocketsArray());
		
		// Aufr�umarbeiten
		String[] str = message.split("\\" + delimiter1);
		if (str.length > 1) {
			ClientMonster cm = this.getClientMonsterFromString(str[1]);
			System.out.println("Leave: " + cm.type);
			if (cm != null) {
				cm.setLifes((cm.getLifes() * (-1))); // Null Leben
				if (cm == this.activePlayer) {
					System.out.println("Active player: next");
					// Falls der verlassende Spieler am Zug ist.
					this.countdown.stopCountdown();
					this.nextPlayer();
				}
				// L�sche Verweise auf das verlassene Game
				cm.listener.killGame();
				cm.listener.killMonster();
				if (this.checkIfGameIsFinished()) {
					return;
				}
				this.checkIfSomebodyLost();
			}
		}
	}

	protected void nextPlayer() {
		// Aufsteigend durchs Array gehen
		this.cleanupClosedSockets();
		int i = 0;
		for (; i < this.clientMonsters.size(); i++) {
			if (this.clientMonsters.get(i) == this.activePlayer) {
				if (i == this.clientMonsters.size() - 1) {
					i = 0;
					break;
				} else {
					i += 1;
					break;
				}
			}
		}

		if (i < this.clientMonsters.size()) {
			this.activePlayer = this.clientMonsters.get(i);
		}
		this.sendActivePlayer();

		if (this.checkIfGameIsFinished()) {
			return;
		}
		this.checkIfSomebodyLost();

		if (this.checkIfGameIsFinished()) {
			return;
		}
		System.out.println("Spielzug Nr. " + this.round);
	}

	private ClientMonster getClientMonsterFromString(String monster) {
		Monster m = null;
		if (Monster.checkENUMCompatibility(Monster.values(), monster)) {
			m = Monster.valueOf(monster);
		}
		ClientMonster cm = null;
		if (m != null) {
			for (ClientMonster cmonster : this.clientMonsters) {
				if (cmonster.type == m) {
					cm = cmonster;
					break;
				}
			}
		}
		return cm;
	}

	protected void playerSuspended(String message) {
		ClientMonster cm = this.getClientMonsterFromString(message);
		if (cm != null) {
			this.playerSuspended(cm);
		} else {
			System.out.println("PlayerSuspended: CM nicht gefunden: " + message);
		}
	}

	protected void playerSuspended(ClientMonster cm) {
		if (cm != null) {
			cm.setLifes((cm.getLifes() * (-1))); // Null Leben
			String msg = Services.GamePlayerSuspended + delimiter1 + cm.type;
			Message.sendToAll(msg, this.getSocketsArray());

			if (cm == this.activePlayer) {
				this.countdown.stopCountdown();
				this.nextPlayer();
			}
			cm.listener.killGame();
			cm.listener.killMonster();
		}
	}

	protected void cleanupClosedSockets() {

		// Zur Vermeidung einer ConcurrentModificationException
		ArrayList<Socket> toRemove = new ArrayList<Socket>();
		ArrayList<ClientMonster> toRemoveCM = new ArrayList<ClientMonster>();

		for (Socket socket : this.sockets) {
			if (!socket.isConnected()) {
				// Sofern das Socket nicht verbunden ist, soll es gel�scht werden.
				toRemove.add(socket);

				for (ClientMonster cm : this.clientMonsters) {

					// Andere Clients �ber Disconnect informieren.
					if (cm.getSocket().equals(socket)) {
						String playerLeave = Services.GameConnectionLost + delimiter1 + cm.type;
						Message.sendToAll(playerLeave, this.getSocketsArray());
						if (this.monsterInTokyo == cm) {

							// Falls jener Client in Tokyo stand, entfernen.
							this.tokyoLeave();
						}
						toRemoveCM.add(cm);
						break;
					}
				}
				try {
					// Versuche das Socket ordentlich zu schliessen.
					socket.close();
				} catch (IOException io) {
					System.out.println("Can't close socket " + socket.toString());
				}
			}
		}
		for (Socket socket : toRemove) {
			this.sockets.remove(socket);
		}
		for (ClientMonster cm : toRemoveCM) {
			this.clientMonsters.remove(cm);
		}
	}

	protected void updateAllPlayerData() {
		for (ClientMonster cm : this.clientMonsters) {
			// Generelles Update aller Spielerdaten
			this.updatePlayerData(this.getPlayerData(cm));
		}
	}

	protected void updatePlayerData(String message) {
		Message.sendToAll(message, this.getSocketsArray());
	}

	protected boolean checkIfGameIsFinished() {
		if (!this.finalState) {
			String message = null;
			ClientMonster winningMonster = null;
			if (this.clientMonsters.size() == 1) {
				winningMonster = this.clientMonsters.get(0);
			} else {
				for (ClientMonster cm : this.clientMonsters) {
					// Gewonnen hat, wer >20 Ruhmpunkte hat UND �berlebt!
					if (cm.getVictoryPoints() >= victoryPointsToWin && cm.getLifes() > 0) {
						winningMonster = cm;
						break;
					}
				}
				if (winningMonster == null) {
					if (this.clientMonsters.size() == 2) {
						if (this.clientMonsters.get(0).getLifes() <= 0 && this.clientMonsters.get(1).getLifes() > 0) {
							winningMonster = this.clientMonsters.get(1);
						} else if (this.clientMonsters.get(1).getLifes() <= 0
								&& this.clientMonsters.get(0).getLifes() > 0) {
							winningMonster = this.clientMonsters.get(0);
						}
					}
				}
			}
			if (winningMonster != null) {
				// Update Spielerdaten auf dem GUI
				this.updateAllPlayerData();
				// Versende Gewinn-Meldung an alle verbleibenden Spieler
				message = Services.GameProoverMonsterWin + delimiter1 + winningMonster.type;
				Message.sendToAll(message, this.getSocketsArray());
				// Stoppe Countdown
				this.countdown.stopCountdown();
				// L�sche Verweis auf Game, da dieses jetzt beendet ist.
				for (ClientMonster cm : this.clientMonsters) {
					cm.listener.killGame();
					cm.listener.killMonster();
				}				
				this.finalState = true;
				Database_Access.getController()
						.updateWins(this.getUsernameFromClientListener(winningMonster.getSocket()), 1);
				return true;
			}
		}
		return false;
	}

	protected void nobodyWins() {
		String message = String.valueOf(Service.GameProoverNobodyWins) + delimiter1
				+ String.valueOf(Service.GameProoverNobodyWins);
		Message.sendToAll(message, this.getSocketsArray());
		this.countdown.stopCountdown();
	}

	private String getUsernameFromClientListener(Socket socket) {
		for (Client_Listener cl : Server.getServer().getClients()) {
			if (cl.getSocket() == socket) {
				return cl.getUsername();
			}
		}
		return null;
	}

	protected void checkIfSomebodyLost() {
		if (!this.finalState) {
			int howManyLost = 0;
			ArrayList<ClientMonster> toRemove = new ArrayList<ClientMonster>();

			// Vermeidung einer ConcurrentModificationException:
			// Zuerst abfragen, dann separat herausl�schen.
			for (ClientMonster cm : this.clientMonsters) {
				if (cm.getLifes() <= 0) {
					howManyLost++;
					toRemove.add(cm);
				}
			}
			if (howManyLost == this.clientMonsters.size()) {
				if (this.countdown != null) {
					this.countdown.stopCountdown();
				}
				this.updateAllPlayerData();
				this.nobodyWins();
				this.finalState = true;
				return;
			}
			for (ClientMonster cm : toRemove) {
				if (cm.getLifes() <= 0) {
					this.updatePlayerData(this.getPlayerData(cm));
					String message = Services.GameProoverMonsterLoose + delimiter1 + cm.type;
					Message.sendToAll(message, this.getSocketsArray());
					this.sockets.remove(cm.getSocket());
					this.clientMonsters.remove(cm);
					cm.listener.killGame();
					cm.listener.killMonster();
					if (cm == this.monsterInTokyo) {
						this.tokyoLeave();
						if (this.damageByDices) {
							String goTokyo = Services.TokyoGo + delimiter1 + this.activePlayer.type;
							this.tokyoGo(goTokyo);
						}
					}
				}
			}
			if (howManyLost > 0) {
				this.updateAllPlayerData();
			}
		}
	}

	protected void stopCountDown() {
		this.countdown.stopCountdown();
	}

	///// W�RFEL
	protected void diceRoll(String message) {
		Message.sendToAll(message, this.getSocketsArray());
		this.countdown.stopCountdown();
		this.countdown = new Countdown_Game(this, this.activePlayer);
	}

	protected void diceRollFinal(String message) {
		this.round++;
		this.goTokyoInThisTurn = false;
		this.damageByCard = false;
		this.damageByDices = false;

		this.countdown.stopCountdown();

		String[] diceStrings = message.split("\\" + delimiter1);
		if (diceStrings.length > 1) {
			Message.sendToAll(message, this.getSocketsArray());

			// Das Monster in Tokyo erh�lt 2 Ruhmpunkte.
			this.diceTokyoBonus();

			// Hat irgendwer verloren oder gewonnen?
			if (this.checkIfGameIsFinished()) {
				return;
			}
			this.checkIfSomebodyLost();

			int lifesDef = 0;
			int energyDef = 0;
			int fameDef = 0;
			int attackDef = 0;

			Constants_Dices.GenericDiceValues[] dices = new Constants_Dices.GenericDiceValues[Constants_Dices.noOfDices];

			int[] results = new int[Constants_Dices.GenericDiceValues.values().length];
			String[] elements = diceStrings[1].split("\\" + delimiter2);

			// Parse erst alle String-W�rfelwerte in ein Dice-Array
			for (int i = 0; i < elements.length; i++) {
				if (Constants_Dices.checkENUMCompatibility(Constants_Dices.GenericDiceValues.values(), elements[i])) {
					dices[i] = Constants_Dices.GenericDiceValues.valueOf(elements[i]);
				} else {
					System.out.println("Falscher W�rfelwert: " + elements[i]);
				}
			}

			/**
			 * Dann: Gehe jeden generischen W�rfelwert durch und pr�fe, ob
			 * dieser Wert erw�rfelt wurde. Falls ja, erh�he den Counter im
			 * Resultat-Array um 1.
			 */
			for (int i = 0; i < Constants_Dices.GenericDiceValues.values().length; i++) {
				for (int dice = 0; dice < dices.length; dice++) {
					if (Constants_Dices.GenericDiceValues.values()[i] == dices[dice]) {
						results[i]++;
					}
				}
			}

			/**
			 * Schlussendlich: Gehe erneut alle generischen W�rfelwerte durch
			 * und pr�fe, ob im Resultat-Array ein Vorkommen von >=3 vorliegt.
			 * Gewonnene Ruhmpunkte = numerischer W�rfelwert, welcher mind. 3x
			 * vorkommt. Zus. Check: Jeder weitere identische W�rfelwert: +1.
			 */
			for (int i = 0; i < results.length; i++) {
				if (Constants_Dices.GenericDiceValues.values()[i].isNumeric) {
					if (results[i] >= 3) {
						// Wenn dasselbe W�rfelsymbol 3x erscheint, dann
						// Ruhmpunkte = Zahlenwert
						fameDef += Constants_Dices.GenericDiceValues.values()[i].value;
						// Jeder dar�ber hinausgehende identische W�rfelwert +1
						fameDef += results[i] - 3;
					}
				}
				if (Constants_Dices.GenericDiceValues.values()[i].energy) {
					energyDef = results[i];
				}
				if (Constants_Dices.GenericDiceValues.values()[i].lifes) {
					lifesDef = results[i];
				}
				if (Constants_Dices.GenericDiceValues.values()[i].attack) {
					attackDef = results[i];
				}
			}

			///// Auswertung der W�rfelwerte /////

			// --- ATTACKEN

			if (this.activePlayer == this.monsterInTokyo) {
				// Wenn der aktive Spieler in Tokyo steht, verlieren alle
				// anderen Monster Leben!
				for (ClientMonster cm : this.clientMonsters) {
					if (cm != this.activePlayer) {
						cm.setLifes(attackDef * (-1));
					}
				}
			} else {
				// Wenn der aktive Spieler NICHT in Tokyo steht, verliert das
				// Monster in Tokyo Leben!
				if (this.monsterInTokyo != null) {
					// Sofern niemand in Tokyo steht, bleibt diese Routine
					// ergebnislos. Fortsetzung gleich nachfolgend.
					this.monsterInTokyo.setLifes(attackDef * (-1));
				}
			}

			// Hat irgendwer verloren oder gewonnen?
			if (this.checkIfGameIsFinished()) {
				return;
			}
			this.checkIfSomebodyLost();

			if (this.monsterInTokyo != null && attackDef > 0) {
				this.damageByDices = true;
			}

			if (this.monsterInTokyo == null && attackDef > 0) {
				// Falls Tokyo unbesetzt + Attacke gew�rfelt, wird Tokyo
				// besetzt.
				String msg = String.valueOf(Services.TokyoGo) + delimiter1 + this.activePlayer.type;
				this.tokyoGo(msg);
				this.monsterInTokyo = this.activePlayer;
				this.activePlayer.setVictoryPoints(Constants_Dices.gainedVictoryPointsOnConquerTokyo);
				this.goTokyoInThisTurn = true;
			}

			// ------------ Punktegutschriften ---------------------
			this.activePlayer.setVictoryPoints(fameDef); // Ruhm gutschreiben
			this.activePlayer.setEnergy(energyDef); // Energie gutschreiben

			if (this.activePlayer != this.monsterInTokyo) {
				// Heilung nur dann m�glich, wenn Monster NICHT in Tokyo steht.
				// Heinung bis maximal 10 Leben!
				if (this.activePlayer.getLifes() + lifesDef > maxLifes) {
					// F�lle bis 10 (maxLifes) auf!
					this.activePlayer.setLifes(maxLifes - this.activePlayer.getLifes());
				} else {
					this.activePlayer.setLifes(lifesDef);
				}
			}

			// Hat irgendwer verloren oder gewonnen?
			if (this.checkIfGameIsFinished()) {
				return;
			}
			this.checkIfSomebodyLost();

			this.updateAllPlayerData();

			// AUFRUF DER N�CHSTEN FUNKTION IM SPIELPROZESS:
			this.cardBuyQuestion();
		}
	}

	protected void diceTokyoBonus() {
		if (this.activePlayer == this.monsterInTokyo) {
			this.activePlayer.setVictoryPoints(Constants_Dices.gainedVictoryPointsOnEveryTurn);
		}
	}

	///// KARTEN

	protected void cardBuyQuestion() {
		this.countdown.stopCountdown();
		String buyCard = String.valueOf(Services.CardBuyQuestion) + delimiter1
				+ String.valueOf(Services.CardBuyQuestion);
		Message.sendToOne(buyCard, this.activePlayer.getSocket());
		this.countdown = new Countdown_Game(this, this.activePlayer);
	}

	protected void cardBuyAnswer(String message) {
		this.countdown.stopCountdown();
		boolean buyAgain = false;
		if (message.equals(String.valueOf(Service.CardBuyAnswer) + delimiter1 + String.valueOf(true))) {
			buyAgain = true;
		}
		Message.sendToAll(message, this.getSocketsArray());
		if (buyAgain) {
			this.countdown = new Countdown_Game(this, this.activePlayer);
		} else {
			// Ansonsten mit den n�chsten Spieler weiterfahren.
			this.tokyoLeaveQuestion();
		}
	}

	protected void newCards(String message) {
		// Gebe drei neue Karten aus.
		Message.sendToAll(message, this.getSocketsArray());
		if (this.countdown != null) {
			this.countdown.stopCountdown();
		}
		if (this.round != 0) {
			this.activePlayer.setEnergy(Constants_Cards.newCardsCosts * (-1));
			this.updatePlayerData(this.getPlayerData(this.activePlayer));
			this.cardBuyQuestion();
		}
	}

	protected void cardBought(String message) {
		this.countdown.stopCountdown();
		Message.sendToAll(String.valueOf(Service.CardBought) + delimiter1 + message, this.getSocketsArray());
		this.redeemCard(message);
	}

	protected void redeemCard(String message) {
		// Struktur: CardBought|HEILUNG;Karte1,Karte2,neueKarte3
		// F�r die Einl�sung interessiert nur der 1. Teil des Karten-Strings
		String[] msg = message.split("\\" + delimiter3);
		if (msg[0].length() > 0) {
			if (Constants_Cards.checkENUMCompatibility(Card.values(), msg[0])) {
				Card card = Card.valueOf(msg[0]);
				if (this.activePlayer.getEnergy() + card.getCost() >= 0) {
					// Aktionen f�r aktiven Spieler ausf�hren, sofern
					// Energiekontingent ausreicht.
					this.activePlayer.setEnergy(card.getCost());

					// Heilung nur m�glich bis max. 10 bzw. 0 Leben!
					if (this.activePlayer.getLifes() + card.getOwnHealth() > maxLifes) {
						// Max. 10 (=> Differenz zu 10 errechnen...)
						this.activePlayer.setLifes(maxLifes - this.activePlayer.getLifes());
					} else if (this.activePlayer.getLifes() + card.getOwnHealth() < 0) {
						// Z�hler auf Null bringen
						this.activePlayer.setLifes(this.activePlayer.getLifes() * (-1));
					} else {
						this.activePlayer.setLifes(card.getOwnHealth());
					}
					this.activePlayer.setEnergy(card.getOwnEnergy());
					this.activePlayer.setVictoryPoints(card.getOwnFame());

					if (card.goToTokyo) {
						// Falls Karte die Besetzung von Tokyo fordert
						if (this.monsterInTokyo != this.activePlayer) {
							this.tokyoLeave();
							this.monsterInTokyo = this.activePlayer;

							String tokyoGo = String.valueOf(Services.TokyoGo) + delimiter1 + this.activePlayer.type;
							this.tokyoGo(tokyoGo);

							this.goTokyoInThisTurn = true;
							this.activePlayer.setVictoryPoints(Constants_Dices.gainedVictoryPointsOnConquerTokyo);
						}
						this.updatePlayerData(this.getPlayerData(this.activePlayer));
					}

					for (ClientMonster cm : this.clientMonsters) {
						if (cm != this.activePlayer) {
							if (cm.getLifes() + card.getOtherHealth() >= 0) {
								cm.setLifes(card.getOtherHealth());
							} else {
								// Z�hler auf Null bringen
								cm.setLifes(cm.getLifes() * (-1));
							}
							if (cm.getVictoryPoints() + card.getOtherFame() >= 0) {
								cm.setVictoryPoints(card.getOtherFame());
							} else {
								// Z�hler auf Null bringen
								cm.setVictoryPoints(cm.getVictoryPoints() * (-1));
							}
						}
					}

					if (card.getOtherHealth() != 0) {
						this.damageByCard = true;
					}

					// Hat irgendwer verloren oder gewonnen?
					if (this.checkIfGameIsFinished()) {
						return;
					}
					this.checkIfSomebodyLost();

					this.updateAllPlayerData();

					// Erneute Frage nach Kauf einer Karte.
					this.cardBuyQuestion();
				} else {
					System.out.println("Not enough energy (" + this.activePlayer.getEnergy() + ") for buying card "
							+ card.name + " (" + card.cost + ")");
					this.tokyoLeaveQuestion();
				}
			} else {
				System.out.println("Card not found: " + msg[0]);
				this.tokyoLeaveQuestion();
			}
		}
	}

	////// TOKYO
	protected void tokyoGo(String message) {
		Message.sendToAll(message, this.getSocketsArray());
	}

	protected void tokyoLeaveQuestion() {
		/**
		 * Die Frage, ob das Monster Tokyo verlassen will daruf nur gestellt
		 * werden wenn,
		 * 
		 * 1) Tokyo besetzt ist,
		 * 
		 * 2) Tokyo nicht im selben Spielzug besetzt wurde,
		 * 
		 * 3) ein anderes Monster jenes in Tokyo per W�rfeln angegriffen hat.
		 */

		if (this.monsterInTokyo != null && !this.goTokyoInThisTurn && this.damageByDices
				&& this.activePlayer != this.monsterInTokyo) {
			this.countdown = new Countdown_Game(this, this.monsterInTokyo);
			String message = String.valueOf(Services.GameTokyoLeaveQuestion) + delimiter1
					+ String.valueOf(Services.GameTokyoLeaveQuestion);
			Message.sendToOne(message, this.monsterInTokyo.getSocket());
		} else {
			this.nextPlayer();
		}
	}

	protected void tokyoLeaveAnswer(String message) {
		String[] msg = message.split("\\" + delimiter1);
		this.countdown.stopCountdown();

		// Falls der Spieler Tokyo verlassen will, sendet der Server ein Leave
		if (msg[1].equals(String.valueOf(true))) {
			// Falls Tokyo verlassen werden soll:
			// Wenn das Monster angegriffen wurde, nimmt automatisch das
			// angreifende Monster seinen Platz ein.
			if (this.activePlayer != this.monsterInTokyo && this.damageByDices) {
				this.tokyoLeave();
				String goTokyo = String.valueOf(Services.TokyoGo) + delimiter1 + this.activePlayer.type;
				this.monsterInTokyo = this.activePlayer;
				this.tokyoGo(goTokyo);
			} else {
				this.tokyoLeave();
			}
		} else {
			// Falls der Spieler Tokyo NICHT verlassen will, sendet der Server
			// ein False zur�ck.
			Message.sendToAll(message, this.getSocketsArray());
		}
		this.nextPlayer();
	}

	protected void tokyoLeave() {
		// Hat irgendwer verloren oder gewonnen?
		if (this.checkIfGameIsFinished()) {
			return;
		}
		this.checkIfSomebodyLost();
		if (this.monsterInTokyo != null) {
			this.countdown.stopCountdown();
			String message = String.valueOf(Services.TokyoLeave) + delimiter1 + this.monsterInTokyo.type;
			Message.sendToAll(message, this.getSocketsArray());
			this.monsterInTokyo = null;
		}
	}

	///// CHAT
	protected void chat(String message) {
		Message.sendToAll(message, this.getSocketsArray());
	}

}// end Game