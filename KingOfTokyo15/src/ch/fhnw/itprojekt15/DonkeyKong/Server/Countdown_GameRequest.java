package ch.fhnw.itprojekt15.DonkeyKong.Server;

/**
 * 
 * Der Countdown wird vom GameRequst_Server gestartet. Er schickt Anzahl Spieler und 
 * Countdown den Spiel-interessierten Clients. Sobald der Countdown auf 0 ist, wird startGame
 * Auf dem GameRequest-Objekt aufgerufen. Sollte das Spiel schon fr�her starten (6 Sockets), kann der
 * Countdown vom GameRequest wieder gestoppt werden.
 * 
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class Countdown_GameRequest extends Thread implements Constants_Server  {
	private boolean runCountdowndGameRequest;
	private int countdown;
	private GameRequest_Server gameRequest;

	/**
	 * 
	 * @param gameRequest
	 */
	public Countdown_GameRequest(GameRequest_Server gameRequest){
		this.runCountdowndGameRequest = true;
		this.countdown = 30;
		this.gameRequest = gameRequest;
		this.start();
	}
	
	public void run(){
		this.countdown();
	}

	//Countdown in einer While-Schleife. Der Countdown wird st�ndig an die Clients verschickt.
	protected void countdown(){
		while(this.runCountdowndGameRequest){
			if (this.gameRequest.sockets.size() > 1){
			
			System.out.println("countdown gestartet");
			String message;
			message = countdownGameRequest + delimiter1;
			message += countdown + delimiter2 + this.gameRequest.sockets.size();
			Message.sendToAll(message, this.gameRequest.sockets);
			
			//Wenn Countdown bei 0 --> Game soll starten.
			if(this.countdown == 0){
				this.runCountdowndGameRequest = false;
				this.gameRequest.startGame();
				
			}
			
			try {
				Thread.sleep(990);
			} catch (InterruptedException e) {
						e.printStackTrace();
			}
			
			this.countdown--;
			} else { //sollte sich pl�tzlich wieder nur ein Socket im Array befinden:
				this.gameRequest.justOnePlayer();
				this.runCountdowndGameRequest = false;
			}
			
			
		}
	}
	
	protected void stopCountdown(){
		this.runCountdowndGameRequest = false;
	}
}//end Countdown_GameRequest