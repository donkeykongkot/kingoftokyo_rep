package ch.fhnw.itprojekt15.DonkeyKong.Server;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

import ch.fhnw.itprojekt15.DonkeyKong.Database.Constants_Database;
import ch.fhnw.itprojekt15.DonkeyKong.Database.Database_Access;

/**
 * In dieser Klasse wird der message austausch zwischen Datenbank und Client vollzogen.
 * 
 * Die Methode validation pr�ft die Eingaben im Registrierungsfenster und l�st jenachdem dan die passende
 * Methode (ok(), userAlreadyExists()) auf.
 * @author Roman
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public class Registration_Server implements Constants_Server, Constants_Database {

	private String message;
	private static Registration_Server singelton;
	private Socket socket;
	private String username;
	private String password;
	private int wins = 0;

	private Registration_Server() {

	}

	public static Registration_Server getRegistrationServer() {
		if (singelton == null) {
			singelton = new Registration_Server();
		}
		return singelton;
	}
	// Logindaten sind korrekt
	private void ok() {

		ArrayList<Client_Listener> clients = Server.getServer().getClients();

		for (Client_Listener cl : clients) {
			if (cl.getSocket() == this.socket) {
				cl.setUsername(this.username);
				break;
			}
		}

		String message;
		message = registrationOK + delimiter1 + registrationOK;
		Message.sendToOne(message, this.socket);

	}
	// Benutzer existert bereits
	private void userAlreadyExists() {
		String message;
		message = registrationUserAlreadyExists + delimiter1 + registrationUserAlreadyExists;
		Message.sendToOne(message, this.socket);
	}

	/**
	 * 
	 * @param message
	 * @param socket
	 */
	//Pr�f methode
	protected void validation(String message, Socket socket) {

		this.socket = socket;
		this.message = message;

		Scanner scanner = new Scanner(message);
		scanner.useDelimiter(delimiter2);
		username = scanner.next();
		password = scanner.next();

		if (Database_Access.getController().checkUsernameReg(username) == true) {
			this.userAlreadyExists();

		} else if (Database_Access.getController().insertNewUser(username, password, wins) == true) {
			this.ok();
		}
	}

}// end Registration_Server