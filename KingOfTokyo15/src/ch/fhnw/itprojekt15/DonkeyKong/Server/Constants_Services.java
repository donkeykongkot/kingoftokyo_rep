package ch.fhnw.itprojekt15.DonkeyKong.Server;

import java.net.Socket;

/**
 * Im Services-Enum wird mit der Methode searchLobby-Methodes eruiert, welche Methode
 * auf welchem zust�ndigen Server-Objekt aufgerufen werden soll. (switch case)
 * Messages die f�r das Game bestimmt sind, werden im Game-Objekt separat behandelt.
 * 
 * @author Mattias
 * @version 1.0
 * @created 06-Nov-2015 12:16:11
 */
public interface Constants_Services {

	public static enum Services{
		LoginValidation,
		RegistrationValidation,
		HighscoreRequestData,
		PlayerListRequestData,
		GameRequest,
		GameRequestJustOnePlayer,
		CountdownGameRequest,
		GameStartGame,
		GameBoardPlayerLeave,
		GamePlayerLeave,
		CountdownGame,
		GamePlayerSuspended,
		GameConnectionLost,
		GameActivePlayer,
		GameDiceTokyoBonus,
		GameTokyoLeaveQuestion,
		DiceRoll,
		DiceRollFinal,
		TokyoLeaveAnswer,
		TokyoGo,
		CardBuyAnswer,
		CardNewCards,
		CardBought,
		GameProoverMonsterLoose,
		GameProoverMonsterWin,
		TokyoLeave,
		PlayerDataUpdate,
		Chat,
		Testing,
		CardBuyQuestion,
		StopCountdown,
		LeaveLobby,
		GameProoverNobodyWins,
		HeartbeatFromClient,
		HeartbeatFromServer;
		
		/**
		 * 
		 * @param message
		 * @param service
		 * @param socket
		 */
		public void searchLobbyMethodes(String service, String message,  Socket socket) {
		
			switch(service){
			case "LoginValidation": 
				LoginProover.getLoginProover().validation(socket, message);
				break;
			case "RegistrationValidation": 
				Registration_Server.getRegistrationServer().validation(message, socket);
				break;
			case "HighscoreRequestData": 
				Highscore_Server.getHighscoreServer().requestData(message, socket);
				break;
			case "PlayerListRequestData": 
				PlayerList_Server.getPlayerList().requestData(message, socket);
				break;
			case "GameRequest": 
				GameRequest_Server.getGameRequest().request(message, socket);
				break;
			case "GameBoardPlayerLeave": 
				//Wird von Klasse GameLeaveListener behandelt.
				break;
			case "DiceRoll": 
				//Wird von Klasse Game behandelt.
				break;
			case "DiceRollFinal": 
				//Wird von Klasse Game behandelt.
				break;
			case "TokyoLeaveAnswer": 
				//Wird von Klasse Game behandelt.
				break;
			case "CardBuyAnswer": 
				//Wird von Klasse Game behandelt.
				break;
			case "CardNewCards": 
				//Wird von Klasse Game behandelt.
				break;
			case "CardBought": 
				//Wird von Klasse Game behandelt.
				break;
			case "Chat": 
				//Wird von Klasse Game behandelt.
				break;
			case "LeaveLobby":
				Server.getServer().leaveLobby(message, socket);
				GameRequest_Server.getGameRequest().leaveLobby(message, socket);
				break;
			case "HeartbeatFromClient":
				break;
			}
		}
		
	};
	public static boolean checkENUMCompatibility(Object[] enumType, String value){
		for(Object o: enumType){
			if(o.toString().equals(value)){
				return true;
			}
		}
		return false;
	}
	



}