package ch.fhnw.itprojekt15.DonkeyKong.Chat;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;

/**
 * Dies ist die Chat-Komponente. Sie macht sich die TickerBoard-Funktionalit�t
 * zu Nutze und verschickt �ber das Socket Messages des Service "Chat". Diese
 * Messages wiederum werden im TickerBoard allen noch verbleibenden Spielern im
 * Spiel angezeigt.
 * 
 * @author Andy
 * @version 1.0
 * @created 28.11.2015
 */

public class Chat_Controller implements ActionListener {
	private static Chat_Controller singleton;
	private Chat_Model model;
	private Chat_View view;

	public Chat_Controller() {
		this.view = new Chat_View();
		this.model = new Chat_Model(view);
		this.view.send.addActionListener(this);
		this.view.message.addActionListener(this);
	}

	public static Chat_Controller getController() {
		if (singleton == null) {
			singleton = new Chat_Controller();
		}
		return singleton;
	}

	public JPanel gui() {
		return this.view;
	}

	public static void kill() {
		singleton = null;
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		// Gilt sowohl f�r Button, als auch f�r Enter-Eingaben
		this.model.sendMsg(this.view.message.getText().trim());
		this.view.message.setText("");
		this.view.message.requestFocusInWindow();
	}
}
