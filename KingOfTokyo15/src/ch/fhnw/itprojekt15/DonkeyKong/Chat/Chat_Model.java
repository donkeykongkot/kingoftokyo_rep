package ch.fhnw.itprojekt15.DonkeyKong.Chat;

import ch.fhnw.itprojekt15.DonkeyKong.MonsterBoard.Constants_Monster;
import ch.fhnw.itprojekt15.DonkeyKong.Server.Constants_Services.Services;
import ch.fhnw.itprojekt15.DonkeyKong.ServerConnection.Server;

public class Chat_Model {
	
	private Chat_View view;
	
	public Chat_Model(Chat_View view){
		this.view = view;
	}
	
	protected void sendMsg(String message){
		String msg = Services.Chat + "|" + Constants_Monster.Monster.getMyMonster() + "," + message;
		Server.getServer().send(msg);	
	}
}
