package ch.fhnw.itprojekt15.DonkeyKong.Chat;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Chat_View extends JPanel implements Constants_Chat{

	private JLabel text = new JLabel(textGuiMessage);
	protected JTextField message = new JTextField();
	protected JButton send = new JButton(textButtonSend);

	public Chat_View() {
		super();
		this.setBorder(BorderFactory.createTitledBorder(textGuiName));
		this.setLayout(new FlowLayout());
		message.setPreferredSize(new Dimension(350, 30));
		this.add(text);
		this.add(message);
		this.add(send);
	}

}
