Um das Spiel starten zu können müssen folgende Voraussetzungen erfüllt sein:

1) Es muss **Java 8** (Client + Server) und **MySQL** (Server) installiert sein.

2) Die Spieler müssen sich im selben Netzwerk befinden, wobei die Firewall ausgeschaltet sein muss. Bei Tests im FHNW-Netzwerk *eduroam* konnte trotz verschiedener Port-Tests leider zu keiner Zeit eine Verbindung herstellt werden, weshalb ein anderes Netzwerk zu benützen ist. In unseren Tests erwies sich ein Android-Hotspot als hervorragende Alternative.

3) Installiert wird das Spiel in Eclipse wie folgt:

- Neuen, leeren Workspace eröffnen
- File > Import: Existing Eclipse Projects into Workspace
- Pfad zum zuvor entpackten Moodle-ZIP eingeben und “KingOfTokyo15” importieren.


4) Vor dem Spielstart müssen in der Datenbank-Configurationsdatei *Constants_Database* (unter Package ch.fhnw.itprojekt15.DonkeyKong.**Database**) die Logindaten der MySQL-Datenbank registriert sein, damit diese angesprochen werden kann. Es wird davon ausgegangen, dass der MySQL-User Schreibrechte besitzt, damit automatisch eine Datenbank und Tabellen erstellt werden können.